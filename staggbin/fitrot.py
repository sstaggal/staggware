#!/usr/bin/env python

import protomof
import pylab
import numpy
import scipy
import sys

intilt=sys.argv[1]
outtilt=sys.argv[2]

imagedict,paramdict,seriesname=protomof.parseTilt(intilt)

ns=imagedict.keys()
ns.sort()

tilts=[]
rots=[]

for n in ns:
	tilts.append(imagedict[n]['tilt'])
	rots.append(imagedict[n]['rotation'])

tilts=numpy.array(tilts)
rots=numpy.array(rots)
pylab.plot(tilts,rots,'ro')

p=scipy.polyfit(tilts,rots,1)
newrots=scipy.polyval(p,tilts)
pylab.plot(tilts,newrots)

#print newrots.shape

for n in ns:
	imagedict[n]['rotation']=newrots[n-1]

protomof.writeTiltFile(outtilt, seriesname,imagedict,paramdict)

pylab.show()
print "Done!"

