#!/usr/bin/env python

import glob
import os
from matplotlib import pyplot
import numpy
import frealignlib
import sys

def getnptcls(paramfilename):
	f=open(paramfilename,'r')
	lines=f.readlines()
	f.close()
	nptcls=int(lines[-1].split()[-1])
	return nptcls

paramfilename=sys.argv[1]

sets=glob.glob('set_*')

basedir=os.getcwd()
fscdict={}
nptcls=[]
respoint5=[]
respoint143=[]
fout=open('p_vs_res.txt','w')
fout.write('NPtcls\tFSC_0.5\tFSC_0.143\n')
for set in sets:
	os.chdir(set)
	print set
	ns, res, fsc = frealignlib.parseCombine('frealign.combine.out')
	nptcls.append(getnptcls(paramfilename))
	fscdict[nptcls[-1]]=(ns,res,fsc)
	os.chdir(basedir)
	
	for f in range(len(fsc)):
		#print fsc[f]
		if fsc[f] < 0.5:
			fa=fsc[f-1]
			fb=fsc[f]
			frac=(fa-0.5)/(fa-fb)
			#print fa,fb, fa-fb, (fa-0.5)/(fa-fb)
			ra=res[f-1]
			rb=res[f]
			respoint5.append(ra-((ra-rb)*frac))
#			fout.write('%d\t%f\n'% (nptcls[-1],respoint5[-1]))
			break
	for f in range(len(fsc)):
		#print fsc[f]
		if fsc[f] < 0.143:
			fa=fsc[f-1]
			fb=fsc[f]
			frac=(fa-0.143)/(fa-fb)
			#print fa,fb, fa-fb, (fa-0.5)/(fa-fb)
			ra=res[f-1]
			rb=res[f]
			respoint143.append(ra-((ra-rb)*frac))
			break
	fout.write('%d\t%f\t%f\n'% (nptcls[-1],respoint5[-1],respoint143[-1]))

fout.close()
respoint5=numpy.array(respoint5)
respoint143=numpy.array(respoint143)
nptcls=numpy.array(nptcls)		
#pyplot.plot(1/res,fsc)
pyplot.plot(numpy.log(nptcls),1/respoint5,'bo')	
pyplot.plot(numpy.log(nptcls),1/respoint143,'ro')	
ylocs,labs=pyplot.yticks()
ylabs=1/ylocs
pyplot.yticks(ylocs,ylabs)

#xlocs,xlabs=pyplot.xticks()
#xlabs=10**xlabs
#pyplot.xticks(xlocs,xlabs)

#pyplot.plot(nptcls,respoint5,'bo')
#pyplot.plot(nptcls,respoint143,'ro')
pyplot.show()
