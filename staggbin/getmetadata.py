#!/usr/bin/env python

#import frealignlib
import optparse
import os
import sys
import sinedon
import leginondata
import math

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('--session', '-s', dest='s', help='session name')
	parser.add_option('--preset', '-p', dest='p', help='preset name')
	parser.add_option('--metafile', '-m', dest='m', help='output metadata file name')
	options, args=parser.parse_args()
	
	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options

def recordLog(args):
	f=open('getmetadata.log','a')
	[f.write('%s '% arg) for arg in args]
	f.write('\n')
	f.close()

	
if __name__=="__main__":
	recordLog(sys.argv)
	options=parseOptions()
	sinedon.setConfig('leginondata')
	
	lq=leginondata.AcquisitionImageData()
	sq=leginondata.SessionData()
	pq=leginondata.PresetData()
	
	sq['name']=options.s
	pq['name']=options.p
	lq['session']=sq
	lq['preset']=pq
	
	imagedata=lq.query()
	
	f=open(options.m,'w')
	#write header
	f.write('imagename\tdarkreference\tbrightreference\texposuretime(ms)\tdose(e-/A^2))\tnominaldefocus(um)\tstagetilt(degrees)\tkeV\n')
	for image in imagedata:
		presettime=image['preset']['exposure time']
		cameratime=image['camera']['exposure time']
		presetdose=image['preset']['dose']
		if presetdose is not None:
			presetdose=presetdose/10**20
		else:
			presetdose=-1
			
		actualdose=presetdose/presettime*cameratime
		print image['filename']
		try:
			f.write('%s\t%s\t%s\t%.2f\t%.2f\t%.2f\t%.2f\t%d\n' % (image['filename'],image['dark']['filename'], image['bright']['filename'],cameratime,actualdose,image['scope']['defocus']/10e-7,image['scope']['stage position']['a']*180/math.pi,image['scope']['high tension']/1000))
		except:
			f.write('%s\tNone\n' % image['filename'])
	f.close()
	
