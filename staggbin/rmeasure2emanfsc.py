#!/usr/bin/env python

import sys

infile=sys.argv[1]
outfile=sys.argv[2]
apix=float(sys.argv[3])
box=float(sys.argv[4])

f=open(infile)
lines=f.readlines()
f.close()

f=open(outfile,'w')
start=False
for line in lines:
	if start is True:
		words=line.split()
		if len(words)==0:
			break
		res=float(words[0])
		corr=float(words[3])
		Fpix=apix*box/res
		Fpix=int(round(Fpix))
		f.write('%d\t%f\n' % (Fpix,corr))
	
	
	if line[0:4]=='  ==':
		print "True now"
		start=True
f.close()
print "Done!"

