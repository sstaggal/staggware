#!/usr/bin/env python

from pyami import mrc
import sys
import numpy

maskname=sys.argv[1]
newmask=sys.argv[2]
a=mrc.read(maskname)

overcount=0
undercount=0
count=0
for i in numpy.nditer(a, op_flags=['readwrite']):
	if i > 1.0:
		i[...]=1.0
		overcount+=1
	elif i < 0.0:
		i[...]=0.0
		undercount+=1
	count+=1
print overcount,'voxels greater than 1 out of', count,'voxels total or',overcount/float(count)*100,"%",undercount,'voxels less than 0'

mrc.write(a,newmask)

