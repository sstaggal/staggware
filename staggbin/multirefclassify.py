#!/usr/bin/env python

from appionlib import apEMAN
import sys
import os
import EMAN

ptclstack=sys.argv[1]
refstack=sys.argv[2]

nptcls=apEMAN.getNumParticlesInStack(ptclstack)

nrefs=apEMAN.getNumParticlesInStack(refstack)
refs=EMAN.readImages(refstack,-1,-1,0)

#set up class lists
classes=[]
for cls in range(nrefs):
	classes.append([])

#sort particles into lists corresponding to classes
for n in range(nptcls):
	ptcl=EMAN.EMData()
	ptcl.readImage(ptclstack,n)
	bestcc=0
	cc=[]
	for ref in range(nrefs):
		currcc=apEMAN.getCC(refs[ref],ptcl)
		if currcc > bestcc:
			bestcc=currcc
			bestref=ref
		cc.append(currcc)
	classes[bestref].append(n)

#write cls files
for cls in range(len(classes)):
	name='cls%d.lst' % (cls)
	f=open(name, 'w')
	f.write('#LST\n')
	for ptcl in classes[cls]:
		f.write('%d\t%s\n' % (ptcl, ptclstack))
	f.close()
	
	command='proc2d %s %s average' % (name,'classes.hed')
	print command
	os.system(command)
	
#print classes
print "Done!"
