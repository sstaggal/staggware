#!/usr/bin/env python

#this is a straight copy and paste of the mrcconvert.py file found in pyami


from optparse import OptionParser
import sys
from pyami import mrc
import numpy
import glob
import os

usage = '''
Convert all the MRC images in a directory from input mode to mode 1.
   %prog inputdirectory outputdirectory

  MODE   data type
  ----   ---------
     0   signed 8-bit bytes range -128 to 127
     1   16-bit halfwords
     2   32-bit reals
     3   complex 16-bit integers
     4   complex 32-bit reals
     6   unsigned 16-bit range 0 to 65535
'''

parser = OptionParser(usage=usage)

(options, args) = parser.parse_args()

needhelp = False

mode = 1

if len(args) != 2:
	needhelp = True

if needhelp:
	parser.print_help()
	sys.exit()

indirectory = args[0]
outdirectory = args[1]

images=glob.glob(os.path.join(indirectory,"*.mrc"))
count=0
for image in images:
	imagename=os.path.split(image)[1]
	count+=1
	print count, "Converting", imagename
	input = mrc.read(image)
	newtype = mrc.mrc2numpy[mode]
	output = numpy.asarray(input, newtype)
	mrc.write(output, os.path.join(outdirectory,imagename))

print "Done!"

