#!/usr/bin/env python

from protomof import *
import glob, sys
import pylab
import colorsys

if len(sys.argv) ==1:
	print """Please, check your arguments.
Usage: plotrotitr.py <seriesname> 
--------------------------------------
"""
	sys.exit()


seriesname=sys.argv[1]

tiltfiles=glob.glob(seriesname+"*-fitted.tlt")
tiltfiles.sort()


iters=[]
fig=pylab.figure(1)
hue=0.0
colorincrement=0.66666666666/len(tiltfiles)
for tilt in tiltfiles:
	iteration=int(tilt.split('-')[1])
	imagedict,paramdict,seriesname=parseTilt(tilt)
	rot=[]
	angle=[]	
	for img in range(1,len(imagedict)+1):
		angle.append(imagedict[img]['tilt'])
		rot.append(imagedict[img]['rotation'])
	color=colorsys.hsv_to_rgb(hue,1,1)
#	print color
	pylab.plot(angle,rot, label=tilt, figure=fig, color=color)
	hue+=colorincrement

pylab.legend()
pylab.show()

print "Done!"
