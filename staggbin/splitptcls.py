#!/usr/bin/env python

import frealignlib
import splitptclslib
import optparse
import os
import sys
import sinedon

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('--stackid', dest='stackid', type='int', help='primary key of stack in database')
	parser.add_option('--projectid', dest='projectid', type='int', help='primary key of project in database')
	parser.add_option('--sets',dest='sets',type='int', default=5, help='number of sets to split the data into')
	parser.add_option('--setuponly', dest='setuponly', default=False, action='store_true', help="setup without executing")
	parser.add_option('--paramfile',dest='paramfile', help='path to frealign param file to modify, use full path')
	parser.add_option('--stackfile', dest='stackfile', help='path to stack file, use full path')
	parser.add_option('--jobfile', dest='jobfile', help='path to job file, use full path')
	parser.add_option('--type', dest='type', help='split type, e.g. imageshift, timestamp, etc')
	parser.add_option('--threshold', type='float', dest='threshold', help='presa threshold for inclusion in the map. Note this does not modify the jobfile. User should make sure that the values match')
	parser.add_option('--queue', dest='queue', default='stagg_q', help='queue to submit job to')
	parser.add_option('--conftype', dest='conftype', default='res80', help='confidence type for titrate confidence option')
#other options
	options, args=parser.parse_args()
	
	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options

def recordLog(args):
	f=open('splitptcls.log','a')
	[f.write('%s '% arg) for arg in args]
	f.write('\n')
	f.close()

	
if __name__=="__main__":
	recordLog(sys.argv)
	options=parseOptions()
	options.new_score=0
	db='ap%d' % (options.projectid)
	sinedon.setConfig('appiondata',db=db)

	if options.type=='imageshift':
		data, first,last,div=splitptclslib.splitimageshift(options.stackid,options.sets)
		print first, last,div
		splitptclslib.submitjobs(options,data,first,last,div)

	elif options.type=='holeposition':
		data, first,last,div=splitptclslib.splitholeposition(options.stackid,options.sets)
		print first, last,div
		splitptclslib.submitjobs(options,data,first,last,div)

	elif options.type=='timestamp':
		data, first,last,div=splitptclslib.splittime(options.stackid,options.sets)
		print first, last,div
		splitptclslib.submitjobs(options,data,first,last,div)

	elif options.type=='square':
		sets=splitptclslib.splitsquare(options.stackid)
		print sets
		splitptclslib.submitsquarejobs(options,sets)

	elif options.type=='defocus':
		data, first,last,div=splitptclslib.splitdefocus(options.paramfile,options.sets)
		print first, last,div
		splitptclslib.submitjobs(options,data,first,last,div)

	elif options.type=='logsplit':
		splitptclslib.submitlogsplitjobs(options)	

	elif options.type=='phaseresidual':
		data, first,last,div=splitptclslib.splitphaseresidual(options.paramfile,options.sets)
		print first, last,div
		splitptclslib.submitphaseresidualjobs(options,data,first,last,div)
		
	elif options.type=='ptcldensity':
		data, first,last,div=splitptclslib.splitptcldensity(options.stackid,options.sets)
		print first, last,div
		splitptclslib.submitjobs(options,data,first,last,div)
	
	elif options.type=='aceconfidence':
		data, first,last,div=splitptclslib.aceconfidence(options.stackid,options.sets)
		print first, last,div
		splitptclslib.submitjobs(options,data,first,last,div)
	elif options.type=='titrateconfidence':
		splitptclslib.titrateconfidence(options.stackid,options.sets)
	
	else:
		print options.type, "not supported"
		sys.exit()
		
