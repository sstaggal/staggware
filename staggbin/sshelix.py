#!/usr/bin/env python

import os
import sys
import EMAN
import optparse

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('--iters', dest='iterations', type='int', help='number of refinement iterations')
	parser.add_option('--dang', dest='dang', type='float', default=3, help='angular increment')
	parser.add_option('--minalt', dest='minalt', type='float', default=90, help='minimum altitude, default=90')
	parser.add_option('--proc', dest='proc', type='int', default=1, help='number of processors to use in refinement')
	parser.add_option('--classkeep', dest='classkeep', type='float', default=1.0, help='cutoff for keeping particles')
	parser.add_option('--classiter', dest='classiter', type='int', default=8, help='number of iterations of class averaging')
	parser.add_option('--iradius', dest='iradius', type='float', default=0.0, help='inner radius in Angtroms for helical symmetry operations')
	parser.add_option('--oradius', dest='oradius', type='float', help='outer radius in Angstroms for helical symmetry operations')
	parser.add_option('--apix', dest='apix', type='float', help='pixel size in Angstroms')
	parser.add_option('--twistsearch', dest='twistsearch', type='float',default=1.0, help='twist search step in degrees. Default is 1.0')
	parser.add_option('--risesearch', dest='risesearch', type='float', default=0.1, help='rise search step in Angstroms. Default is 0.1')
	parser.add_option('--stack', dest='stack', default='start.hed', help='particle stack')
	parser.add_option('--nosearch', dest='nosearch', default=False, action='store_true', help='do not run helical parameter search')
	parser.add_option('--nosym', dest='nosym', default=False, action='store_true', help='do not apply helical symmetry')
	parser.add_option('--startiter', dest='startiter', type='int', default=0, help='starting iteration. default is 0. initial volume should be threed.<startiter>a.mrc. Thus, default is threed.0a.mrc.')
	parser.add_option('--coran', dest='coran', default=False, action='store_true', help='run correspondence analysis after each iter')
	parser.add_option('--coranfactornumbers', dest='coranfactornumbers', type='str', default='1,2,3', help='eigenfactors to use for classification')
	parser.add_option('--nophase', dest='nophase', default=False, action='store_true', help='do not use the default phase classifier')
	parser.add_option('--csym', dest='csym', default=None, help="C symmetry to apply before estimating and applying helical symmetry. Format: 'csym=c6' ")
	parser.add_option('--fsc', dest='fsc', default=False, action='store_true', help='make FSC plot each iteration. Both symmetrized and unsymmetrized versions are made')
	options, args=parser.parse_args()

	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options

def makemrafile(stackname,filename,boxsize,nptcls,proc,nophase=False):
	minjobs=(boxsize**2 * nptcls) // 40000000
	if minjobs < proc:
		nfiles=proc
	else:
		nfiles=minjobs
	f=open(filename,'w')
	for n in range(nfiles):
		if nophase:
			command='classesbymra %s proj.hed split refine logit=1 shrink=1 sep=1 frac=%d/%d quiet\n' %  (stackname,n,nfiles)
		else:
			command='classesbymra %s proj.hed split refine logit=1 shrink=1 sep=1 phase frac=%d/%d quiet\n' %  (stackname,n,nfiles)		
		f.write(command)
	f.close()
	
def printandexecute(command, execute=True):
	print command
	if execute:
		os.system(command)
		
def printlog(argv,logfile='log.log'):
	if os.path.exists(logfile):
		f=open(logfile,'r+')
		f.seek(0,2)
	else:
		f=open(logfile,'w')
	for word in argv:
		f.write('%s ' % word)
	f.write('\n')
	f.close()

if __name__ == "__main__":
	options=parseOptions()
	printlog(sys.argv)
#	dang=3
#	minalt=85
#	proc=16
#	iterations=12
#	classkeep=1.0
#	classiter=8
#	iradius=0
#	oradius=55
#	apix=3.0
#	twistsearch=3.0
#	risesearch=0.3
#	stack='start.hed'
	mrafile='tmpmra.txt'

	nptcls=EMAN.fileCount(options.stack)[0]
	stackdata=EMAN.EMData()
	stackdata.readImage(options.stack,0)
	boxsize=stackdata.xSize()

	for iteration in range(options.startiter,(options.startiter+options.iterations)):

		invol='threed.%da.mrc' % iteration
		outvol='threed.%d.mrc' % (iteration+1)
		outvola='threed.%da.mrc' % (iteration+1)


		###make projections
		command='project3d %s out=proj.hed prop=%f minalt=%f' % (invol, options.dang, options.minalt)
		printandexecute(command)

		###align and classify
		command='proc2d proj.hed cls mraprep'
		printandexecute(command)

		if options.nophase:
			makemrafile(options.stack,mrafile,boxsize,nptcls,options.proc,nophase=True)
		else:
			makemrafile(options.stack,mrafile,boxsize,nptcls,options.proc)
						
		command='runpar proc=%d,%d file=%s' % (options.proc,options.proc,mrafile)
		printandexecute(command)

		###make averages
		if options.nophase:
			command='classalignall %d ref1 finalref saverefs keep=%f proc=%d logit=1 refine' % (options.classiter,options.classkeep,options.proc)
		else:
			command='classalignall %d ref1 finalref saverefs keep=%f proc=%d logit=1 refine phase' % (options.classiter,options.classkeep,options.proc)			
		printandexecute(command)

		
		###clean up
		command='tar cvf cls.%d.tar cls????.lst' % (iteration+1)
		printandexecute(command)

		command='mv classes.hed classes.%d.hed' % (iteration+1)
		printandexecute(command)
		command='mv classes.img classes.%d.img' % (iteration+1)
		printandexecute(command)


		###reconstruct
		pad=int(round(boxsize * 1.25))
		if pad%2 != 0:
			pad+=1

		command='make3d classes.%d.hed out=%s pad=%d' % (iteration+1,outvol, pad)
		printandexecute(command)
		
		command='proc3d %s %s norm' % (outvol,outvola)
		printandexecute(command)
		
		###run coran if selected
		if options.coran is True:
			command='coran_for_cls.py iter=%d proc=%d mask=%d hard=60 eotest factornumbers=%s' % (iteration+1, options.proc, boxsize/2, options.coranfactornumbers)
			printandexecute(command)
			
		###apply csym if selected
		if options.csym is not None:
			command='proc3d %s %s sym=%s' % (outvola, outvola, options.csym)
			printandexecute(command)
		
		command='proc3d %s %s rot=90,0,0 spidersingle' %(outvola,'vol.spi')
		printandexecute(command)		

		###apply helical symmetry with IHRSR tools
		if options.nosearch is False:
			command='hsearch_lorentz %s %s %f %f %f %f %f' % ('vol.spi', 'symdoc.spi', options.apix, options.iradius, options.oradius, options.twistsearch, options.risesearch)
			printandexecute(command)

		if options.nosym is False:
			command='himpose %s %s %s %f %f %f' % ('vol.spi', 'symdoc.spi', 'volsym.spi', options.apix, options.iradius, options.oradius)
			printandexecute(command)

			command='proc3d %s %s' % ('volsym.spi',outvola)
			printandexecute(command)
		
		### do FSC
		if options.fsc is True and options.coran is False:
			command='eotest proc=%d pad=%d classkeep=%d classiter=%d refine' % (options.proc, pad, options.classkeep, options.classiter)
			printandexecute(command)
			os.rename('fsc.eotest',('fsc.eotest.%d' % (iteration+1)))
			
		###clean up	
		command='rm cls????.lst'
		printandexecute(command)

		print

	print 'Done!'
