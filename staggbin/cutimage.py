#!/usr/bin/env python

import optparse
import sys
import os
import numpy

def parseOptions():
	usage="usage: %prog [options] <in>.img <out>.img "
	parser=optparse.OptionParser(usage=usage)
	parser.add_option('--center', dest='center', help='desired center <x,y,z>')
	parser.add_option('--boxsize', dest='boxsize', help='boxsize <x,y,z>')
	options, args=parser.parse_args()

	if len(args) != 2 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options,args


if __name__ == "__main__":
	options,args=parseOptions()
	
	
	words=options.center.split(',')
	center = [int(word) for word in words]
	center=numpy.array(center)
	words=options.boxsize.split(',')
	boxsize= [int(word) for word in words]
	boxsize=numpy.array(boxsize)
	
	halfbox=numpy.round(boxsize/2)
	origin=center - halfbox
	
	command=('i3cut -o %d %d %d -s %d %d %d %s %s' % (origin[0],origin[1],origin[2],boxsize[0],boxsize[1],boxsize[2],args[0],args[1]))
	print command
	os.system(command)
	print "Done!"

		
	
