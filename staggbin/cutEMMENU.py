#!/usr/bin/env python

from pyami import mrc
from pyami import numpil
import sys

infile=sys.argv[1]
outfile=sys.argv[2]

a=numpil.read(infile)

size=a.shape[1]

a=a[0:size,:]

mrc.write(a, outfile)

