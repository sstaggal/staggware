#!/usr/bin/env python

import numpy
import sys

def parseCombine(combineout):
	f=open(combineout)
	lines=f.readlines()
	f.close()
	
	n=0
	while n <  len(lines):
		line=lines[n]
		if line[0]=='C':
			words=line.split()
			if words[1:3]==['NO.','RESOL'] and 'CC' in words:
				n+=1
				line=lines[n]
				ns=[]
				fsc=[]
				res=[]
				while line[0]=='C':
					line=lines[n]
					words=line.split()
					if words[1]=='Average':
						break
					ns.append(float(words[1]))
					res.append(float(words[2]))
					fsc.append(float(words[6]))
					n+=1
				break
		n+=1
	ns=numpy.array(ns)
	res=numpy.array(res)
	fsc=numpy.array(fsc)
	return ns, res, fsc

if __name__ == "__main__":

	if len(sys.argv) ==1:
		print """Please, check your arguments.
	Usage: grefrealignres.py <frealign.combine.out> 
	--------------------------------------
	"""
		sys.exit()

	
	outfile=sys.argv[1]
	
	ns,res,fsc=parseCombine(outfile)
	for f in range(len(fsc)):
		#print fsc[f]
		if fsc[f] < 0.5:
			fa=fsc[f-1]
			fb=fsc[f]
			frac=(fa-0.5)/(fa-fb)
			#print fa,fb, fa-fb, (fa-0.5)/(fa-fb)
			ra=res[f-1]
			rb=res[f]
			respoint5= (ra-((ra-rb)*frac))
			break
	for f in range(len(fsc)):
		#print fsc[f]
		if fsc[f] < 0.143:
			fa=fsc[f-1]
			fb=fsc[f]
			frac=(fa-0.143)/(fa-fb)
			#print fa,fb, fa-fb, (fa-0.5)/(fa-fb)
			ra=res[f-1]
			rb=res[f]
			respoint143=(ra-((ra-rb)*frac))
			break
	
	print "FSC 0.5 =",respoint5
	print "FSC 0.143 =",respoint143
