#!/usr/bin/env python

from appionlib import apDatabase
import optparse
import sys
import smtplib
from email.mime.text import MIMEText
import time

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-f', dest='f', type='str', help='frames disk usage file')
	parser.add_option('-l', dest='l', type='str', help='leginon disk usage file')
	parser.add_option('-a', dest='a', type='str', help='appion disk usage file')
	parser.add_option('-e', dest='e', default=False, action='store_true', help='email users usage info.')
	parser.add_option('--usagethresh', dest='usagethresh', type='int', default=500, help='usage threshold for emailing users')
	options, args=parser.parse_args()
	
	if len(args) !=0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	return options

def parseUsageFile(filename,sessioninfo,datatype):
	f=open(filename,'r')
	lines=f.readlines()
	f.close()
	
	for line in lines:
		words=line.split()
		meg_usage=int(words[0])
		sessionname=words[1].split('/')[-1]
		if sessionname not in sessioninfo.keys():
			sessioninfo[sessionname]={}
			try:
				sessiondata=apDatabase.getSessionDataFromSessionName(sessionname)
				sessioninfo[sessionname]['user']=sessiondata['user']['firstname']
				sessioninfo[sessionname]['group']=sessiondata['user']['group']['name']
				sessioninfo[sessionname]['info']=sessiondata['comment']
				sessioninfo[sessionname]['email']=sessiondata['user']['email']
			except:
				sessioninfo[sessionname]['user']=None
			
			sessioninfo[sessionname]['totals']={}
		sessioninfo[sessionname]['totals'][datatype]=meg_usage/1000.0
			
def sortByUsage(a,b):
	atotal=a[1]['sum']
	btotal=b[1]['sum']
	
	return int(round(atotal-btotal))	

def sendEmail(to=None, frm='sstagg@fsu.edu', subject=None, text=None):
	msg=MIMEText(text)
	msg['Subject']=subject
	msg['From'] = frm
	msg['To'] = to
	s = smtplib.SMTP('localhost')
	s.sendmail(frm, [to], msg.as_string())
	s.quit()

	

if __name__ == "__main__":

	options=parseOptions()
	
	manager='sstagg@fsu.edu'
	summaryfile='/data/lustre/myamiusage.txt'
	sessioninfo={}
	parseUsageFile(options.f,sessioninfo,'framesusage')
	parseUsageFile(options.l,sessioninfo,'leginonusage')
	parseUsageFile(options.a,sessioninfo,'appionusage')
	
	### generate usage sums
	keys = sessioninfo.keys()
	for key in keys:
		sessioninfo[key]['sum']=sum(sessioninfo[key]['totals'].values())
	
	
	### generate sorted list of tuples with keys as item 0
	sessionsort=sessioninfo.items()
	sessionsort.sort(cmp=sortByUsage,reverse=True)
	
	groupleaders={'stagg':'sstagg@fsu.edu', 'taylor':'ktaylor@fsu.edu', 'stroupe': 'mestroupe@bio.fsu.edu', 'hongli': 'hli4@fsu.edu', 'admin':'dsousa@fsu.edu', 'guest':'dsousa@fsu.edu'}
	grouptotals={}
	
	f=open(summaryfile,'w')
	
	for items in sessionsort:
		session=items[0]
		s=''
		s+="Session\t=\t%s\n" % (session)
		s+="User\t=\t%s\n" % (sessioninfo[session]['user'])
		info=sessioninfo[session].get('info')
		if info is not None:
			s+="Comment\t=\t%s\n" % info
		s+="Total\t=\t%.3f GB\n" % (sessioninfo[session]['sum'])
		
		
		appionusage=sessioninfo[session]['totals'].get('appionusage')
		leginonusage=sessioninfo[session]['totals'].get('leginonusage')
		framesusage=sessioninfo[session]['totals'].get('framesusage')
		
		if appionusage is not None:
			s+="Appion\t=\t%.3f GB\n" % (appionusage)
		
		if leginonusage is not None:
			s+="Leginon\t=\t%.3f GB\n" % (leginonusage)
		
		if framesusage is not None:
			s+="Frames\t=\t%.3f GB\n" % (framesusage)
		s+='\n'
		print s
		if sessioninfo[session].get('group') in grouptotals.keys():
			#print sessioninfo[session].get('group')
			grouptotals[sessioninfo[session].get('group')]+=sessioninfo[session]['sum']
		else:
			#print sessioninfo[session].get('group')
			grouptotals[sessioninfo[session].get('group')]=sessioninfo[session]['sum']
		
		if options.e is True and sessioninfo[session]['sum'] > options.usagethresh:
			subject='Disk usage for session %s' % (session)
			message="Dear %s,\n" % (sessioninfo[session]['user'])
			message+="This message is to inform you that the disk usage for session %s has exceeded %d GB. You don't necessarily need to take any action at this time, but please make sure that your usage does not exceed the limit for your group.\n\n" % (session,options.usagethresh)
			message+=s
			sendEmail(to=sessioninfo[session]['email'], frm=manager, subject=subject, text=message)
			time.sleep(2)
		f.write(s)
	f.close()
	
	for group in grouptotals.keys():
		for groupleader in groupleaders.keys():
			#print group
			if group is not None and groupleader in group.lower():
				subject='Disk usage for %s group' % (group)
				message="Dear Dr. %s,\n" % (groupleader.title())
				message+="Your group's total disk usage for all of your Leginon, Appion, and Frames data is %d GB\n" % (grouptotals[group])
				if options.e is True:
					sendEmail(to=groupleaders[groupleader], frm=manager, subject=subject, text=message)
				break
	print grouptotals
