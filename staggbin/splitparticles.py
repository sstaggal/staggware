#!/usr/bin/env python

import glob
import os
import EMAN

def sortptcls(a,b):
	an=int(a.split()[0])
	bn=int(b.split()[0])
	return an-bn

dirs=glob.glob("*.dir")

wd=os.getcwd()

ptcls=[]
for d in dirs:
	print d
	classdir=os.path.join(d,'classes')
	os.chdir(classdir)
	lsts=glob.glob("*.lst")
	lstroot=lsts[0].split('.')[0]
	lst=lstroot + '.lst'
	f=open(lst)
	lines=f.readlines()
	f.close()
	lines.pop(0)
	ptcls+=lines
	os.chdir(wd)
ptcls.sort(sortptcls)

f=open('good.lst','w')
f.write('#LST\n')

ptclnumbers=[]
for ptcl in ptcls:
	s=ptcl.replace('start.hed','../start.hed')
	words=ptcl.split()
	ptclnumbers.append(int(words[0]))
	f.write(s)
f.close()
print len(ptcls)

nptcls,imgtype=EMAN.fileCount('../start.hed')

f=open('bad.lst','w')
f.write('#LST\n')
for ptcl in range(nptcls):
	if ptcl not in ptclnumbers:
		f.write('%d\t../start.hed\n' % ptcl)
f.close()
print "Done!"


