#!/usr/bin/env python

import protomof
import optparse
import sys

def parseOptions():
	usage="usage: %prog [options] img1 img2 ... \nWhere img<n> are tilt image numbers separate by spaces.\nThese will be removed in OUTTILT."
	parser=optparse.OptionParser(usage=usage)
	parser.add_option('-i', dest='intilt', help='input tlt file')
	parser.add_option('-o', dest='outtilt', help='output tlt file')
	parser.add_option('-r', dest='refnumber', help='reference image number in original tilt file', type='int')
	options, args=parser.parse_args()

	if len(args) == 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	intargs=[]
	for arg in args:
		intargs.append(int(arg))
	
	return options, intargs

if __name__ == "__main__":
	options, args=parseOptions()
	imagedict,parameterdict,seriesname=protomof.parseTilt(options.intilt)

	newimagedict={}
	keys=imagedict.keys()
	keys.sort()
	newkey=1
	for key in keys:
		if key==options.refnumber:
			newref=newkey
			print "new reference number is", newref			
		if key not in args:
			newimagedict[newkey]=imagedict[key]
			newkey+=1
		else:
			print "skipping image number", key
			#print imagedict[key]
	protomof.writeTiltFile2(options.outtilt,seriesname,newimagedict,parameterdict['azimuth'],newref)
	
	print "Done!"
	
