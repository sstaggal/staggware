#!/usr/bin/env python

import optparse
import sys
import os
import numpy

def parseOptions():
	usage="usage: %prog [options] <in>.img <pos>.pos "
	parser=optparse.OptionParser(usage=usage)
	parser.add_option('--boxsize', dest='boxsize', help='boxsize <x,y,z>')
	options, args=parser.parse_args()

	if len(args) != 2 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options,args

def parsePositionFile(filename):
	f=open(filename)
	lines=f.readlines()
	f.close()
	
	coords=[]
	for line in lines:
		words=line.split()
		coords.append(numpy.array([int(words[0]),int(words[1]),int(words[2])]))
	return coords

if __name__ == "__main__":
	options,args=parseOptions()
	
	tomogram=args[0]
	posfile=args[1]
	
	words=options.boxsize.split(',')
	boxsize= [int(word) for word in words]
	boxsize=numpy.array(boxsize)
	
	halfbox=numpy.round(boxsize/2)

	#parse position file
	coords=parsePositionFile(posfile)
	
	basename=os.path.splitext(tomogram)[0]
	
	for n in range(len(coords)):
		subvolumename='%s.%d.%s' % (basename,n,'spi')
		origin=coords[n] - halfbox
		command=('i3cut -fmt spider -o %d %d %d -s %d %d %d %s %s' % (origin[0],origin[1],origin[2],boxsize[0],boxsize[1],boxsize[2],tomogram,subvolumename))
		print command
		os.system(command)
	print "Done!"

		
	
