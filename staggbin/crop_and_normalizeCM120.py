#!/usr/bin/env python

from pyami import mrc
from pyami import numpil
import sys
import os
import glob
import time
import subprocess
from appionlib import apEMAN

####
# This script will remove the information box from tiff images
# collected on the CM120 or CM120 BioTwin, and will set the image
# mean to 0 and standard deviation to 1 using EMAN proc2d.
# Finished mrc files will then be ready to upload to Appion.
#
# NOTE: Please run this from one of the krios nodes as it requires
# Appion libraries.
####


tiffiles = glob.glob('*.tif')
for tif in tiffiles:
	newmrc=tif[:-3]+'mrc'
	infile=tif
	outfile=newmrc
	a=numpil.read(infile)
	size=a.shape[1]
	a=a[0:size,:]
	mrc.write(a, outfile)
	mrcfiles = glob.glob('*.mrc')

for mrcfile in mrcfiles:
	apEMAN.executeEmanCmd("proc2d %s %s norm=0,1"%(mrcfile,mrcfile))

print "Finished"

