#!/usr/bin/env python

from appionlib import starFile
from appionlib import proc2dLib
from appionlib import apFrealign
import optparse
import sys


def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('--star', dest='star', type='str', help='input star file')
	parser.add_option('--datablock', dest='datablock', type='str', default='data_', help='name of data block at top of star file')
	parser.add_option('--par', dest='par', type='str', help='output star file')
	parser.add_option('--mag', dest='mag', type='int', default=10000, help='magnification for par file')
	parser.add_option('--outstack', dest='outstack', type='str', default='stack.hed', help='output stack')
	parser.add_option('--apix', dest='apix', type='float', default=1.0, help='apix for the output stack')
	parser.add_option('--skipstack', dest='skipstack', action='store_true', default=False, help='skip making stack')
	
	options, args=parser.parse_args()

	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options

if __name__ == "__main__":
	options=parseOptions()
	star=starFile.StarFile(options.star)
	star.read()
	dataBlock=star.getDataBlock(options.datablock)
	loopDict=dataBlock.getLoopDict()
	f=open(options.par, 'w')
	proc2d=proc2dLib.RunProc2d()
	proc2d.setValue('outfile',options.outstack)
	proc2d.setValue('apix', options.apix)
	proc2d.setValue('append', True)
	
	micrograph=None
	groupnumber=0
	for n,ptcldict in enumerate(loopDict):
		newdict={}
		origstackn=int(ptcldict['_rlnImageName'].split('@')[0])
		inputfile=ptcldict['_rlnImageName'].split('@')[1]
		keys=ptcldict.keys()
		if '_rlnAnglePsi' in keys:
			newdict['psi']=float(ptcldict['_rlnAnglePsi'])
		else:
			newdict['psi']=0
		if '_rlnAngleTilt' in keys:
			newdict['theta']=float(ptcldict['_rlnAngleTilt'])
		else:
			newdict['theta']=0
		if '_rlnAngleRot' in keys:
			newdict['phi']=float(ptcldict['_rlnAngleRot'])
		else:
			newdict['phi']=0
		if '_rlnOriginX' in keys:
			newdict['shx']=float(ptcldict['_rlnOriginX'])
		else:
			newdict['shx']=0
		if '_rlnOriginY' in keys:
			newdict['shy']=float(ptcldict['_rlnOriginY'])
		else:
			newdict['shy']=0
			
		newdict['mag']=options.mag
		
		if '_rlnGroupNumber' in keys:
			newdict['film']=int(ptcldict['_rlnGroupNumber'])
		else:
			if micrograph != ptcldict['_rlnMicrographName']:
				micrograph=ptcldict['_rlnMicrographName']
				groupnumber+=1
			newdict['film']=groupnumber
		newdict['df1']=float(ptcldict['_rlnDefocusU'])
		newdict['df2']=float(ptcldict['_rlnDefocusV'])
		newdict['angast']=float(ptcldict['_rlnDefocusAngle'])
		newdict['presa']=0
		newdict['dpres']=0
		newdict['pnumber']=0
		newdict['ptclnum']=n+1
		#print newdict
		apFrealign.writeParticleParamLine(newdict, f)
		proc2d.setValue('infile', inputfile)
		proc2d.setValue('first', origstackn-1)
		proc2d.setValue('last', origstackn-1)
		proc2d.setValue('append', True)
		proc2d.setValue('invert', True)
		if options.skipstack is False:
			proc2d.run()
		print n
	f.close()
print "Done!"
	

		
		
		
		
	
