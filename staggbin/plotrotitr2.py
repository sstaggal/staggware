#!/usr/bin/env python

from protomof import *
import glob, sys
import pylab
import colorsys

if len(sys.argv) ==1:
	print """Please, check your arguments.
Usage: plotrotitr.py <seriesname> <paramtype>
--------------------------------------
"""
	sys.exit()


seriesname=sys.argv[1]
param=sys.argv[2]

corrfiles=glob.glob(seriesname+"*.corr")
corrfiles.sort()


iters=[]
fig=pylab.figure(1)
hue=0.0
colorincrement=0.66666666666/len(corrfiles)


for corr in corrfiles:
	iteration=int(corr.split(seriesname)[1].split('.')[0])
	
	
	f=open(corr,'r')
	lines=f.readlines()
	f.close()
	rot=[]
	cofx=[]
	cofy=[]
	coa=[]
	for line in lines:
		words=line.split()
		rot.append(float(words[1]))
		cofx.append(float(words[2]))
		cofy.append(float(words[3]))
		coa.append(float(words[4]))

	color=colorsys.hsv_to_rgb(hue,1,1)
	if param=='rot':
		pylab.plot(rot,label=corr, figure=fig, color=color)
	elif param=='cofx':
		pylab.plot(cofx,label=corr, figure=fig, color=color)
	elif param=='cofy':
		pylab.plot(cofy,label=corr, figure=fig, color=color)
	elif param=='coa':
		pylab.plot(coa,label=corr, figure=fig, color=color)
	else:
		print "Please enter rot, cofx, cofy, or coa"
		sys.exit()
	hue+=colorincrement

pylab.legend()
pylab.show()

print "Done!"
