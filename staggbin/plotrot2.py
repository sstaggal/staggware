#!/usr/bin/env python

import sys
import numpy
import pylab

corrfile=sys.argv[1]
param=sys.argv[2]

f=open(corrfile,'r')
lines=f.readlines()
f.close()

rot=[]
cofx=[]
cofy=[]
coa=[]
for line in lines:
	words=line.split()
	rot.append(float(words[1]))
	cofx.append(float(words[2]))
	cofy.append(float(words[3]))
	coa.append(float(words[4]))

if param=='rot':
	pylab.plot(rot)
elif param=='cofx':
	pylab.plot(cofx)
elif param=='cofy':
	pylab.plot(cofy)
elif param=='coa':
	pylab.plot(coa)
else:
	print "Please enter rot, cofx, cofy, or coa"

pylab.show()
print "Done!"
