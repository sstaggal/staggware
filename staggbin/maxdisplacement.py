#!/usr/bin/env python

from protomof import *
import sys
from math import *

if len(sys.argv) !=3:
	print"USAGE:\nmaxdisplacement <tltfile.tlt> <imagesize>\n"
	sys.exit()
	
tltfile=sys.argv[1]
imagesize=int(sys.argv[2])


paramdict,modeldict,name=parseTilt(tltfile)

keys=paramdict.keys()
keys.sort()

cen=imagesize/2
maxdispx=0
maxdispy=0
for key in keys:
	xdisp=abs(paramdict[key]['x']-cen)
	ydisp=abs(paramdict[key]['y']-cen)
	if xdisp > maxdispx:
		maxdispx=xdisp
	if ydisp > maxdispy:
		maxdispy=ydisp
print "Maximum displacement in x is", maxdispx
print "Maximum displacement in y is", maxdispy
print "A good starting boxsize is", int(floor(imagesize-maxdispx)),',',int(floor(imagesize-maxdispy))
