#!/usr/bin/env python

import sys
import pylab

infile=sys.argv[1]
param=sys.argv[2]

f=open(infile,'r')
lines=f.readlines()
f.close()

iteration=[]
ang=[]
rise=[]
for line in lines[1:]:
	words=line.split()
	iteration.append(int(words[0]))
	ang.append(float(words[2]))
	rise.append(float(words[3]))

if param=='ang':
	pylab.plot(iteration,ang,'b+')
elif param=='rise':
	pylab.plot(iteration,rise,'b+')

pylab.show()
print "Done!"
