#!/usr/bin/env python

import os
import math
import shutil
import copy
import random

def parseFrealignParamFile(paramfile):
	"""
	parse a typical FREALIGN parameter file from v8.08
	"""
	if not os.path.isfile(paramfile):
		print "Parameter file does not exist: %s"%(paramfile)

	### cannot assume spaces will separate columns.
	#0000000001111111111222222222233333333334444444444555555555566666666667777777777888888888899999999990000000000
	#1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
	#     24  219.73   84.00  299.39   15.33  -17.51  10000.     1  27923.7  27923.7  -11.41   0.00    0.00
	f = open(paramfile, "r")
	parttree = []
	print "Processing parameter file: %s"%(paramfile)
	for line in f:
		sline = line.strip()
		if sline[0] == "C":
			### comment line
			continue
		words=sline.split()
		
		partdict = {
			'ptclnum': int(words[0]),
			'psi': float(words[1]),
			'theta': float(words[2]),
			'phi': float(words[3]),
			'shx': float(words[4]),
			'shy': float(words[5]),
			'mag': float(words[6]),
			'film': int(words[7]),
			'df1': float(words[8]),
			'df2': float(words[9]),
			'angast': float(words[10]),
			'presa': float(words[11]),
			'dpres': float(words[12])
		}
		parttree.append(partdict)
	f.close()
	if len(parttree) < 2:
		print "No particles found in parameter file %s"%(paramfile)


	print "Processed %d particles"%(len(parttree))
	return parttree

def writeParticleParamLine(particleparams, fileobject):
	p=particleparams
	fileobject.write("%7d%8.3f%8.3f%8.3f%8.3f%8.3f%9.1f%5d%9.1f%9.1f%8.2f%7.2f%8.2f\n" 
		% (p['ptclnum'],p['psi'],p['theta'],p['phi'],p['shx'],p['shy'],p['mag'],
		p['film'],p['df1'],p['df2'],p['angast'],p['presa'],p['dpres']))

def logSplit(start,end,divisions):
	end=math.log(end)
	start=math.log(start)
	incr=(end-start)/divisions
	val=start
	stacklist=[]
	for n in range(0, divisions):
		nptcls=int(round(math.exp(val)))
		stacklist.append(nptcls)
		val+=incr
	print "Making stacks of the following sizes",stacklist
	return(stacklist)	

def modifyjob(jobin,setname,jobout):
	f=open(jobin,'r')
	lines=f.readlines()
	f.close()
	
	path=lines[7][:-1]
	outpath=os.path.join(path,setname)
	
	f=open(jobout,'w')
	for line in lines[:7]:
		f.write(line)
	f.write('%s\n' % (outpath))
	
	for line in lines[8:]:
		f.write(line)

def modifyparams(paramlst, keep, outparam, threshold=65, newpresa=90):
	nptcls=len(paramlst)
	candidates=[]
	newparamlst=copy.deepcopy(paramlst) ### make copy so don't overwrite originals
	
	### find candidate particles with presa less than threshold
	### set all others to newpresa (greater than threshold)
	for n in range(nptcls):
		if newparamlst[n]['presa'] < threshold:
			candidates.append([n,newparamlst[n]['presa']])
		newparamlst[n]['presa']=newpresa
	
	
	### randomize the candidate particles and reset the kept ones to original presa
	random.shuffle(candidates)
	for candidate in candidates[:keep]:
		newparamlst[candidate[0]]['presa']=candidate[1]
	
	### write out the new param file
	f=open(outparam,'w')
	for param in newparamlst:
		writeParticleParamLine(param,f)
	f.close()
			
	
if __name__ == '__main__':
	
	paramfile='params.iter005.par'
	paramlst=parseFrealignParamFile(paramfile)
	nptcls=len(paramlst)
	threshold=65
	sets=logSplit(100,nptcls,20)
	subdirs=[]
	basedir=os.getcwd()
	for set in sets:
		subdirs.append('set_%d' % (set))
		os.mkdir(subdirs[-1])
		os.chdir(subdirs[-1])
		modifyjob(os.path.join(basedir,'frealign.combine.sh'),subdirs[-1],'frealign.combine.sh')
		shutil.copy(os.path.join(basedir,'working.hed'),'working.hed')
		shutil.copy(os.path.join(basedir,'working.img'),'working.img')
		os.system('ln -s /panfs/storage.local/imb/stagg/sstagg/11feb25b/frealign2/start.* .')
		modifyparams(paramlst,set, paramfile)
		
		print "submitting job for ", subdirs[-1]
		os.system('msub -q stagg_q frealign.combine.sh')
		
		os.chdir(basedir)
	
