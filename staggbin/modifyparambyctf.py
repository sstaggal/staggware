#!/usr/bin/env python

import frealignlib
import optparse
import os
import sys
import sinedon
from appionlib import apStack
from appionlib.apCtf import ctfdb

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('--stackid', dest='stackid', type='int', help='primary key of stack in database')
	parser.add_option('--ctfid1', dest='ctfid1', type='int', help='primary key of ctf run in database for session 15jan06b')
	parser.add_option('--ctfid2', dest='ctfid2', type='int', help='primary key of ctf run in database for session 15jan14a')
	parser.add_option('--projectid', dest='projectid', type='int', help='primary key of project in database')
	#parser.add_option('--sets',dest='sets',type='int', default=5, help='number of sets to split the data into')
	#parser.add_option('--setuponly', dest='setuponly', default=False, action='store_true', help="setup without executing")
	parser.add_option('--paramfile',dest='paramfile', help='path to frealign param file to modify, use full path')
	parser.add_option('--newparam',dest='newparam', help='path to output frealign param file')
	#parser.add_option('--stackfile', dest='stackfile', help='path to stack file, use full path')
	#parser.add_option('--jobfile', dest='jobfile', help='path to job file, use full path')
	#parser.add_option('--type', dest='type', help='split type, e.g. imageshift, timestamp, etc')
	#parser.add_option('--threshold', type='float', dest='threshold', help='presa threshold for inclusion in the map. Note this does not modify the jobfile. User should make sure that the values match')
	#parser.add_option('--queue', dest='queue', default='stagg_q', help='queue to submit job to')
	#parser.add_option('--conftype', dest='conftype', default='res80', help='confidence type for titrate confidence option')
#other options
	options, args=parser.parse_args()
	
	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options

def recordLog(args):
	f=open('splitptcls.log','a')
	[f.write('%s '% arg) for arg in args]
	f.write('\n')
	f.close()

	
if __name__=="__main__":
	recordLog(sys.argv)
	options=parseOptions()
	options.new_score=0
	db='ap%d' % (options.projectid)
	sinedon.setConfig('appiondata',db=db)
	
	stackdata=apStack.getStackParticlesFromId(options.stackid)
	params=frealignlib.parseFrealignParamFile(options.paramfile)
	
	f=open(options.newparam,'w')
	currentimage=None
	for ptcl in range(len(stackdata)):
		ptcldata=stackdata[ptcl]
		imagedata=ptcldata['particle']['image']
		sessiondata=ptcldata['particle']['image']['session']
		if sessiondata['name']=='15jan06b':
			ctfdata=ctfdb.getCtfValueForCtfRunId(imagedata,ctfrunid=options.ctfid1)
		elif sessiondata['name']=='15jan14a':
			ctfdata=ctfdb.getCtfValueForCtfRunId(imagedata,ctfrunid=options.ctfid2)
		
		paramdict=params[ptcl]
		if ctfdata is None:
			ctfdata={'defocus1':paramdict['df1']/10**10, 'defocus2': paramdict['df2']/10**10, 'angle_astigmatism': paramdict['angast'], 'acerun':{'name': "Not found"} }
			print "Replacing", currentimage,"with", ctfdata
		if imagedata['filename']!=currentimage:
			print imagedata['filename'], ctfdata['defocus1'],ctfdata['defocus2'],ctfdata['acerun']['name']
			print 'oldctf=', paramdict['df1'],paramdict['df2']
			print 'newctf=', ctfdata['defocus1']*10**10,ctfdata['defocus2']*10**10
			currentimage=imagedata['filename']
		paramdict['df1']=ctfdata['defocus1']*10**10
		paramdict['df2']=ctfdata['defocus2']*10**10
		paramdict['angast']=ctfdata['angle_astigmatism']
		frealignlib.writeParticleParamLine(paramdict,f)
		
	f.close()
		
		
		
		
	
