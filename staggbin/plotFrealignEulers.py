#!/usr/bin/env python
import numpy
from matplotlib import pyplot
import sys
import frealignlib


paramfile=sys.argv[1]
ptcllst=frealignlib.parseFrealignParamFile(paramfile)

psi=[]
theta=[]
phi=[]
for ptcl in ptcllst[0:-1:10]:
	psi.append(ptcl['psi'])
	
	phi.append(ptcl['phi'])
	
	ptcl['theta'] = ptcl['theta']%90
	theta.append(ptcl['theta'])
	
theta=numpy.array(theta, dtype='float32')
phi=numpy.array(phi, dtype='float32')
#print phi
pyplot.polar(phi,theta,'x',color='b')
pyplot.xlabel('phi')
pyplot.ylabel('theta')
#pyplot.savefig('eulers.pdf')
pyplot.show()
