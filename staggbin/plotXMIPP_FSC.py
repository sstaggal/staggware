#!/usr/bin/env python

import numpy
from matplotlib import pyplot
import sys

fscfile=sys.argv[1]

f=open(fscfile)
lines=f.readlines()
f.close()

cols=len(lines[1].split())-1
freq=[]
collist=[]

#initialize column lists
for n in range(cols):
	collist.append([])

for line in lines[1:]:
	words=line.split()
	freq.append(float(words[0]))
	for n in range(cols):
		collist[n].append(float(words[n+1]))

for n in range(len(collist)):
	print type(n)
	label='ref%d' % (n+1)
	pyplot.plot(freq,collist[n], label=label)

pyplot.legend()
pyplot.show()
		
print "Done!"
