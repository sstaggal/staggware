#!/usr/bin/env python

from protomof import *
import glob, sys
import pylab

if len(sys.argv) ==1:
	print """Please, check your arguments.
Usage: plotazimuth.py <seriesname> 
--------------------------------------
"""
	sys.exit()


seriesname=sys.argv[1]

tiltfiles=glob.glob(seriesname+"*-fitted.tlt")
tiltfiles.sort()


iters=[]
azlist=[]
for tilt in tiltfiles:
	iteration=int(tilt.split('-')[1])
	imagedict,paramdict,seriesname=parseTilt(tilt)
	azlist.append(paramdict['azimuth'])
	iters.append(iteration)

pylab.plot(iters,azlist)

pylab.show()

print "Done!"
