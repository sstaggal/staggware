#!/usr/bin/env python

import sys
from matplotlib import pyplot

f=open(sys.argv[1])
lines=f.readlines()
f.close()

data=False
zScore=[]
llc=[]
maxp=[]
for line in lines:
	words=line.split()
	if len(words) > 0 and data is False:
		for word in words:
			if '@' in word:
				data=True
				print "starting data"
				break
	if len(words) >0 and data is True:
		zScore.append(float(words[-1]))
		maxp.append(float(words[-2]))
		llc.append(float(words[-3]))

pyplot.figure()
pyplot.subplot(221)
pyplot.hist(zScore,100, range=[0,2])
pyplot.title('zScore')

pyplot.subplot(222)
pyplot.hist(llc,100)
pyplot.title('Log Likelihood')

pyplot.subplot(223)
pyplot.hist(maxp,100,[0,500])
pyplot.title('Max Prob')

pyplot.subplot(224)
pyplot.plot(zScore,maxp, 'bo')
pyplot.xlim(0,2)
pyplot.title('Z vs Max Prob')

pyplot.show()

