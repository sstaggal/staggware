#!/usr/bin/env python

import sinedon
from appionlib import apStack
from appionlib.apCtf import ctfdb
import frealignlib
from appionlib import apDefocalPairs 
from appionlib import apDatabase 

if __name__ == '__main__':
	sinedon.setConfig('appiondata',db='ap571')
	stackdata=apStack.getStackParticlesFromId(31)
	params=[]
	ptclnum=0
	currentmicrograph=None
	micrographn=0
 	for ptcl in stackdata[:200]:
		ptclnum+=1
		pd={}
		pd['ptclnum']=ptclnum
		pd['psi']=0.0
		pd['theta']=0.0
		pd['phi']=0.0
		pd['shx']=0.0
		pd['shy']=0.0
		pd['mag']=10000
		if ptcl['particle']['image']['filename'] !=currentmicrograph:
			micrographn+=1
			currentmicrograph=ptcl['particle']['image']['filename']
		pd['film']=micrographn
		sibs=apDefocalPairs.getAllSiblings(ptcl['particle']['image'])
		for sib in sibs:
			if 'exc-a' in sib['filename']:
				ctf=ctfdb.getBestCtfValue(sib,sortType='res50')
				pd['df1']=ctf['defocus1']*1.0e10
				pd['df2']=ctf['defocus2']*1.0e10
				pd['angast']=ctf['angle_astigmatism']

		pd['occ']=100
		pd['logp']=0.0
		pd['sigma']=0.0
		pd['score']=0.0
		pd['change']=0.0
		params.append(pd)
	#print params
	
	f=open('params.par','w')
	for param in params:
		frealignlib.writeParticleParamLine(param, f)
	f.close()
	apix=apDatabase.getPixelSize(ptcl['particle']['image'])
	print 'Pixel size =',apix,'apix'
	print "Done!"
