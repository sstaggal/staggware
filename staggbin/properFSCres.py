#!/usr/bin/env python

import optparse
import os
import sys

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-e', dest='e', type='str', help='even volume')
	parser.add_option('-o', dest='o', type='str', help='odd volume')
	parser.add_option('-a', dest='a', type='float', help='apix')
	parser.add_option('-r', dest='r', type='int', help='radius (pixels)')
	parser.add_option('-t', dest='t', type='float', help='threshold')
	options, args=parser.parse_args()
	
	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options


	
if __name__=="__main__":
	options=parseOptions()
	e=options.e
	eroot=e.split('.')[0]
	eout=eroot+'.m.mrc'
	o=options.o
	oroot=o.split('.')[0]
	oout=oroot+'.m.mrc'
	f=open('properFSCres.log','a')
	[f.write('%s '% arg) for arg in sys.argv]
	f.write('\n')
	f.close()
	
	
	ecommand='e2proc3d.py --process=mask.auto3d:radius=%d:nshells=5:threshold=%f:nshellsgauss=5 %s %s' % (options.r, options.t,e,eout)
	ocommand='e2proc3d.py --process=mask.auto3d:radius=%d:nshells=5:threshold=%f:nshellsgauss=5 %s %s' % (options.r, options.t,o,oout)
	
	print ecommand
	os.system(ecommand)
	print ocommand
	os.system(ocommand)
	
	fsc='%s_%s.eotest' % (eout, oout)
	fsccommand='proc3d %s %s fsc=%s' % (eout, oout, fsc)
	print fsccommand
	os.system(fsccommand)
	
	f=open(fsc)
	lines=f.readlines()
	f.close()
	
	freq=[]
	corr=[]
	for line in lines:
		words=line.split()
		freq.append(int(words[0]))
		corr.append(float(words[1]))
	
	for n in range(len(freq)):
		if corr[n] < 0.5:
			freq0_5=(freq[n]+freq[n-1])/2.0
			break
			
	for n in range(len(freq)):
		if corr[n] < 0.143:
			freq0_143=(freq[n]+freq[n-1])/2.0
			break
	
	res0_5=len(freq)*2*options.a/freq0_5
	res0_143=len(freq)*2*options.a/freq0_143
	
	print "Resolution at FSC 0.5=", res0_5
	print "Resolution at FSC 0.143=", res0_143
	
	
