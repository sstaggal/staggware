#!/usr/bin/env python

import glob
import os
from matplotlib import pyplot
import numpy

def parseCombine(combineout):
	f=open(combineout)
	lines=f.readlines()
	f.close()
	
	n=0
	while n <  len(lines):
		line=lines[n]
		if line[0]=='C':
			words=line.split()
			if words[1:3]==['NO.','RESOL'] and 'CC' in words:
				n+=1
				line=lines[n]
				ns=[]
				fsc=[]
				res=[]
				while line[0]=='C':
					line=lines[n]
					words=line.split()
					if words[1]=='Average':
						break
					ns.append(float(words[1]))
					res.append(float(words[2]))
					fsc.append(float(words[5]))
					n+=1
				break
		n+=1
	ns=numpy.array(ns)
	res=numpy.array(res)
	fsc=numpy.array(fsc)
	return ns, res, fsc

sets=glob.glob('set_*')

basedir=os.getcwd()
fscdict={}
nptcls=[]
respoint5=[]
for set in sets[:-1]:
	os.chdir(set)
	print set
	ns, res, fsc = parseCombine('frealign.combine.out')
	setn=int(set.split('_')[-1])
	fscdict[setn]=(ns,res,fsc)
	#pyplot.plot(1/res,fsc)
#	locs,labs=pyplot.xticks()
#	labs=1/locs
#	pyplot.xticks(locs,labs)
	os.chdir(basedir)
	
	for f in range(len(fsc)):
		#print fsc[f]
		if fsc[f] < 0.5:
			fa=fsc[f-1]
			fb=fsc[f]
			frac=(fa-0.5)/(fa-fb)
			#print fa,fb, fa-fb, (fa-0.5)/(fa-fb)
			ra=res[f-1]
			rb=res[f]
			nptcls.append(setn)
			respoint5.append(ra-((ra-rb)*frac))
			break
respoint5=numpy.array(respoint5)
nptcls=numpy.array(nptcls)		
#pyplot.plot(numpy.log(nptcls),1/respoint5)	
pyplot.plot(nptcls,respoint5)
pyplot.show()
