#!/usr/bin/env python

import EMAN, sys, numpy, math

volname=sys.argv[1]
apix=sys.argv[2]

vol=EMAN.EMData()
vol.readImage(volname,-1)

boxsize=vol.xSize()
center=boxsize/2

#f=open(
radavg=numpy.zeros(center+1)
nvals=numpy.zeros(center+1)
for x in range(0,boxsize):
	print x
	for y in range(0,boxsize):
		for z in range(0, boxsize):
			dx=x-center
			dy=y-center
			dz=z-center
			d=math.sqrt(dx*dx+dy*dy+dz*dz)
			d=round(d)
			d=int(d)
			#print d
			if d > center:
				continue
			else:
				val=vol.valueAt(x,y,z)
				radavg[d]=radavg[d]+val
				nvals[d]=nvals[d]+1
radavg=radavg/nvals
 
 
