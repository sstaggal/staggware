#!/usr/bin/env python

from protomof import *
import glob, sys
import pylab
import colorsys

if len(sys.argv) ==1:
	print """Please, check your arguments.
Usage: plotrot.py <tiltfile> 
--------------------------------------
"""
	sys.exit()


tiltfile=sys.argv[1]


fig=pylab.figure(1)

imagedict,paramdict,seriesname=parseTilt(tiltfile)
rot=[]
angle=[]	
for img in range(1,len(imagedict)+1):
	angle.append(imagedict[img]['tilt'])
	rot.append(imagedict[img]['rotation'])
color=(0,0,1)
pylab.plot(angle,rot, label=tiltfile, figure=fig, color=color)

pylab.legend()
pylab.show()

print "Done!"
