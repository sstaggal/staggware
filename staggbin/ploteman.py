#!/usr/bin/env python

import numpy
from matplotlib import pyplot
import sys

fontsize=20
apix=float(sys.argv[1])
outfilename=sys.argv[2]
files=sys.argv[3:]
pyplot.figure(figsize=(7,7), dpi=100)
for fname in files:
	print fname
	f=open(fname)
	lines=f.readlines()
	f.close()

	freq=[]
	corr=[]
	for line in lines:
		words=line.split()
		freq.append(int(words[0]))
		corr.append(float(words[1]))

	freq=numpy.array(freq)
	corr=numpy.array(corr)
	
	pyplot.plot(freq,corr, linewidth=2.5)

pyplot.plot([0,freq[-1]],[0.143,0.143], linewidth=1, linestyle='--',color='black')
pyplot.xlim(0,len(lines))
pyplot.xticks(numpy.linspace(0,len(lines),6,endpoint=True))
pyplot.ylim(-0.025,1)
pyplot.yticks(numpy.linspace(0,1,6,endpoint=True))
locs,labs=pyplot.xticks()
labs=len(lines)*2*apix/locs
print labs, type(labs), labs.dtype
labs=[ '%.2f' % x for x in labs]
pyplot.xticks(locs,labs, fontsize=fontsize)
pyplot.yticks(fontsize=fontsize)

pyplot.ylabel("FSC", fontsize=fontsize)
pyplot.xlabel("Resolution",fontsize=fontsize)
pyplot.savefig(outfilename)
#pyplot.show()
