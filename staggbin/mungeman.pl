#!/usr/bin/perl

%mach=();
use Cwd;

if ( $#ARGV > 0 ) {
   die "Usage: mungeman machfile\n";
}

if ( $#ARGV == 0 ) {
   open(STDIN, "< $ARGV[0]") || die "Cannot open input file";
}

while ( my $line = <STDIN>) {
   chomp ($line );
   @args = split ' ', $line;
   if ( $args[0] =~ /:/ ) {
      @args = split ':', $line;
   }
   $mach{ $args[0] }++;
}
close(STDIN);

while ( my ($key, $value) = each(%mach) ) {
        printf("ssh %d 1 %s %s\n", $value, $key, &Cwd::cwd());
}
