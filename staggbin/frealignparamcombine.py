#!/usr/bin/env python

import glob
import sys
import frealignlib

def combineParameterFiles(params):
	"""
	combine all the parameter files
	"""
	outpar = open(params['outpar'], 'w')
	totparams=0
	for p in params['outparlst']:
		f=open(p)
		lines=f.readlines()
		f.close()
		for line in lines:
			if line[0]!='C':
				outpar.write(line)
				totparams+=1
	outpar.close()
	if totparams!=params['last']:
		print "Error: The number of particle parameters (",totparams,")does not equal the total number of particles(",params['last'],")"
		sys.exit()

def sortf(x,y):
	xp=int(x.split('.')[-1])
	yp=int(y.split('.')[-1])
	return xp-yp

itr=int(sys.argv[1])
nptcls=int(sys.argv[2])

paramfile='params.iter%03d.par' % (itr)
pfiles=glob.glob(paramfile+'.*')

pfiles.sort(cmp=sortf)

params={'outparlst':pfiles ,'outpar':paramfile,'last':nptcls}

combineParameterFiles(params)
	
#print d
