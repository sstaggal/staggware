#!/usr/bin/env python

import csv
import numpy as np
from pyami import mrc
import sys
import math
import os, errno
import subprocess
import argparse
import fileinput
import shutil
import logging

parser = argparse.ArgumentParser()

parser.add_argument('-s','--star_in', action='store', dest='star_in',
                    help='      initial star file for the aligned model')
parser.add_argument('-m','--model', action='store', dest='model',
                    help='      aligned model to delete')
parser.add_argument('-a','--angpix', action='store', dest='angpix',
                    help='      angstroms per pixel')
parser.add_argument('-b','--box_size', type=int, action='store', dest='cut_out_size',
                    help='      Size of box to extract, pixels, default is 3/8 of box size')
parser.add_argument('-x','--x_start', type=int, action='store', dest='x_start',
                    help='      center of box to extract in x at phi=0, pixels, IMOD coordinates, default is 3/4 of box size')
parser.add_argument('-n','--n_boxes', type=int, action='store', dest='n_boxes', default=4,
                    help='      Number of boxes to extract around axis, convenient if 360 is divisable by n_boxes, defualt is 9')
parser.add_argument('-z','--z_step', type=int, action='store', dest='z_step',
                    help='      Z step from center of initially picked box, default is 1/4 of box ')
parser.add_argument('-c','--cutout', action='store_true', default=False,
                    dest='cutouts',
                    help='      cutout boxes to extract on tubes before extraction, for looking at the tubes themselves, not just decorations')
parser.add_argument('-o', '--output', action='store', dest='output_rootname',
                     help='     output rootname for star/mrcs, default is extracted_particles')

parser.add_argument('-k', '--keep_scratch', action='store_true', default=False,
                    dest='keep_scratch',
                    help='      keep scratch folder, default is False, only final star, mrcs, and reconstruction output on default')


parser.add_argument('-t','--test', action='store_true', default=False,
                    dest='test_TF',
                    help='	option for testing, dont use, was needed during setup')


results = parser.parse_args()

if results.star_in == None:
        print 'Required arguments: --star_in <star file> --model <model file> -- angpix <angpix>; use -h or --help to see all options'
        sys.exit()

if results.model == None:
        print 'Required arguments: --star_in <star file> --model <model file> -- angpix <angpix>; use -h or --help to see all options'
        sys.exit()


if results.angpix == None:
        print 'Required arguments: --star_in <star file> --model <model file> -- angpix <angpix>; use -h or --help to see all options'
        sys.exit()
#get box size first because will make defaults for other arguments from it:

input_mrc= mrc.read(results.model)
box_size = int(input_mrc.shape[0])

#set defaults

if results.x_start == None:
        x_start = int(round((float(box_size)*3)/4))
else:
        x_start = results.x_start

if results.z_step == None:
        z_step = int(round((float(box_size))/4))
else:
        z_step = results.z_step

if results.n_boxes == None:
        n_boxes = 9
else:
        n_boxes = results.n_boxes

if results.cut_out_size == None:
        cut_out_size = int(round((float(box_size)*3)/8))
else:
        cut_out_size = results.cut_out_size
if results.output_rootname == None:
        output_rootname = 'extracted_particles'
else:
        output_rootname = results.output_rootname
#print inputs





logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='extracted_particles.log',
                    filemode='w')
#define a handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
#set format for which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
#tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


#other logger

logger1 = logging.getLogger('Making models              ')
logger2 = logging.getLogger('Subtraction                        ')
logger3 = logging.getLogger('Extraction                 ')
logger4 = logging.getLogger('Creating final files       ')




logging.info('star in          ='+ results.star_in)
logging.info('model to delete  ='+ results.model)
logging.info('angpix           ='+ results.angpix)
logging.info('x_start          ='+ str(x_start))
logging.info('cut_out_size     ='+ str(cut_out_size))
logging.info('n boxes to cut   ='+ str(n_boxes))
logging.info('z step           ='+ str(z_step))
logging.info('cutouts          ='+ str(results.cutouts))
logging.info('output rootname  ='+ output_rootname)
logging.info('keep scratch     ='+ str(results.keep_scratch))
logging.info('test	       ='+ str(results.test_TF))














##get all the pieces in
initial_star_in = results.star_in
angpix = str(results.angpix)
number_of_cutouts = float(n_boxes)
angle_to_add = float(360)/number_of_cutouts









##Functions
#Make a directory
def make_tmp(directory):
	try:
		os.makedirs(directory)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise



#phi rotation of box to get center coord takes IMOD coordinates
def z_rotate(coord_0, angle, box_0):
	c0=np.array([coord_0]).reshape(3,1)-(box_0/2)
	a = -np.deg2rad(float(angle))
        rot_matrix=np.array([\
        np.cos(a), -np.sin(a), 0, \
        np.sin(a), np.cos(a), 0, \
        0, 0, 1]).reshape((3,3))
        c1=np.dot(rot_matrix, c0)
	c1=np.round(c1)
	return c1

#generating rotation matrix 
def full_rotate(phi, theta, psi1):
        r=np.deg2rad(float(phi))
        t=np.deg2rad(float(theta))
        p=np.deg2rad(float(psi1))
        rm=np.array([\
        np.cos(p)*np.cos(t)*np.cos(r)-np.sin(p)*np.sin(r), -np.cos(r)*np.sin(p)-np.cos(p)*np.cos(t)*np.sin(r), np.cos(p)*np.sin(t), \
        np.cos(p)*np.sin(r)+np.cos(t)*np.cos(r)*np.sin(p), np.cos(p)*np.cos(r)-np.cos(t)*np.sin(p)*np.sin(r), np.sin(p)*np.sin(t), \
        -np.cos(r)*np.sin(t), np.sin(t)*np.sin(r), np.cos(t)]).reshape((3, 3))
        return rm

#rounds up
def roundup(x):
        return int(math.ceil(x / 10.0)) * 10

#counts the number of models in a  directory - ended up not using
def models_count(directory):
	a = len([f for f in os.listdir(directory)
		if f.endswith('.mrc') and os.path.isfile(os.path.join(directory, f))])
	return a

#makes a list of all the files in a directory with the extension extens (given as a string, no period)
def files_list(directory, extens):
	files_in_dir = []
	for root, dirs, files in os.walk(directory):
		for file in files:
			if file.endswith('.'+ extens ):
				files_in_dir.append(file)
	return files_in_dir


def files_list_dash(directory, extens):
        files_in_dir = []
        for root, dirs, files in os.walk(directory):
                for file in files:
                        if file.endswith('_'+ extens ):
                                files_in_dir.append(file)
        return files_in_dir


#takes in a star file and outputs a list with (column name, column number -1 (so is ready for python)
def parse_star(file):
        ssvin = open(file, 'rb')
        reader=csv.reader(ssvin, delimiter=' ')
        number_of_arguments = 0
        argument_list = []
        for row in reader:
                row = filter(None, row)
                if len(row) == 2:
                        number_of_arguments += 1
                        num = row[1]
                        num =int(num.replace("#",""))
                        argument_list.append((row[0], num-1))
        ssvin.close()
        return argument_list


#makes a zero box in a larger box of ones, coordinates from 3dmod (basically corner as origin)
#should be 'box mask=np.ones' and 'start[2]+k]=0' if opposite is because testing
def make_box(box, x_y_z_center_point, cut_out):
	if results.test_TF is True:
		x_y_z_starting_point = x_y_z_center_point-(cut_out/2)
		box_mask=np.zeros((box,box,box),dtype='float32')
		start=np.array((x_y_z_starting_point[2],x_y_z_starting_point[1],x_y_z_starting_point[0]),dtype='int')
		size=np.array((cut_out,cut_out,cut_out),dtype='int')
		for i in range(size[0]+1):
			for j in range(size[1]+1):
				for k in range(size[2]+1):
					box_mask[start[0]+i, start[1]+j, start[2]+k]=1
	else:
		x_y_z_starting_point = x_y_z_center_point-(cut_out/2)
		box_mask=np.ones((box,box,box),dtype='float32')
		start=np.array((x_y_z_starting_point[2],x_y_z_starting_point[1],x_y_z_starting_point[0]),dtype='int')
		size=np.array((cut_out,cut_out,cut_out),dtype='int')
		for i in range(size[0]+1):
			for j in range(size[1]+1):
				for k in range(size[2]+1):
					box_mask[start[0]+i, start[1]+j, start[2]+k]=0

	return box_mask

#run a command, argument is a string
def run_command(command):
	process=subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output, error = process.communicate()

##Like make box but extracts and takes coord with origin in center
def extract_box(mrcs_i, mrcs_o, box, x_y_z_center_point, cut_out, x_shift, y_shift, particle_number):
	x_y_z_starting_point=x_y_z_center_point+box/2-(cut_out/2)
	start=np.array((int(round(float(x_y_z_starting_point[1])-y_shift)), int(round(float(x_y_z_starting_point[0])-x_shift))), dtype='int')
	size=np.array((int(cut_out), int(cut_out)), dtype='int')
	for i in range(size[0]+1):
		if start[0]+i <= (box-1):
			for j in range(size[1]+1):
				if start[1]+j <= (box-1):
					mrcs_o[particle_number, start[0]+i, start[1]+j]=mrcs_i[particle_number,start[0]+i,start[1]+j]
	
scratch = 'scratch'
scratch_num = 2




if os.path.isdir(scratch) is True:
        while os.path.isdir(scratch) is True:
                if scratch_num == 2:
                        scratch = scratch+str(scratch_num)
                else:
                        scratch = scratch.replace((str(int(scratch_num)-1)),scratch_num)
                scratch_num+=1


make_tmp(scratch)
if results.cutouts is True:
	##MAKE CUTOUTS!!

	xyz_center = np.array([int(x_start), box_size/2, box_size/2])
	xyz_center_list = []
	z_tester=xyz_center[2]-cut_out_size/2
	while z_tester >= 0:
		z_tester = z_tester - z_step
        xyz_center[2] = z_tester + z_step + cut_out_size/2
        number_of_cuts = 0
        while xyz_center[2]+(cut_out_size/2) <= box_size:
		mask=make_box(box_size, xyz_center, cut_out_size)

                ##Multiply arrays
                final_array=np.multiply(input_mrc, mask)

                ##Write final mrc
                mrc_to_write=scratch+'/z_'+str(number_of_cuts)+'_phi_000_.mrc'
		mrc.write(final_array,mrc_to_write)
		logger1.info(mrc_to_write) 
		#make all the rotations for position z
		angle = 0-angle_to_add
                while abs(angle) < 360:
                        if len(str(int(abs(angle)))) == 3:
                                mrc2=scratch+'/z_'+str(number_of_cuts)+'_phi_'+ str(int(abs(angle)))+'_.mrc'
                                eman_run = "proc3d "+mrc_to_write+' '+mrc2+' rot=0,'+str(angle)+',0'
                        elif len(str(int(abs(angle)))) == 2:
                                mrc2=scratch+'/z_'+str(number_of_cuts)+'_phi_0'+ str(int(abs(angle)))+'_.mrc'
                                eman_run = "proc3d "+mrc_to_write+' '+mrc2+' rot=0,'+str(angle)+',0'
                        elif len(str(int(abs(angle)))) == 1:
                                mrc2=scratch+'/z_'+str(number_of_cuts)+'_phi_00'+ str(int(abs(angle)))+'_.mrc'
                                eman_run = "proc3d "+mrc_to_write+' '+mrc2+' rot=0,'+str(angle)+',0'
                        logger1.info(mrc2)
			run_command(eman_run)
			angle=angle-angle_to_add
		xyz_center_list.append((xyz_center[0],xyz_center[1],xyz_center[2]))
		xyz_center[2]  = xyz_center[2] + z_step
		number_of_cuts += 1
        logging.info('Done making models to subtract!')
	logging.info('')

#Subtract the cutouts from the particles

	logger2.info('Starting relion subtractions')	
        mrc_list = files_list(scratch, 'mrc')
        mrc_in_scratch = int(len(mrc_list))

        model_number = int(0)
        while model_number != mrc_in_scratch:
                mrcs_out=scratch+'/'+mrc_list[model_number].split('.')[0]+'particles'
		if results.test_TF is False:
			relion_subtract_run= 'relion_project --i '+scratch+'/'+mrc_list[model_number]+' --o '+mrcs_out+ ' --angpix '+ angpix + ' --ang '+ initial_star_in +' --ctf --maxres 15 --subtract_exp'
		else:
			relion_subtract_run= 'relion_project --i '+scratch+'/'+mrc_list[model_number]+' --o '+mrcs_out+ ' --angpix '+ angpix + ' --ang '+ initial_star_in
                logger2.info('Job '+str(model_number+1)+':'+relion_subtract_run)
                run_command(relion_subtract_run)
                logger2.info('Job '+str(model_number+1)+' done!')
		model_number+=1
	if results.keep_scratch is False:
		for scratch_model in mrc_list:
			os.remove(scratch+'/'+scratch_model)
	logging.info('Done with subtractions')


	
######Extraction of boxes
	##read in star file and mrc file
	logging.info('')
	logger3.info('Extracting particles')
	mrcs_list=files_list(scratch, 'mrcs')
	mrcs_in_scratch=int(len(mrcs_list))
	stack_number=0
	while stack_number != mrcs_in_scratch:
		rn_out=scratch+'/'+mrcs_list[stack_number].split('.')[0]+'_extracted'
		star_out=rn_out+'.star'
		mrcs_out=rn_out+'.mrcs'
		tsvout=open(star_out,'wb')
		writer=csv.writer(tsvout, delimiter=' ')
		star_in=scratch+'/'+mrcs_list[stack_number].split('.')[0]+'.star'
		mrcs_in=scratch+'/'+mrcs_list[stack_number]
		tsvin=open(star_in, 'rb')
		reader=csv.reader(tsvin, delimiter=' ')
		mrcs_to_read=mrc.read(mrcs_in)

	#Get values for later on (mean, number of images, box size, etc)
		mean_all=np.mean(mrcs_to_read)
		mean_list_individuals=np.mean(mrcs_to_read, axis=(1,2))
		particle_number=int(mrcs_to_read.shape[0])
		box_size_extract=roundup(math.sqrt((float(cut_out_size)**2)*2))
		x_y_shift=(box_size/2)-(box_size_extract/2)
		phi_of_box = mrcs_list[stack_number].split('_')[3]

	#The center of the square to transform (i.e. what we are transforming then using as coordinates to make new square), maybe set to automate?? depends on number of runs needed, currently just enter the exact coordinates used in the make_cube.py command into init_center_square (same order and everything
		center_square=z_rotate(xyz_center_list[int(mrcs_list[stack_number].split('_')[1])], phi_of_box, box_size)
	#parsing the star file
		star_arguments=parse_star(star_in)
		for col in star_arguments:
			if col[0] == '_rlnOriginX':
				OX = col[1]
		for col in star_arguments:
			if col[0] == '_rlnOriginY':
				OY = col[1]
		for col in star_arguments:
			if col[0] == '_rlnImageName':
				IName = col[1]
                for col in star_arguments:
                        if col[0] == '_rlnAngleRot':
                                AROT = col[1]
                for col in star_arguments:
                        if col[0] == '_rlnAngleTilt':
                                ATILT = col[1]
                for col in star_arguments:
                        if col[0] == '_rlnAnglePsi':
                                APSI = col[1]

		row_number=0

		temp_mrcs_array=np.zeros((mrcs_to_read.shape),dtype='float32')
		logger3.info('Stack '+str(stack_number+1)+' of '+str(mrcs_in_scratch))
		for row in reader:
	#get rid of empty columns
			row = filter(None, row)
	#select the rows that have particle information
			if len(row) == len(star_arguments):
			#generate rotation matrix	
				rot_matrix=full_rotate(float(row[AROT]), float(row[ATILT]), float(row[APSI]))
				end_position=np.dot(rot_matrix, center_square)
				extract_box(mrcs_to_read, temp_mrcs_array, box_size, end_position, box_size_extract, float(row[OX]), float(row[OY]), row_number)
			#write out star file with aligned y shift and rotation, new image name
				row[IName]= str(row_number+1)+'@'+mrcs_out
				row[OY] = float(row[OY])-float(center_square[2])
				row[AROT] = float(row[AROT]) - float(phi_of_box)
				writer.writerow(row)	
#				logger4.info('Particle '+str(row_number+1)+' of '+str(particle_number))
				row_number+=1
		#right header for star file
			else:
				writer.writerow(row)
		mrc.write(temp_mrcs_array, mrcs_out)
	
		normalize_cmd = 'e2proc2d.py '+mrcs_out+' '+rn_out+'_norm.mrcs --process=normalize'
		logger3.info('Normalizing new stack')
		logger3.info(normalize_cmd)
		run_command(normalize_cmd)
		logger3.info('Finished normalization, replacing old stack')
		mv_cmd = 'mv '+rn_out+'_norm.mrcs '+mrcs_out
		run_command(mv_cmd)
		logger3.info('Complete')
		tsvout.close()
		tsvin.close()
		stack_number+=1
	if results.keep_scratch is False:
		for scratch_mrcs in mrcs_list:
			os.remove(scratch+'/'+scratch_mrcs)
			os.remove(scratch+'/'+scratch_mrcs.split('.')[0]+'.star')
	
	## ZYZ Rotation matrix based of Euler angles 3 for initial Z(rot), 2 for Y(tilt), and 1 for final Z(psi), courtesy of wikipedia
	#Z(1)Y(2)Z(3)=	[	c1c2c3-s1s3,	-c3s1-c1c2s3,	c1s2	]
	#		[	c1s3+c2c3s1,	c1c3-c2s1s3,	s1s2	]
	#		[	-c3s2,		s2s3,		c2	]
	#	else:
	#		writer.writerow(row)




if results.cutouts is False:
	
	xyz_center = np.array([int(x_start), box_size/2, box_size/2])
        xyz_center_list = []
        z_tester=xyz_center[2]-cut_out_size/2
        while z_tester >= 0:
                z_tester = z_tester - z_step
        xyz_center[2] = z_tester + z_step + cut_out_size/2
        number_of_cuts = 0

#Subtract the cutouts from the particles

        logging.info('Starting relion subtraction')
	if results.test_TF is True:
		relion_subtraction_run='relion_project --i '+results.model+' --o scratch/subtracted_particles --angpix '+angpix+' --ang ' +initial_star_in
	else:
		relion_subtraction_run='relion_project --i '+results.model+' --o '+scratch+'/subtracted_particles --angpix '+angpix+' --ang ' +initial_star_in+' --subtract_exp --ctf --maxres 15' 
	logger2.info(relion_subtraction_run)
	run_command(relion_subtraction_run)
	
	
	star_in = scratch+'/subtracted_particles.star'
	mrcs_in = scratch+'/subtracted_particles.mrcs'
	star_arguments = parse_star(star_in)
	for col in star_arguments:
		if col[0] == '_rlnOriginX':
			OX = col[1]
	for col in star_arguments:
		if col[0] == '_rlnOriginY':
			OY = col[1]
	for col in star_arguments:
		if col[0] == '_rlnImageName':
			IName = col[1]
	for col in star_arguments:
		if col[0] == '_rlnAngleRot':
			AROT = col[1]
	for col in star_arguments:
		if col[0] == '_rlnAngleTilt':
			ATILT = col[1]
	for col in star_arguments:
		if col[0] == '_rlnAnglePsi':
			APSI = col[1]


	while xyz_center[2]+(cut_out_size/2) <= box_size:
		mrcs_to_read=mrc.read(mrcs_in)
		mean_all=np.mean(mrcs_to_read)
		mean_list_individuals=np.mean(mrcs_to_read, axis=(1,2))
		particle_number=int(mrcs_to_read.shape[0])
		box_size_extract = roundup(math.sqrt((float(cut_out_size)**2)*2))
		x_y_shift = (box_size/2)-(box_size_extract/2)
		angle=0
		while abs(angle) < 360:
			
			logger3.info('Extraction z'+str(number_of_cuts)+'_phi_'+str(int(abs(angle))))
			rn_out=scratch+'/z_'+str(number_of_cuts)+'_phi_'+str(int(abs(angle)))+'_particles_extracted'
			mrcs_out=rn_out+'.mrcs'
			star_out=rn_out+'.star'
			tsvin=open(star_in, 'rb')
			tsvout=open(star_out, 'wb')
			writer=csv.writer(tsvout, delimiter=' ')
			reader=csv.reader(tsvin, delimiter=' ')
			center_square=z_rotate(xyz_center, angle, box_size)
			row_number=0
			temp_mrcs_array=np.zeros((mrcs_to_read.shape),dtype='float32')
			for row in reader:
				row= filter(None, row)
				if len(row) == len(star_arguments):
					


					rot_matrix=full_rotate(float(row[AROT]), float(row[ATILT]), float(row[APSI]))
					end_position=np.dot(rot_matrix, center_square)
					extract_box(mrcs_to_read, temp_mrcs_array, box_size, end_position, box_size_extract, float(row[OX]), float(row[OY]), row_number)
					#Write out star file with aligned y shift and rotation, new image name
					row[IName]= str(row_number+1)+'@'+mrcs_out
					row[OY] = float(row[OY])-float(center_square[2])
					row[AROT] = float(row[AROT]) - float(angle)
					writer.writerow(row)


#					logger4.info('Particle '+str(row_number+1)+' of '+str(particle_number))
					row_number += 1
				else:
					writer.writerow(row)
			mrc.write(temp_mrcs_array, mrcs_out)
			tsvout.close()
			normalize_cmd = 'e2proc2d.py '+mrcs_out+' '+rn_out+'_norm.mrcs --process=normalize'
			logger3.info('Normalizing new stack')
			logger3.info(normalize_cmd)
			run_command(normalize_cmd)
			logger3.info('Finished normalization, replacing old stack')
			mv_cmd = 'mv '+rn_out+'_norm.mrcs '+mrcs_out
			run_command(mv_cmd)
			logger3.info('Complete')
			angle=angle+angle_to_add
		xyz_center[2] = xyz_center[2] + z_step
		number_of_cuts+=1
		tsvin.close()
	if results.keep_scratch is False:
		os.remove(star_in)
		os.remove(mrcs_in)


	#Make one stack
logger4.info('Making single output star/stack')
star_list=files_list(scratch, 'star')
print star_list
star_list = [final_stars for final_stars in star_list if final_stars.split('_')[-1] == 'extracted.star']
print star_list
logger4.info('To combine:')
for final_stars in star_list:
	logger4.info(final_stars)		
		
star_in_scratch=int(len(star_list))
tmp_star_out=scratch+'/tmp_particles.star'



starout=open(tmp_star_out,'wb')
writer2=csv.writer(starout, delimiter=' ')
star_number=0
while star_number != star_in_scratch:

	star=scratch+'/'+star_list[star_number]
	star_columns=parse_star(star)
	readFile = open(star)
	lines = readFile.readlines()
	readFile.close()
	w = open(star, 'w')
	w.writelines([item for item in lines[:-1]])
	w.close()
	starin=open(star, 'rb')
	reader2=csv.reader(starin, delimiter=' ')
	if star_number == 0:
		for row in reader2:
			row = filter(None, row)
			writer2.writerow(row)
	else:
		for row in reader2:
			row = filter(None, row)
			if len(row) == len(star_columns):
				writer2.writerow(row)
	starin.close()
	star_number+=1
	logger4.info('Stack '+str(star_number)+' of '+str(star_in_scratch))
starout.close()
relion_preprocess_run='relion_preprocess --operate_on scratch/tmp_particles.star --operate_out '+output_rootname
logger4.info('Running relion_preprocess job: '+relion_preprocess_run)
run_command(relion_preprocess_run)
run_command('mv '+output_rootname+'.mrcs.mrcs '+output_rootname+'.mrcs')
logging.info('Preprocess finished, final outputs written to:')
logging.info('star:    '+output_rootname+'.star')
logging.info('stack:   '+output_rootname+'.mrcs')
logging.info('Creating reconstruction of extracted particles:')
if results.test_TF is True:
	relion_reconstruct_command='relion_reconstruct --i '+output_rootname+'.star --angpix '+angpix+' --o '+output_rootname+'.mrc'
else:
	relion_reconstruct_command='relion_reconstruct --i '+output_rootname+'.star --angpix '+angpix+' --o '+output_rootname+'.mrc --ctf --maxres 15'
run_command(relion_reconstruct_command)
logging.info('Created output model: '+output_rootname+'.mrc')
if results.keep_scratch is False:
	shutil.rmtree(scratch)
