#!/usr/bin/env python

from pyami import mrc
import sys
from matplotlib import pyplot

for vol in sys.argv[1:]:
	v=mrc.read(vol)
	s=v.shape[0]
	p=v[s/2][s/2][s/2:]
	pyplot.plot(p)

pyplot.show()
