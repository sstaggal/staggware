#!/usr/bin/env python

import sys
from matplotlib import pyplot

cutoff=float(sys.argv[3])
f=open(sys.argv[1])
lines=f.readlines()
f.close()

newf=open(sys.argv[2],'w')
data=False
zScore=[]
llc=[]
maxp=[]
count=0
for line in lines:
	words=line.split()
	if len(words) > 0 and data is False:
		for word in words:
			if '@' in word:
				data=True
				print "starting data"
				break
		if data is False:
			newf.write(line)

	if len(words) > 0 and data is True:
		zScore=float(words[-1])
		if zScore < cutoff:
			count+=1
			newf.write(line)
print count, 'particles included'
newf.close()
