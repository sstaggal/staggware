#!/usr/bin/env python

import numpy
from matplotlib import pyplot
import sys

paramfile=sys.argv[1]
f=open(paramfile,'r')
lines=f.readlines()
f.close()
#skip=10
skip=1
score=[]
dscore=[]
sigma=[]
logp=[]
defoc=[]
occ=[]
angast=[]
n=[]

for line in range(0,len(lines),skip):
	if lines[line][0]!='C':
		words=lines[line].split()
		dscore.append(float(words[-1]))
		score.append(float(words[-2]))
		sigma.append(float(words[-3]))
		logp.append(float(words[-4]))
		defoc.append(float(words[8]))
		occ.append(float(words[11]))
		angast.append(float(words[10]))
		n.append(float(words[0]))

pyplot.figure()
pyplot.subplot(321)
pyplot.hist(score,100)
pyplot.title('score histogram')

pyplot.subplot(322)
pyplot.hist(dscore,100)
pyplot.title('delta score')

pyplot.subplot(323)
pyplot.plot(n,score, 'bo')
pyplot.title('score vs. ptcl number')

pyplot.subplot(324)
pyplot.plot(defoc,score, 'bo')
pyplot.title('score vs. defocus')

pyplot.subplot(325)
pyplot.hist(occ,100)
pyplot.title('occupancy histogram')

pyplot.subplot(326)
pyplot.hist(angast,100)
pyplot.title('angle astig. histogram')


#fig1=pyplot.figure(1)
#pyplot.plot(n,presa, 'bo',figure=fig1)
#
#fig2=pyplot.figure(2)
#pyplot.hist(presa,100, figure=fig2)
#
##fig3=pyplot.figure(3)
##pyplot.plot(defoc,presa, 'bo', figure=fig3)
#
#fig4=pyplot.figure(4)
#pyplot.hist(dpres,100,figure=fig4)

pyplot.show()
