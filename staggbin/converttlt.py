#!/usr/bin/env python

import protomof
import optparse
import sys

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-i', dest='intilt', help='input tlt file')
	parser.add_option('-o', dest='outtilt', help='output tlt file')
	parser.add_option('-r', dest='refnumber', help='reference image number for new tilt file', type='int')
	options, args=parser.parse_args()

	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options


if __name__ == "__main__":
	options=parseOptions()
	
	imagedict,parameterdict,seriesname=protomof.parseTilt(options.intilt)
	protomof.writeTiltFile2(options.outtilt,seriesname,imagedict,parameterdict['azimuth'],options.refnumber)
	print "Done!"
	
