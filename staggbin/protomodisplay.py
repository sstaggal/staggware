#!/usr/bin/env python

import protomo
import optparse
import sys
import os

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('--param', dest='param', help='parameter file')
	parser.add_option('--tlt', dest='tlt', help='tilt file')
	parser.add_option('-n', dest='imagenumber', type=int, help='image number in the tilt series to display')
	
	options, args=parser.parse_args()
	
	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options

if __name__=="__main__":
	options=parseOptions()
	paramfile=options.param
	root=paramfile.split('.')[0]
	os.system('rm %s.i3t' % root)
	imagenumber=options.imagenumber
	geom=protomo.geom(options.tlt)
	param=protomo.param(paramfile)
	series=protomo.series(param,geom)
	image=series.filter(imagenumber)
	image.display()
	print 'Done!'

