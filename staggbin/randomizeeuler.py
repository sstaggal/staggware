#!/usr/bin/env python
import frealignlib
import random
import sys

paramfilename=sys.argv[1]
outfilename=sys.argv[2]
changefrac=0.25

ptcls=frealignlib.parseFrealignParamFile(paramfilename)

ptclnumber=len(ptcls)
nlst=range(ptclnumber)
random.shuffle(nlst)
victims=nlst[0:int(ptclnumber*changefrac)]
f=open(outfilename,'w')
count=0
for n in range(ptclnumber):
	if n in victims:
		count+=1
		ptcls[n]['phi']=random.random()*360
		ptcls[n]['psi']=random.random()*360
		ptcls[n]['theta']=random.random()*360
	frealignlib.writeParticleParamLine(ptcls[n],f)
f.close()
print count, "particles randomized"
print "Done!"
		
		
