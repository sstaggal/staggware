#!/usr/bin/env python

import glob
import os
#from matplotlib import pyplot
import numpy
import frealignlib
import sys
import subprocess

def getnptcls(paramfilename):
	f=open(paramfilename,'r')
	lines=f.readlines()
	f.close()
	nptcls=int(lines[-1].split()[-1])
	return nptcls

def getrange(paramfilename):
	f=open(paramfilename,'r')
	f.readline()
	datarange=f.readline()
	f.close()
	
	words=datarange.split()
	
	if len(words) >= 3:
		start=words[1]
		end=words[2]
	elif len(words) == 2:
		start=words[1]
		end=None

	return start,end

def sortf(x,y):
	xa=int(x.split('_')[-1])
	ya=int(y.split('_')[-1])
	return xa-ya
		
paramfilename=sys.argv[1]
###change this in the future, make as an option
innerdiam=0
outerdiam=0
apix=float(sys.argv[2])
sets=glob.glob('set_*')

basedir=os.getcwd()
fscdict={}
nptcls=[]
respoint5=[]
respoint143=[]
rmeaspoint5=[]
rmeaspoint143=[]
resout=open('p_vs_res.txt','w')
resout.write('Start\tStop\tNPtcls\tFSC_0.5\tFSC_0.143\tRmeasure_0.5\tRmeasure_0.143\t1/FSC_0.5\t1/FSC_0.143\t1/Rmeasure_0.5\t1/Rmeasure_0.143\n')

sets.sort(cmp=sortf)
for set in sets:
	os.chdir(set)
	print set
	ns, res, fsc = frealignlib.parseCombine('frealign.combine.out')
	nptcls.append(getnptcls(paramfilename))
	start,end=getrange(paramfilename)
	fscdict[nptcls[-1]]=(ns,res,fsc)
	
	#get res from Frealign
	for f in range(len(fsc)):
		#print fsc[f]
		if fsc[f] < 0.5:
			fa=fsc[f-1]
			fb=fsc[f]
			frac=(fa-0.5)/(fa-fb)
			#print fa,fb, fa-fb, (fa-0.5)/(fa-fb)
			ra=res[f-1]
			rb=res[f]
			respoint5.append(ra-((ra-rb)*frac))
#			resout.write('%d\t%f\n'% (nptcls[-1],respoint5[-1]))
			break
	for f in range(len(fsc)):
		#print fsc[f]
		if fsc[f] < 0.143:
			fa=fsc[f-1]
			fb=fsc[f]
			frac=(fa-0.143)/(fa-fb)
			#print fa,fb, fa-fb, (fa-0.5)/(fa-fb)
			ra=res[f-1]
			rb=res[f]
			respoint143.append(ra-((ra-rb)*frac))
			break
	#get res from rmeasure
	rmeasproc = subprocess.Popen('rmeasure.exe', stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	fin = rmeasproc.stdin
	fout = rmeasproc.stdout
	fin.write('working.mrc\n')
	fin.write('%f\n' % apix)
	fin.write('%d,%d\n' % (innerdiam,outerdiam))
	output = fout.read()

	words=output.split()
	for n in range(0,len(words)):
		#print words[n],words[n+4]
		if words[n]=='Resolution' and words[n+4]=='0.5:':
			rmeaspoint5.append(float(words[n+5]))
		if words[n] =='Resolution' and words [n+4]=='0.143:':
			rmeaspoint143.append(float(words[n+5]))
			break
#	### temp hack
#	rmeaspoint5.append(10)
#	rmeaspoint143.append(10)
#	### temp hack
	resout.write('%s\t%s\t%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'% (start,end,nptcls[-1],respoint5[-1],respoint143[-1],rmeaspoint5[-1],rmeaspoint143[-1],1/respoint5[-1],1/respoint143[-1],1/rmeaspoint5[-1],1/rmeaspoint143[-1]))
	os.system('rm working.mrc')
	os.system('rm even')
	os.system('rm odd')
	os.chdir(basedir)

resout.close()
#respoint5=numpy.array(respoint5)
#respoint143=numpy.array(respoint143)
#nptcls=numpy.array(nptcls)		
##pyplot.plot(1/res,fsc)
#pyplot.plot(numpy.log(nptcls),1/respoint5,'bo')	
#pyplot.plot(numpy.log(nptcls),1/respoint143,'ro')	
#ylocs,labs=pyplot.yticks()
#ylabs=1/ylocs
#pyplot.yticks(ylocs,ylabs)
#
##xlocs,xlabs=pyplot.xticks()
##xlabs=10**xlabs
##pyplot.xticks(xlocs,xlabs)
#
##pyplot.plot(nptcls,respoint5,'bo')
##pyplot.plot(nptcls,respoint143,'ro')
#pyplot.show()
