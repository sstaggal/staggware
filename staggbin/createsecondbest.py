#!/usr/bin/env python

import EMAN
import glob
import os
import optparse

parser=optparse.OptionParser()
parser.add_option('--mask',dest='mask', type="int")
parser.add_option('--pad',dest='pad', type="int")
parser.add_option('--hard',dest='hard', type="float")
parser.add_option('--sym',dest='sym')
options,args=parser.parse_args()

dirs=glob.glob("*.dir")

wd=os.getcwd()
outstack=os.path.join(wd,'secondbest.hed')

projections=EMAN.readImages('proj.hed',-1,-1,0)
dirs.sort()
for d in dirs:
	print d
	os.chdir(d)
	averages=EMAN.readImages('sortavg.hed',-1,-1,0)
	secondbest=averages[1]
	clsnumber=int(d[3:7])
	e=projections[clsnumber].getEuler()
	projections[clsnumber].setNImg(-1)
	projections[clsnumber].writeImage(outstack,-1)
	secondbest.setRAlign(e)
	secondbest.writeImage(outstack,-1)
	os.chdir(wd)

make3d='make3d %s out=%s mask=%d sym=%s pad=%d mode=2 hard=%f' % (outstack, 'secondbest.mrc', options.mask, options.sym, options.pad, options.hard)
os.system(make3d)
print "Done!"
