#!/usr/bin/env python

import sys

nptcls=int(sys.argv[1])

f1=open('even.lst','w')
f2=open('odd.lst','w')
f1.write("#LST\n")
f2.write("#LST\n")
for n in range(0,nptcls,2):
	f1.write('%d\tstart.hed\n' % (n))
	f2.write('%d\tstart.hed\n' % (n+1))

f1.close()
f2.close()
print "Done!"

