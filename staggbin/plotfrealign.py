#!/usr/bin/env python

import numpy
from matplotlib import pyplot
import sys

files=sys.argv[1:]
for fname in files:
	print fname
	f=open(fname)
	lines=f.readlines()
	f.close()

	n=0	
	fs=[]
	while n <  len(lines):
		line=lines[n]
		if line[0]=='C':
			words=line.split()
			if words[1:3]==['NO.','RESOL'] and 'CC' in words:
				n+=1
				line=lines[n]
				ns=[]
				fsc=[]
				res=[]
				partfsc=[]
				while line[0]=='C':
					line=lines[n]
					words=line.split()
					if words[1]=='Average':
						break
					ns.append(float(words[1]))
					res.append(float(words[2]))
					fsc.append(float(words[5]))
					partfsc.append(float(words[6]))
					n+=1
				break
		n+=1
	ns=numpy.array(ns)
	res=numpy.array(res)
	fsc=numpy.array(fsc)
	partfsc=numpy.array(partfsc)
	pyplot.plot(1/res,fsc)
	pyplot.plot(1/res,partfsc)
	locs,labs=pyplot.xticks()
	labs=1/locs
	pyplot.xticks(locs,labs)
pyplot.show()
