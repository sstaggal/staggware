#!/usr/bin/env python

from protomof import *
import glob, sys
import pylab
import colorsys

if len(sys.argv) ==1:
	print """Please, check your arguments.
Usage: plotazimuth.py <seriesname> 
--------------------------------------
"""
	sys.exit()


seriesname=sys.argv[1]

tiltfiles=glob.glob(seriesname+"*.tlt")
tiltfiles.sort()
tiltitr=[]
for tilt in tiltfiles:
	if 'fitted' not in tilt and 'itr' not in tilt and 'ali' not in tilt and '-' in tilt:
		tiltitr.append(tilt)

fig=pylab.figure(1)
hue=0.0
colorincrement=0.66666666666/len(tiltitr)
for tilt in tiltitr:
	print tilt
	iteration=int(tilt.split('-')[1])
	imagedict,paramdict,seriesname=parseTiltItr(tilt)
	angle=[]
	cofx=[]
	cofy=[]	
	for img in range(1,len(imagedict)+1):
		#print imagedict[img]
		angle.append(imagedict[img]['tilt'])
		if imagedict[img].has_key('cofx'):
			cofx.append(imagedict[img]['cofx'])
			cofy.append(imagedict[img]['cofy'])
		else:
			cofx.append(1.0)
			cofy.append(1.0)
	color=colorsys.hsv_to_rgb(hue,1,1)
	pylab.plot(angle,cofx, label=tilt, figure=fig, color=color)
	pylab.plot(angle,cofy, label=tilt, figure=fig, color=color)
	hue+=colorincrement

pylab.legend()
pylab.show()

print "Done!"
