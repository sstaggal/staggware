#!/usr/bin/env python

import sys
import protomof
import EMAN
from pyami import correlator, peakfinder, mrc, convolver
import numpy
import optparse
from  math import *
import os
import pylab

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-i', dest='intilt', help='input tlt file')
	parser.add_option('-o', dest='outtilt', help='output tlt file')
	parser.add_option('-v', dest='invol', help='input volume to align against')
	parser.add_option('-p', dest='path', help='path to images')
	parser.add_option('-s', dest='scale', help='scale reference volume', type='int', default=1)
	options, args=parser.parse_args()

	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options
	
def getEMANShift(refname, imagename):
	print "correlating with EMAN"
	#read in reference
	clipsize=2500
	reftmp='reftmp.mrc'
	os.system('proc2d %s %s clip=%d,%d' % (refname, reftmp, clipsize, clipsize))
	ref=EMAN.EMData()
	ref.readImage(reftmp)
	
	#read in image
	imagetmp='imagetmp.mrc'
	os.system('proc2d %s %s clip=%d,%d' % (imagename, imagetmp, clipsize, clipsize))
	image=EMAN.EMData()
	image.readImage(imagetmp)
	
	#calculate rotation and translation
	ali=ref.RTFAlign(image,None)
	
	return ali.Dx(), ali.Dy(), ali.alt(), ali.az(), ali.phi()

def getPyAMIShift(refname, imagename, test=False, corr='cc'):
	#pyami way
	print "correlating with pyami"
	print "correlating %s to %s" % (refname, imagename)
	projarray=mrc.read(refname)
	
	imageclip='imageclip.mrc'
	command=('proc2d %s %s clip=%d,%d' % (imagename, imageclip, projarray.shape[1], projarray.shape[0]))
	print command
	os.system(command)
	imagearray=mrc.read(imageclip)

	if corr=='pc':
		cc=correlator.phase_correlate(projarray, imagearray, zero=False)
		conv=convolver.Convolver()
		kernel=convolver.gaussian_kernel(4)
		conv.setKernel(kernel)
		cc=conv.convolve(image=cc)

	elif corr=='cc':
		cc=correlator.cross_correlate(projarray, imagearray)
	
	else:
		print "unknown correlation type"
		# technically should raise an exception but whatevs
		sys.exit()
	if test:
		mrc.write(cc,'cc.mrc')
		os.system('proc2d cc.mrc cctmp.mrc clip=4096,4096 edgenorm' )
		os.system('proc2d cctmp.mrc cctmp.mrc shrink=16')
		os.system('proc2d cctmp.mrc cc.hed')
		
	peak=peakfinder.findSubpixelPeak(cc)

	ycen=imagearray.shape[0]/2
	xcen=imagearray.shape[1]/2
	peaky=peak['subpixel peak'][0]
	peakx=peak['subpixel peak'][1]
	shifty=ycen-(ycen-((peaky//ycen)*ycen)+peaky%ycen)
	shiftx=xcen-(xcen-((peakx//xcen)*xcen)+peakx%xcen)
	
	#wx, wy=correlator.wrap_coord((shiftx, shifty),cc.shape)
	#print "from wrap coord", wx, wy
	return shiftx, shifty

def makeRotationMatrixForAzimuth(azimuth, theta):
		
	u0=1.0 #reference tilt axis along x by protomo convention
	v0=0.0
	
	#rotate tilt axis by azimuth
	u=u0*cos(azimuth) - v0*sin(azimuth)
	v=u0*sin(azimuth) + v0*cos(azimuth)
	w=0.0

	#from http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/ArbitraryAxisRotation.html
	denom=u**2+v**2+w**2
	a=( (u**2)			+	((v**2+w**2)*cos(theta)) )		/denom
	b=( (u*v*(1-cos(theta)))	-	(w*sqrt(denom)*sin(theta)) )	/denom
	c=( (u*w*(1-cos(theta)))	+	(v*sqrt(denom)*sin(theta)) )	/denom
	d=( (u*v*(1-cos(theta)))	+	(w*sqrt(denom)*sin(theta)) )	/denom
	e=( (v**2)			+	((u**2+w**2)*cos(theta)) )		/denom
	f=( (v*w*(1-cos(theta)))	-	(u*sqrt(denom)*sin(theta)) )	/denom
	g=( (u*w*(1-cos(theta)))	-	(v*sqrt(denom)*sin(theta)) )	/denom
	h=( (v*w*(1-cos(theta)))	+	(u*sqrt(denom)*sin(theta)) )	/denom
	i=( (w**2)		 	+	((u**2+v**2)*cos(theta)) )		/denom

	matrix=numpy.array(((a,b,c),(d,e,f),(g,h,i)))
	return matrix

def makeProjection(volume, matrix, outname, scale, imagesize):
	
	elements=""
	for n in matrix.ravel():
		elements+='%.5f ' %n
	#print elements
	
	command="resample -A %s -prj %s %s" % (elements, volume, outname)
	print command
	os.system(command)
	
	#convert outname to .mrc
	outmrc=outname.split('.')[0]+'.mrc'
	command='cutimage -fmt mrc %s %s' % (outname,outmrc)
	print command
	os.system(command)
	
	d=mrc.readHeaderFromFile(outmrc)
	print
	print 'scaling image by', scale
	command='proc2d %s %s scale=%d clip=%d,%d' % (outmrc, outmrc, scale, d['nx']*scale, d['ny']*scale)
	print command
	os.system(command)
	
	return outmrc

if __name__ == "__main__":
	
	options=parseOptions()
	
	#imagesize=4096
	imagesize=2048
	imagecen=imagesize/2
	
	#read in tilt file
	imagedict, parameterdict, seriesname = protomof.parseTilt(options.intilt)
	
	#loop over all tilts
	azimuth=parameterdict['azimuth']*pi/180
	outproj="proj.img"
	newimagedict={}
	newnumber=1
	f=open('shifts.txt','w')
	for img in range(1,len(imagedict)+1):
#	for img in [35,71]:
		
		#calculate rotation matrix for individual tilt
		tiltangle=imagedict[img]['tilt']
		tiltangle=tiltangle*pi/180.0
		matrix=makeRotationMatrixForAzimuth(azimuth,tiltangle)
		print "azimuth:", azimuth*180/pi, "tilt angle:", tiltangle*180/pi
		#print matrix
		
		#skip zero degree
		
		#launch projection process
		outmrc=makeProjection(options.invol,matrix,outproj, options.scale, imagesize)
		
		#cross correlate the two images
		imagename=os.path.join(options.path, imagedict[img]['filename']+'.mrc')
		shiftx,shifty=getPyAMIShift(outmrc, imagename, test=True)
		
		#align with EMAN
		#ex,ey,alt,az,phi=getEMANShift(outmrc, imagename )
		#print "EMAN", ex, ey, alt, az, phi
		
		#shift tilt file appropriately
		newcenx=imagecen+shiftx
		newceny=imagecen+shifty
		print "shifting %s by %f x and %f y to new centers %f,%f" % (imagedict[img]['filename'], shiftx, shifty, newcenx, newceny)
		f.write('%f\t%f\n' % (shiftx, shifty))
		newimagedict[newnumber]={}
		newimagedict[newnumber]['x']=newcenx
		newimagedict[newnumber]['y']=newceny
		newimagedict[newnumber]['filename']=imagedict[img]['filename']
		newimagedict[newnumber]['tilt']=imagedict[img]['tilt']
		newimagedict[newnumber]['rotation']=imagedict[img]['rotation']
		newnumber+=1
		print "\n\n"
	
	#write new tiltfile
	protomof.writeTiltFile(options.outtilt,seriesname,newimagedict, parameterdict=parameterdict)		
	f.close()
	print "Done!"
