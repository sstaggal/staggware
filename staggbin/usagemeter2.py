#!/usr/bin/env python

from appionlib import apDatabase
import optparse
import sys
import smtplib
from email.mime.text import MIMEText
import time
import os
import grp

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-e', dest='e', default=False, action='store_true', help='email users usage info.')
	parser.add_option('--usagethresh', dest='usagethresh', type='int', default=500, help='usage threshold for emailing users in gigabytes')
	parser.add_option('-l', dest='l', type='str', default='/data/lustre/leginondata', help='base path for leginon session data')
	parser.add_option('-a', dest='a', type='str', default='/data/lustre/appiondata', help='base path for appion session data')
	parser.add_option('-f', dest='f', type='str', default='/data/lustre/framesdata', help='base path for frames session data')
	parser.add_option('--outpath', dest='outpath', type='str', default='/data/lustre/', help='base path for usage output')
	
	options, args=parser.parse_args()
	
	if len(args) !=0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	return options

def parseUsageFile(filename,sessioninfo,datatype):
	f=open(filename,'r')
	lines=f.readlines()
	f.close()
	
	for line in lines:
		words=line.split()
		meg_usage=int(words[0])
		datapath=words[1]
		osinfo=os.stat(datapath)
		gid = osinfo.st_gid
		grpname=grp.getgrgid(gid)
		sessionname=words[1].split('/')[-1]
		if sessionname not in sessioninfo.keys():
			sessioninfo[sessionname]={}
			try:
				sessiondata=apDatabase.getSessionDataFromSessionName(sessionname)
				sessioninfo[sessionname]['user']=sessiondata['user']['firstname']
				sessioninfo[sessionname]['group']=sessiondata['user']['group']['name']
				sessioninfo[sessionname]['info']=sessiondata['comment']
				sessioninfo[sessionname]['email']=sessiondata['user']['email']
			except:
				sessioninfo[sessionname]['user']=None
			
			sessioninfo[sessionname]['totals']={}
		sessioninfo[sessionname]['osgroup']=grpname.gr_name
		sessioninfo[sessionname]['totals'][datatype]=meg_usage/1000.0
			
def executeDiskUsage(usagedir,outfilepath):
	command='du -sm %s/* > %s' % (usagedir, outfilepath)
	print command
	os.system(command)
	

def sortByUsage(a,b):
	atotal=a[1]['sum']
	btotal=b[1]['sum']
	
	return int(round(atotal-btotal))	

def sendEmail(to=None, frm='sstagg@fsu.edu', subject=None, text=None):
	msg=MIMEText(text)
	msg['Subject']=subject
	msg['From'] = frm
	msg['To'] = to
	s = smtplib.SMTP('mailrelay.fsu.edu')
	s.sendmail(frm, [to], msg.as_string())
	s.quit()

	

if __name__ == "__main__":

	options=parseOptions()
	
	manager='sstagg@fsu.edu'
	
	summaryfile='/data/lustre/myamiusage.txt'
	leginonusagename='latest_leginon.txt'
	appionusagename='latest_appion.txt'
	framesusagename='latest_frames.txt'
	
	### generate usage file in megabytes
	
	#leginon	
	executeDiskUsage(options.l, os.path.join(options.outpath,leginonusagename))
	
	#appion
	executeDiskUsage(options.a, os.path.join(options.outpath,appionusagename))

	#frames
	executeDiskUsage(options.f, os.path.join(options.outpath,framesusagename))

	sessioninfo={}
	parseUsageFile(os.path.join(options.outpath, framesusagename),sessioninfo,'framesusage')
	parseUsageFile(os.path.join(options.outpath, leginonusagename),sessioninfo,'leginonusage')
	parseUsageFile(os.path.join(options.outpath, appionusagename),sessioninfo,'appionusage')
	
	### generate usage sums
	keys = sessioninfo.keys()
	for key in keys:
		sessioninfo[key]['sum']=sum(sessioninfo[key]['totals'].values())
	
	
	### generate sorted list of tuples with keys as item 0
	sessionsort=sessioninfo.items()
	sessionsort.sort(cmp=sortByUsage,reverse=True)
	
	groupleaders={'stagg':'sstagg@fsu.edu', 'taylor':'ktaylor@fsu.edu', 'stroupe': 'mestroupe@bio.fsu.edu', 'hongli': 'hli4@fsu.edu', 'admin':'dsousa@fsu.edu', 'guest':'dsousa@fsu.edu'}
	grouptotals={}
	
	f=open(summaryfile,'w')
	
	for items in sessionsort:
		session=items[0]
		s=''
		s+="Session\t=\t%s\n" % (session)
		s+="User\t=\t%s\n" % (sessioninfo[session]['user'])
		info=sessioninfo[session].get('info')
		if info is not None:
			s+="Comment\t=\t%s\n" % info
		s+="Total\t=\t%.3f GB\n" % (sessioninfo[session]['sum'])
		
		
		appionusage=sessioninfo[session]['totals'].get('appionusage')
		leginonusage=sessioninfo[session]['totals'].get('leginonusage')
		framesusage=sessioninfo[session]['totals'].get('framesusage')
		
		if appionusage is not None:
			s+="Appion\t=\t%.3f GB (%s)\n" % (appionusage, sessioninfo[session]['osgroup'])
		
		if leginonusage is not None:
			s+="Leginon\t=\t%.3f GB (%s)\n" % (leginonusage, sessioninfo[session]['osgroup'])
		
		if framesusage is not None:
			s+="Frames\t=\t%.3f GB (%s)\n" % (framesusage, sessioninfo[session]['osgroup'])
		s+='\n'
		print s
		if sessioninfo[session].get('group') in grouptotals.keys():
			#print sessioninfo[session].get('group')
			grouptotals[sessioninfo[session].get('group')]+=sessioninfo[session]['sum']
		else:
			#print sessioninfo[session].get('group')
			grouptotals[sessioninfo[session].get('group')]=sessioninfo[session]['sum']
		
		print options.e, options.usagethresh, sessioninfo[session]['sum']
		if options.e is True and sessioninfo[session]['sum'] > options.usagethresh:
			print "Emailing %s" % (sessioninfo[session]['user'])
			subject='Disk usage for session %s' % (session)
			message="Dear %s,\n" % (sessioninfo[session]['user'])
			message+="This message is to inform you that the disk usage for session %s has exceeded %d GB. You don't necessarily need to take any action at this time, but please make sure that your usage does not exceed the limit for your group.\n\n" % (session,options.usagethresh)
			message+=s
			sendEmail(to=sessioninfo[session]['email'], frm=manager, subject=subject, text=message)
			time.sleep(2)
		f.write(s)
	f.close()
	
	for group in grouptotals.keys():
		for groupleader in groupleaders.keys():
			#print group
			if group is not None and groupleader in group.lower():
				subject='Disk usage for %s group' % (group)
				message="Dear Dr. %s,\n" % (groupleader.title())
				message+="Your group's total disk usage for all of your Leginon, Appion, and Frames data is %d GB\n" % (grouptotals[group])
				if options.e is True:
					sendEmail(to=groupleaders[groupleader], frm=manager, subject=subject, text=message)
				break
	print grouptotals
