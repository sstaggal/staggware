#!/usr/bin/env python

import sys
import os
import shutil

def findAndErase(path,keep,execute=False):
	if os.path.exists(path):
		print "Found session data", path
		if keep is '':
			if execute is True:
				print "Removing", path
				#shutil.rmtree(path)
			else:
				print 'Skipping', path
		else:
			print "Keeping", path
	

if len(sys.argv) != 3:
	print '''Usage:removearchiveddata.py <archivefile.csv> <execute switch>
--------------------------------------------------------------
execute switch should be "execute" in order to erase data
'''

archivename=sys.argv[1]
executeswitch=sys.argv[2]


if executeswitch=="execute":
	execute=True
else:
	execute=False
	
archivef=open(archivename,'r')
lines=archivef.readlines()
archivef.close()

datapaths=['/data/lustre/','/data/athena/','/data/zeus']
reconpath='/panfs/storage.local/imb/stagg/sstagg'

for line in lines[1:]:
	words=line.split(',')
	session=words[0]
	if session.strip()=='':
		print "Blank session, skipping"
		continue
	print "Searching for session", session
	leginonkeep=words[2]
	appionkeep=words[3]
	reconstructionkeep=words[4]
	for path in datapaths:
		leginonpath=os.path.join(path,'leginondata',session)
		appionpath=os.path.join(path,'appiondata',session)
		findAndErase(leginonpath, leginonkeep, execute=execute)
		findAndErase(appionpath, appionkeep, execute=execute)

	#reconstructionpath=os.path.join(reconpath,session)
	#findAndErase(reconstructionpath, reconstructionkeep, execute=execute)
	
	print "\n\n\n"
	
print "Done!"
		
	
