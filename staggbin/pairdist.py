#!/usr/bin/env python

import EMAN
from appionlib import apEulerCalc
import glob
import math
import sys
import pylab


npairs=int(sys.argv[1])
cutoff=int(sys.argv[2])

projections='proj.hed'

#determine eulers from projections
imgnum, imgtype = EMAN.fileCount(projections)
img = EMAN.EMData()
img.readImage(projections, 0, 1)

# for projection images, get eulers
eulerdict={}
for i in range(imgnum):
	img.readImage(projections, i, 1)
	e = img.getEuler()
	alt = e.thetaMRC()*180./math.pi
	az = e.phiMRC()*180./math.pi
	phi = e.omegaMRC()*180./math.pi
	eulers={'euler1':alt,'euler2':az,'euler3':phi}
	eulerdict[i]=eulers

clslist=glob.glob('cls*.lst')

particledict={}
for cls in clslist:
	print "Processing ",cls
	f=open(cls,'r')
	lines=f.readlines()
	f.close()
	
	#determine cls number
	classnumber=int(lines[1].split()[0])
	#print "Using Eulers ", eulerdict[classnumber]
	for line in lines[2:]:
		words=line.split()
		ptclnumber=int(words[0])
		particledict[ptclnumber]=eulerdict[classnumber]
dlist=[]
np=[]

for n in range(npairs):
	d=apEulerCalc.eulerCalculateDistance(particledict[n],particledict[n+npairs])
	if d < cutoff:
		np.append(n)
		np.append(n+npairs)

	dlist.append(d)

np.sort()

f=open('sorted.lst','w')
f.write('#LST\n')
for ptcl in np:
	f.write('%d\t%s\n' % (ptcl,'start.hed'))
f.close()

print len(np)

pylab.hist(dlist,100)
pylab.show()

print "Done!"
