#!/usr/bin/env python

import numpy
from matplotlib import pyplot
import sys

paramfiles=sys.argv[1:]

occlst=[]
for paramfile in paramfiles:
	
	f=open(paramfile,'r')
	lines=f.readlines()
	f.close()
	#skip=10
	skip=1
	score=[]
	dscore=[]
	sigma=[]
	logp=[]
	defoc=[]
	occ=[]
	n=[]

	for line in range(0,len(lines),skip):
		if lines[line][0]!='C':
			words=lines[line].split()
			dscore.append(float(words[-1]))
			score.append(float(words[-2]))
			sigma.append(float(words[-3]))
			logp.append(float(words[-4]))
			occ.append(float(words[-5]))
			defoc.append(float(words[8]))
			n.append(float(words[0]))

	occlst.append(occ)

nptcl=len(occlst[0])
niters=len(occlst)
doccpop=[]
doccmean=[]
for ptcl in range(nptcl):
	doccptcliter=[]
	for n in range(niters-1):
			doccptcl=occlst[n][ptcl]-occlst[n+1][ptcl]
			doccpop.append(doccptcl)
			doccptcliter.append(doccptcl)
	doccmean.append(sum(doccptcliter)/(niters-1))

pyplot.figure(1)
pyplot.hist(doccpop)
pyplot.figure(2)
pyplot.plot(doccptcliter)
pyplot.show()
print 'Done!'
