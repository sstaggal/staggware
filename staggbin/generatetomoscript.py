#!/usr/bin/env python

import sys, os

if len(sys.argv) ==1:
	print """Please, check your arguments.
Usage: generatetomoscript <iters> <startiter.param> <out.sh> [removerot]
----------------------------------------------
"""
	sys.exit()

if 'removerot' in sys.argv:
	removerot=True
else:
	removerot=False
	
outsh=sys.argv[3]
startiter=sys.argv[2]
iters=int(sys.argv[1])

prefix=startiter.split('.param')[0].split('-')[0]
iternumber=int(startiter.split('.param')[0].split('-')[-1])

f=open(outsh,'w')
f.write("#!/bin/bash\n\n")
f.write('#MOAB -l nodes=1\n')
f.write('#MOAB -l walltime=4:00:00\n\n')
f.write('export PROTOMOROOT="/panfs/storage.local/winkler/software/protomo-1.1.4"\n')
f.write('source $PROTOMOROOT/init.sh\n')
f.write('export PATH=/panfs/storage.local/winkler/software/protomo/bin:$PATH\n\n')

f.write('cd %s\n\n' % (os.getcwd()))
for n in range(iters):
	curriter=prefix+'-'+str(iternumber+n).zfill(2)
	newiter=prefix+'-'+str(iternumber+n+1).zfill(2)
	f.write('cp %s %s\n' % (curriter+'.param', newiter+'.param'))
	if removerot:
		f.write('removerot.py %s %s\n' % (curriter+'-fitted.tlt', newiter+'-itr.tlt'))
	else:
		f.write('cp %s %s\n' % (curriter+'-fitted.tlt', newiter+'-itr.tlt'))
	f.write('bash tomo-refine.sh %s\n' % (newiter+'.param'))
	f.write('bash tomo-fit.sh %s\n\n' % (newiter+'.param'))
	

print "Done!"
