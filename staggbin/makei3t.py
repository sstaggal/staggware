#!/usr/bin/env python

import protomo
import sys

paramfile=sys.argv[1]
tiltfile=sys.argv[2]

param=protomo.param(paramfile)
geom=protomo.geom(tiltfile)
series=protomo.series(param,geom)

print "Done!"

