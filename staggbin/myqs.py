#!/usr/bin/env python

import subprocess

queues=['chemistry_q', 'imb_q', 'stroupe_q', 'stagg_q', 'sec4m_q']

for q in queues:
	print q
	print '_______________________'
	s=subprocess.check_output('squeue -p %s -o "%%.7i %%.9P %%.8j %%.8u %%.8T %%.10M %%.9l %%C %%.6D %%R"' % (q), shell=True)
	lines = s.split('\n')
	total=0
	for line in lines:
		print line
		words=line.split()
		if 'RUNNING' in words:
			total+=int(words[7])
	print '%s total running is %d' % (q,total)
	print '_______________________\n'
