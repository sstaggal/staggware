#!/usr/bin/env python

import sys
import optparse
import os
from protomof import *

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-i', dest='intilt', help='input tlt file')
	parser.add_option('-o', dest='outtilt', help='output tlt file')
	parser.add_option('-x', dest='cenx', type='int', help='new x center for "reference image"')
	parser.add_option('-y', dest='ceny', type='int', help='new y center for "reference image"')
	parser.add_option('-r', dest='refimg', type='int', help='reference image')
	parser.add_option('-s', dest='scale', type='int', default=1, help='scale original shift. Used for going between scaled centered tlt files and corner centered')

	options, args=parser.parse_args()

	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options
	
	
if __name__ == "__main__":
	options=parseOptions()
	

	imgdict,paramdict,series=parseTilt(options.intilt)
		
	
	keys=imgdict.keys()
	keys.sort()
	
	relx=imgdict[options.refimg]['x']-options.cenx
	rely=imgdict[options.refimg]['y']-options.ceny
	
	print "Original reference image", imgdict[options.refimg], 'shifted by', relx, rely
		
	for key in keys:
		newx=imgdict[key]['x']*options.scale-relx
		newy=imgdict[key]['y']*options.scale-rely
		imgdict[key]['x']=newx
		imgdict[key]['y']=newy
	
	
	series=os.path.splitext(options.outtilt)[0]
	writeTiltFile(options.outtilt,series,imgdict,paramdict)

	
	print "Done!"
