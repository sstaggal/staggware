#!/usr/bin/env python

import sys

def parseStar(filename,headlength):
	f=open(filename,'r')
	lines=f.readlines()
	f.close()
	ptcls=[]
	for n in range(headlength,len(lines)):
		words=lines[n].split()
		if len(words) > 1:
			ptcls.append(words)
	return ptcls	

if __name__=="__main__":
	
	starname=sys.argv[1]
	headlength=int(sys.argv[2])
	column=int(sys.argv[3])
	ptcls=parseStar(starname,headlength)
	
	f=open('star.lst','w')
	f.write("#LST\n")
	
	for ptcl in ptcls:
		#print ptcl
		nptcl=int(ptcl[column-1].split('@')[0])-1 #subtract one to get EMAN numbering
		f.write ("%d\tstart.hed\n" % (nptcl))
	f.close()
	print "Done!"
