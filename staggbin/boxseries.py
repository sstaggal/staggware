#!/usr/bin/env python

import sys
import optparse
import os
from pyami import mrc
import copy
from protomof import *

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-i', dest='intilt', help='input tlt file')
	parser.add_option('-p', dest='inpath', help='path to mrc files')
	parser.add_option('-o', dest='outprefix', help='output prefix')
	parser.add_option('-b', dest='boxsize', type='int', help='box size')
	
	options, args=parser.parse_args()

	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options
	
	
if __name__ == "__main__":
	options=parseOptions()
	

	imgdict,paramdict,series=parseTilt(options.intilt)
		
	keys=imgdict.keys()
	keys.sort()
	
	newdict=copy.deepcopy(imgdict)
	for key in keys:
		m=mrc.read(os.path.join(options.inpath,imgdict[key]['filename']+'.mrc'))
		x=imgdict[key]['y'] #pyami mrc package switches x and y compared to protomo	
		y=imgdict[key]['x'] #pyami mrc package switches x and y compared to protomo
		boxrad=options.boxsize/2
		
		m2=m[x-boxrad:x+boxrad,y-boxrad:y+boxrad]
		
		name='%s_%03d.mrc' % (options.outprefix, key)
		
		newdict[key]['x']=boxrad
		newdict[key]['y']=boxrad
		newdict[key]['filename']=os.path.splitext(name)[0]
		print 'writing', name
		mrc.write(m2,name)
		
	writeTiltFile(options.outprefix+'.tlt',options.outprefix,newdict,paramdict)

	
	print "Done!"
