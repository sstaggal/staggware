#!/usr/bin/env python

import sys
import os
import shutil
import subprocess

def findAndSync(path,keep,execute=False):
	if os.path.exists(path):
		print "Found session data", path
		if keep is '':
			if execute is True:
				print "Removing", path
				#shutil.rmtree(path)
			else:
				print 'Skipping', path
		else:
			print "Keeping", path
	

if len(sys.argv) != 3:
	print '''Usage:removearchiveddata.py <archivefile.csv> <execute switch>
--------------------------------------------------------------
execute switch should be "execute" in order to erase data
'''

archivename=sys.argv[1]
executeswitch=sys.argv[2]


if executeswitch=="execute":
	execute=True
else:
	execute=False
	
archivef=open(archivename,'r')
lines=archivef.readlines()
archivef.close()

sourceroot='/usbC/backup'
dataroot='/data/lustre/leginondata'

for line in lines[1:]:
	words=line.split(',')
	session=words[0]
	leginonkeep=words[2]
	appionkeep=words[3]
	reconstructionkeep=words[4]
	if leginonkeep is not '' or appionkeep is not '' or reconstructionkeep is not '':
		print "Restoring session", session
		sourcepath=os.path.join(sourceroot,session)
		datapath=os.path.join(dataroot)
		command=['rsync','-av',sourcepath,datapath]
		if execute is True:
			subprocess.call(command)
		else:
			command.append('--dry-run')
			subprocess.call(command)
	
	else:
		print "Skipping session", session
	print "\n\n\n"
	
print "Done!"
		
	
