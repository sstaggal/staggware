#!/usr/bin/env python

from pyami import mrc
import numpy
from scipy import optimize
import sys	
import argparse

parser=argparse.ArgumentParser(description="Make difference map with local normalization")
parser.add_argument('--box', type=int, help="Size of the box to be used for local normalization")
parser.add_argument('out', type=str, nargs=1, help="Out difference map prefix")
parser.add_argument('--plusvol', '-p', type=str, help="Map to be subtracted from")
parser.add_argument('--minusvol', '-m', type=str, help="Map to subtract from plusvol")
parser.add_argument('--kernelmean', '-k', action='store_true', default=False, help="Also output map that is mean of diff map kernel for each point")
args=parser.parse_args()

a=mrc.read(args.plusvol)
b=mrc.read(args.minusvol)

winsize=args.box
halfsize=winsize/2
quartersize=halfsize/2

c=numpy.zeros(a.shape)
cmean=numpy.zeros(a.shape)

for i in range(halfsize,a.shape[0]-halfsize):
	print i
	for j in range(halfsize,a.shape[0]-halfsize):
		if a.ndim == 2:
			ac=a[ i-halfsize:i+halfsize, j-halfsize:j+halfsize ]
			if ac.std()==0:
				continue
			ac=(ac-ac.mean())/ac.std()			

			bc=b[ i-halfsize:i+halfsize, j-halfsize:j+halfsize ]
			if bc.std()==0:
				continue
			bc=(bc-bc.mean())/bc.std()

			dab=ac-bc
			if args.kernelmean is True:
				cmean[i,j]=dab.mean()
			c[i,j]=dab[halfsize,halfsize]
			
		elif a.ndim == 3:
			for k in range(halfsize,a.shape[0]-halfsize):
					ac=a[ i-halfsize:i+halfsize, j-halfsize:j+halfsize, k-halfsize:k+halfsize ]
					if ac.std()==0:
						continue
					ac=(ac-ac.mean())/ac.std()			

					bc=b[ i-halfsize:i+halfsize, j-halfsize:j+halfsize, k-halfsize:k+halfsize ]
					if bc.std()==0:
						continue
					bc=(bc-bc.mean())/bc.std()

					dab=ac-bc
					#print dab.shape
					#mrc.write(ac,'ac.mrc')
					#mrc.write(bc,'bc.mrc')
					#mrc.write(dab,'dab.mrc')

					if args.kernelmean is True:
						cmean[i,j,k]=dab.mean()
					c[i,j,k]=dab[halfsize,halfsize,halfsize]
					#print i,j,i-halfsize,i+halfsize, j-halfsize, j+halfsize, quartersize
					#sys.exit()

c=(c-c.mean())/c.std()
mrc.write(c,args.out[0]+'.mrc')			
if args.kernelmean is True:
	cmean=(cmean-cmean.mean())/cmean.std()
	mrc.write(cmean,args.out[0]+'.meandiff.mrc')

print "Done!"
