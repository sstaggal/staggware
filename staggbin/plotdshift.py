#!/usr/bin/env python

import optparse
import pylab
import protomof
import math
import sys
import colorsys

def parseOptions():
	parser=optparse.OptionParser()
	options, args=parser.parse_args()
	
	if len (sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return args
	
if __name__ == '__main__':
	
	args=parseOptions()
	
	nplots=len(args) - 1

	hue=0.0
	colorincrement=0.66666666666/nplots
	
	for arg in range(nplots):
		imagedict1,paramdict1,seriesname1=protomof.parseTilt(args[arg])
		imagedict2,paramdict2,seriesname2=protomof.parseTilt(args[arg+1])
		
		keys=imagedict1.keys()
		keys.sort()
		ds=[]
		tilts=[]
		for key in keys:
			dx=imagedict1[key]['x']-imagedict2[key]['x']
			dy=imagedict1[key]['y']-imagedict2[key]['y']
			
			d=math.sqrt(dx**2+dy**2)
			
			ds.append(d)
			tilts.append(imagedict1[key]['tilt'])
		label=args[arg]+args[arg+1]
		color=colorsys.hsv_to_rgb(hue,1,1)
		pylab.plot(tilts,ds, 'o', label=label, color=color)
		hue+=colorincrement
	pylab.legend()
	pylab.show()
	print "Done!"
