#!/usr/bin/env python

import optparse
import os
import sys

def parseOptions():
	parser=optparse.OptionParser()
	parser.add_option('-f', dest='f', type='str', help='EMAN FSC file')
	parser.add_option('-a', dest='a', type='float', help='apix')
	options, args=parser.parse_args()
	
	if len(args) != 0 or len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	
	return options
if __name__=="__main__":
	options=parseOptions()
	f=open(options.f)
	lines=f.readlines()
	f.close()
	
	freq=[]
	corr=[]
	for line in lines:
		words=line.split()
		freq.append(int(words[0]))
		corr.append(float(words[1]))
	
	for n in range(len(freq)):
		if corr[n] < 0.5:
			freq0_5=(freq[n]+freq[n-1])/2.0
			break
			
	for n in range(len(freq)):
		if corr[n] < 0.143:
			freq0_143=(freq[n]+freq[n-1])/2.0
			break
	
	res0_5=len(freq)*2*options.a/freq0_5
	res0_143=len(freq)*2*options.a/freq0_143
	
	print "Resolution at FSC 0.5=", res0_5
	print "Resolution at FSC 0.143=", res0_143
	
