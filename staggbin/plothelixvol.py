#!/usr/bin/env python

from pyami import mrc
from matplotlib import pyplot
import sys
import numpy

apix=float(sys.argv[2])
filename=sys.argv[1]

a=mrc.read(filename)

x,y,z=a.shape

npoints=z/2

xscale=range(npoints)
xscale=numpy.array(xscale)
print xscale
xscale=xscale*apix

b=a[x/2,y/2,(z/2):]


pyplot.plot(xscale,b)
pyplot.show()
