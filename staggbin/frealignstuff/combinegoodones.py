#!/usr/bin/env python

import frealignlib
import copy
import os
import shutil

def modifyparams(paramlst, keep, outparam, threshold=65, newpresa=90):
	nptcls=len(paramlst)
	candidates=[]
	newparamlst=copy.deepcopy(paramlst) ### make copy so don't overwrite originals
	
	### find candidate particles with presa less than threshold
	### set all others to newpresa (greater than threshold)
	for n in range(nptcls):
		if newparamlst[n]['presa'] < threshold:
			candidates.append([n,newparamlst[n]['presa']])
		newparamlst[n]['presa']=newpresa
	
	
	### randomize the candidate particles and reset the kept ones to original presa
	random.shuffle(candidates)
	for candidate in candidates[:keep]:
		newparamlst[candidate[0]]['presa']=candidate[1]
	
	### write out the new param file
	f=open(outparam,'w')
	for param in newparamlst:
		writeParticleParamLine(param,f)
	f.close()
			
def modifyjob(jobin,setname,jobout):
	f=open(jobin,'r')
	lines=f.readlines()
	f.close()
	
	path=lines[7][:-1]
	outpath=os.path.join(path,setname)
	
	f=open(jobout,'w')
	for line in lines[:7]:
		f.write(line)
	f.write('%s\n' % (outpath))
	
	for line in lines[8:]:
		f.write(line)
	
if __name__ == '__main__':
	
	nsets=10
	paramfile='params.iter005.par'
	paramlst=frealignlib.parseFrealignParamFile(paramfile)
	nptcls=len(paramlst)
	
	f=open('goodones.par','w')
	for ptcl in range(nptcls):
		if (ptcl+1 >=1 and ptcl+1 <= 3628) or (ptcl+1 >=18141 and ptcl+1 <= 36280):
			pass
		else:
			paramlst[ptcl]['presa']=90
		frealignlib.writeParticleParamLine(paramlst[ptcl],f)
	f.close()
