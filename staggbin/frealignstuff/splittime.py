#!/usr/bin/env python

import frealignlib
import copy
import os
import shutil

def modifyparams(paramlst, keep, outparam, threshold=65, newpresa=90):
	nptcls=len(paramlst)
	candidates=[]
	newparamlst=copy.deepcopy(paramlst) ### make copy so don't overwrite originals
	
	### find candidate particles with presa less than threshold
	### set all others to newpresa (greater than threshold)
	for n in range(nptcls):
		if newparamlst[n]['presa'] < threshold:
			candidates.append([n,newparamlst[n]['presa']])
		newparamlst[n]['presa']=newpresa
	
	
	### randomize the candidate particles and reset the kept ones to original presa
	random.shuffle(candidates)
	for candidate in candidates[:keep]:
		newparamlst[candidate[0]]['presa']=candidate[1]
	
	### write out the new param file
	f=open(outparam,'w')
	for param in newparamlst:
		writeParticleParamLine(param,f)
	f.close()
			
def modifyjob(jobin,setname,jobout):
	f=open(jobin,'r')
	lines=f.readlines()
	f.close()
	
	path=lines[7][:-1]
	outpath=os.path.join(path,setname)
	
	f=open(jobout,'w')
	for line in lines[:7]:
		f.write(line)
	f.write('%s\n' % (outpath))
	
	for line in lines[8:]:
		f.write(line)
	
if __name__ == '__main__':
	
	nsets=10
	paramfile='params.iter005.par'
	paramlst=frealignlib.parseFrealignParamFile(paramfile)
	nptcls=len(paramlst)
	
	div=nptcls/nsets
	start=0
	last=div
	basedir=os.getcwd()
	subdirs=[]
	for set in range(nsets):
		print 'set',set
		newparamlst=copy.deepcopy(paramlst)
		subdirs.append('set_%d' % (set))
		os.mkdir(subdirs[-1])
		os.chdir(subdirs[-1])
		modifyjob(os.path.join(basedir,'frealign.combine.sh'),subdirs[-1],'frealign.combine.sh')
		shutil.copy(os.path.join(basedir,'working.hed'),'working.hed')
		shutil.copy(os.path.join(basedir,'working.img'),'working.img')
		os.system('ln -s /panfs/storage.local/imb/stagg/sstagg/11feb25b/frealign2/start.* .')
		f=open(paramfile,'w')
		for ptcl in range(nptcls):
			if ptcl < start or ptcl >= last:
				newparamlst[ptcl]['presa']=90
			frealignlib.writeParticleParamLine(newparamlst[ptcl],f)
		f.close()
		start=last
		last=last+div

		print "submitting job for ", subdirs[-1]
		os.system('msub -q stagg_q frealign.combine.sh')
		os.chdir(basedir)
	
