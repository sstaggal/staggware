#!/usr/bin/env python

import frealignlib
import copy
import os
import shutil

			
def modifyjob(jobin,setname,jobout):
	f=open(jobin,'r')
	lines=f.readlines()
	f.close()
	
	path=lines[7][:-1]
	outpath=os.path.join(path,setname)
	
	f=open(jobout,'w')
	for line in lines[:7]:
		f.write(line)
	f.write('%s\n' % (outpath))
	
	for line in lines[8:]:
		f.write(line)
	
if __name__ == '__main__':
	
	paramfile='params.iter005.par'
	paramlst=frealignlib.parseFrealignParamFile(paramfile)
	f=open('bestparams.par','w')
	total=0
	for param in paramlst:
		if param['df1'] > 8000 and param['df1'] < 16000 and (param['ptclnum'] < 10800 or param['ptclnum'] > 18000):
			total+=1
		else:
			param['presa']=90
		frealignlib.writeParticleParamLine(param,f)
	f.close()
	print "Total is ", total
#	nptcls=len(paramlst)
#	
#	defoclst=[x['df1'] for x in paramlst]
#	defoclst.sort()
#	mindefocus=defoclst[0]
#	maxdefocus=25000
#	
#	div=(maxdefocus-mindefocus)/nsets
#	first=mindefocus
#	last=first+div
#	basedir=os.getcwd()
#	subdirs=[]
#	for set in range(nsets):
#		print 'set',set, first, last
#		newparamlst=copy.deepcopy(paramlst)
#		subdirs.append('set_%d' % (set))
#		os.mkdir(subdirs[-1])
#		os.chdir(subdirs[-1])
#		modifyjob(os.path.join(basedir,'frealign.combine.sh'),subdirs[-1],'frealign.combine.sh')
#		shutil.copy(os.path.join(basedir,'working.hed'),'working.hed')
#		shutil.copy(os.path.join(basedir,'working.img'),'working.img')
#		os.system('ln -s /panfs/storage.local/imb/stagg/sstagg/11feb25b/frealign2/start.* .')
#		f=open(paramfile,'w')
#		total=0
#		for ptcl in range(nptcls):
#			if newparamlst[ptcl]['df1'] < first or newparamlst[ptcl]['df1'] >= last:
#				newparamlst[ptcl]['presa']=90
#			else:
#				total +=1 
#			frealignlib.writeParticleParamLine(newparamlst[ptcl],f)
#		print "Total particles", total
#		f.close()
#		first=last
#		last=last+div
#
#		print "submitting job for ", subdirs[-1]
#		os.system('msub -q stagg_q frealign.combine.sh')
#		os.chdir(basedir)
	
