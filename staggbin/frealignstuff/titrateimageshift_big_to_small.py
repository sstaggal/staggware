#!/usr/bin/env python

import sinedon
from appionlib import apStack
import frealignlib
import os
import shutil
import sys
import copy

def modifyjob(jobin,setname,jobout):
	f=open(jobin,'r')
	lines=f.readlines()
	f.close()
	
	path=lines[7][:-1]
	outpath=os.path.join(path,setname)
	
	f=open(jobout,'w')
	for line in lines[:7]:
		f.write(line)
	f.write('%s\n' % (outpath))
	
	for line in lines[8:]:
		f.write(line)

def sortshift(a,b):
	p=a['shift']-b['shift']
	if p < 0:
		return -1
	elif p > 0:
		return +1
	else:
		return 0
	
if __name__=='__main__':

	stackid=39
	sinedon.setConfig('appiondata', db='ap20')
	stackdataorig=apStack.getStackParticlesFromId(stackid)
	
	shiftlst=[]
	n=0
	#f=open('shiftdist.txt','w')
	for ptcl in stackdataorig[:1000]:
		imageshift=ptcl['particle']['image']['scope']['image shift']
		d=(imageshift['x']**2+imageshift['y']**2)**0.5
		#f.write('%e\n' % (d))
		shiftlst.append({'ptcl':n,'shift':d})
		n+=1
	
	nsets=10
	paramfile='params.iter005.par'
	paramlst=frealignlib.parseFrealignParamFile(paramfile)
	nptcls=len(paramlst)
	
	shiftlst.sort(sortshift)
	#minshift=shiftlst[0]['shift']
	#maxshift=tmpshiftdist[-1]['shift']

	shiftdist=[]
	#f=open('shiftdist.txt','w')
	for ptcl in stackdataorig:
		imageshift=ptcl['particle']['image']['scope']['image shift']
		d=(imageshift['x']**2+imageshift['y']**2)**0.5
		#f.write('%e\n' % (d))
		shiftdist.append(d)
	
	nsets=10
	paramfile='params.iter005.par'
	paramlst=frealignlib.parseFrealignParamFile(paramfile)
	nptcls=len(paramlst)
	
	tmpshiftdist=copy.deepcopy(shiftdist)
	tmpshiftdist.sort()
	minshift=tmpshiftdist[0]
	maxshift=tmpshiftdist[-1]

	div=(maxshift-minshift)/nsets
	first=minshift
	last=first+div
	basedir=os.getcwd()
	subdirs=[]

	
	for set in range(nsets):
		print 'set', set, first, last
		newparamlst=copy.deepcopy(paramlst)
		subdirs.append('setbigtosmall_%d' % (set))
		os.mkdir(subdirs[-1])
		os.chdir(subdirs[-1])
		modifyjob(os.path.join(basedir,'frealign.combine.sh'),subdirs[-1],'frealign.combine.sh')
		shutil.copy(os.path.join(basedir,'working.hed'),'working.hed')
		shutil.copy(os.path.join(basedir,'working.img'),'working.img')
		os.system('ln -s /panfs/storage.local/imb/stagg/sstagg/11feb25b/frealign2/start.* .')
		f=open(paramfile,'w')
		total=0
		
		print 'set', set, 'less than',last
		for ptcl in range(nptcls):
	
			if shiftdist[ptcl] >= last:
				newparamlst[ptcl]['presa']=90
			else:
				total +=1 
			frealignlib.writeParticleParamLine(newparamlst[ptcl],f)
		print "Total particles", total
		f.close()
		last=last+div

		print "submitting job for ", subdirs[-1]
		os.system('msub -q stagg_q frealign.combine.sh')
		os.chdir(basedir)
		
	
	
	print "Done!"

