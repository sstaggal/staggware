#!/usr/bin/env python

import numpy
import matplotlib.pyplot as plt
import sys

fname=sys.argv[1]

f=open(fname)
lines=f.readlines()
f.close()

nlines=len(lines)

freq=numpy.zeros(nlines)
eamp=numpy.zeros(nlines)
camp=numpy.zeros(nlines)
shamp=numpy.zeros(nlines)
wilson=numpy.zeros(nlines)

for n in range(nlines):
	words=lines[n].split()
	freq[n]=float(words[0])
	eamp[n]=float(words[1])
	camp[n]=float(words[2])
	shamp[n]=float(words[3])
	wilson[n]=float(words[4])

# eamp=numpy.log(eamp)
# camp=numpy.log(camp)
# shamp=numpy.log(shamp)

plt.plot(freq,eamp, label='eamp')
plt.plot(freq,camp, label='camp')
plt.plot(freq,shamp, label='shamp')
plt.plot(freq,wilson, label='wilson')

locs,labs=plt.xticks()
labs=1/locs
labs=numpy.sqrt(labs)
labs=numpy.round(labs,2)
plt.xticks(locs,labs)

plt.xlabel('d (Angstroms)')
plt.ylabel('lnF')
plt.legend()
plt.show()
