#!/usr/bin/env python
# common frealign library
import os
import numpy

def parseFrealignParamFile(paramfile):
	"""
	parse a typical FREALIGN parameter file from v8.08
	"""
	if not os.path.isfile(paramfile):
		print "Parameter file does not exist: %s"%(paramfile)

	### cannot assume spaces will separate columns.
	#0000000001111111111222222222233333333334444444444555555555566666666667777777777888888888899999999990000000000
	#1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
	#     24  219.73   84.00  299.39   15.33  -17.51  10000.     1  27923.7  27923.7  -11.41   0.00    0.00
	f = open(paramfile, "r")
	parttree = []
	print "Processing parameter file: %s"%(paramfile)
	for line in f:
		sline = line.strip()
		if sline[0] == "C":
			### comment line
			continue
		words=sline.split()
		
		partdict = {
			'ptclnum': int(words[0]),
			'psi': float(words[1]),
			'theta': float(words[2]),
			'phi': float(words[3]),
			'shx': float(words[4]),
			'shy': float(words[5]),
			'mag': float(words[6]),
			'film': int(words[7]),
			'df1': float(words[8]),
			'df2': float(words[9]),
			'angast': float(words[10]),
			'occ': float(words[11]),
			'logp': int(words[12]),
			'sigma': float(words[13]),
			'score': float(words[14]),
			'change': float(words[15]),

		}
		parttree.append(partdict)
	f.close()
	if len(parttree) < 2:
		print "No particles found in parameter file %s"%(paramfile)


	print "Processed %d particles"%(len(parttree))
	return parttree

def writeParticleParamLine(particleparams, fileobject):
	p=particleparams
	fileobject.write("%7d%8.3f%8.3f%8.3f%8.3f%8.3f%9.1f%5d%9.1f%9.1f%8.2f%8.2f%10d%11.4f%8.2f%8.2f\n" 
		% (p['ptclnum'],p['psi'],p['theta'],p['phi'],p['shx'],p['shy'],p['mag'],
		p['film'],p['df1'],p['df2'],p['angast'],p['occ'],p['logp'],p['sigma'],p['score'],p['change']))

def parseCombine(combineout):
	f=open(combineout)
	lines=f.readlines()
	f.close()
	
	n=0
	while n <  len(lines):
		line=lines[n]
		if line[0]=='C':
			words=line.split()
			if words[1:3]==['NO.','RESOL'] and 'CC' in words:
				n+=1
				line=lines[n]
				ns=[]
				fsc=[]
				res=[]
				while line[0]=='C':
					line=lines[n]
					words=line.split()
					if words[1]=='Average':
						break
					ns.append(float(words[1]))
					res.append(float(words[2]))
					fsc.append(float(words[5]))
					n+=1
				break
		n+=1
	ns=numpy.array(ns)
	res=numpy.array(res)
	fsc=numpy.array(fsc)
	return ns, res, fsc

