#!/usr/bin/env python


def parseTable (tablefilename):
	f=open(tablefilename)
	lines=f.readlines()
	f.close()
	dtablelst=[]
	for line in lines:
		words=line.split()
		d={}
		d['tag']=words[0]
		d['aligned']=int(words[1])
		d['averaged']=int(words[2])
		d['dx']=float(words[3])
		d['dy']=float(words[4])
		d['dz']=float(words[5])
		d['tdrot']=float(words[6])
		d['tilt']=float(words[7])
		d['narot']=float(words[8])
		d['cc']=float(words[9])
		d['cc2']=float(words[10])
		d['cpu']=words[11]
		d['ftype']=int(words[12])
		d['ymintilt']=float(words[13])
		d['ymaxtilt']=float(words[14])
		d['xmintilt']=float(words[15])
		d['xmaxtilt']=float(words[16])
		d['fs1']=int(words[17])
		d['fs2']=int(words[18])
		d['tomo']=int(words[19])
		d['ttag']=int(words[20])
		d['class']=int(words[21])
		d['annotation']=words[22]
		d['x']=float(words[23])
		d['y']=float(words[24])
		d['z']=float(words[25])
		d['dshift']=float(words[26])
		d['daxis']=float(words[27])
		d['dnarot']=float(words[28])
		d['dcc']=float(words[29])
		d['otag']=words[30]
		d['npar']=words[31]
		d['ref']=int(words[32])
		d['sref']=int(words[33])
		d['apix']=float(words[34])
		d['def']=float(words[35])
		d['eig1']=float(words[36])
		#d['eig2']=float(words[37])
		dtablelst.append(d)
		###unclear how many columns there actually are
	return dtablelst


def writeTableLine(f,tabledict):
	d=tabledict
	f.write("%s %d %d %.2f %.2f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n" % \
		(d['tag'], d['aligned'], d['averaged'], d['dx'], d['dy'], d['dz'], d['tdrot'], d['tilt'], d['narot'], d['cc'], d['cc2'], d['cpu'], d['ftype'],\
		d['ymintilt'], d['ymaxtilt'], d['xmintilt'], d['xmaxtilt'], d['fs1'], d['fs2'], d['tomo'], d['ttag'], d['class'], d['annotation'],d['x'],\
		d['y'], d['z'], d['dshift'],d['daxis'], d['dnarot'], d['dcc'], d['otag'], d['npar'], d['ref'], d['sref'], d['apix'], d['def'], d['eig1']))
		
		
		
		
