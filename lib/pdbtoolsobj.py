#!/usr/bin/env python

"This module provides a set of tools for parsing, measuring, and modifying pdb files"

import math
import sys
import numpy
import copy

def getmass(element):
	"""Determine mass for element type"""
	massdict={"H":1.01, "C":12.01,"N":14.01,"O":16.0,"P":30.97,"S":32.07,"MG":24.31}
	try:
		mass=massdict[element]
	except:
		print "Warning! Element", element, "is not defined. Mass is set to 0 for that atom."
		mass=0
	return mass

def writepdbline(outfile, d):
	"Write a line to outfile in pdb format. d must be a dictionary containing records for an atom"
	outfile.write("%-6s%5d %-4s%1s%-3s %1s%4d%1s   %8.3f%8.3f%8.3f%6.2f%6.2f          %1s%-2s\n" % (d['rtype'],d['atomnumber'],d['atomtype'],d['altloc'],d['residue'],d['chain'],d['residuenumber'],d['icode'],d['coords'][0],d['coords'][1],d['coords'][2],d['occupancy'],d['tempfact'],d['element'],d['charge']))



class PDBObj:
	
	def __init__(self,pdbfilename):
		"""Parse pdb and return records as a list of dictionaries"""
		pdbfile=open(pdbfilename,'r')
		lines=pdbfile.readlines()
		pdbfile.close()
	
		#parse the pdb
		records=[]
		symlst=[]
		symmatrices=[]
		for line in lines:
			if line[:6]=="REMARK":
				words=line.split()
				if "BIOMT1" in words or "BIOMT2" in words or "BIOMT3" in words:
					symlst.append(line)
			elif line[:4]=="ATOM" or line[:6]=="HETATM":
				d={}
				d['rtype']=line[0:6]
				d['atomnumber']=int(line[6:11])
				d['atomtype']=line[12:16]
				d['altloc']=line[16:17]
				d['residue']=line[17:20]
				d['chain']=line[21:22]
				d['residuenumber']=int(line[22:26])
				d['icode']=line[26:27]
				x=float(line[30:38])
				y=float(line[38:46])
				z=float(line[46:54])
				d['coords']=numpy.array([x,y,z])
				d['occupancy']=float(line[54:60])
				d['tempfact']=float(line[60:66])
				d['element']=line[76:78].strip()
				d['charge']=line[78:80]
				d['mass']=getmass(d['element'])
				records.append(d)
		
		for n in range(len(symlst)/3):
			mat1=symlst[3*n+0].split()
			mat2=symlst[3*n+1].split()
			mat3=symlst[3*n+2].split()
			m=numpy.array(([float(mat1[4]),float(mat1[5]),float(mat1[6])],\
				[float(mat2[4]),float(mat2[5]),float(mat2[6])],\
				[float(mat3[4]),float(mat3[5]),float(mat3[6])]))
			symmatrices.append(m)
				
		self.biosym=symmatrices
		self.records=records

	def centerOfMass(self):
		totmass=0
		cenx=0
		ceny=0
		cenz=0
		for atom in self.records:
			cenx+=atom['coords'][0]*atom['mass']
			ceny+=atom['coords'][1]*atom['mass']
			cenz+=atom['coords'][2]*atom['mass']
			totmass+=atom['mass']
		cenx=cenx/totmass
		ceny=ceny/totmass
		cenz=cenz/totmass
		return (cenx,ceny,cenz)
	
	def writeStructure(self,filename):
		f=open(filename,"w")
		for atom in self.records:
			writepdbline(f,atom)
		f.close()
	
	def applyBioSym(self):
		oligomer=1
		natoms=len(self.records)
		for mat in self.biosym[1:]:
			print "Copy", oligomer
			oligomer+=1
			for n in range(natoms):
				newatom=copy.deepcopy(self.records[n])
				newatom['coords']=numpy.dot(self.records[n]['coords'],mat)
				self.records.append(newatom)

if __name__=="__main__":
	pdb1=PDBObj("1NSF.pdb")
	pdb1.applyBioSym()
	pdb1.writeStructure("1NSF_sym.pdb")
	pdb1.centerOfMass()
	print "Done!"
	
