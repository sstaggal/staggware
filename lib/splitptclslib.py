#!/usr/bin/env python

import os
from appionlib import apStack
from appionlib import apDatabase
from appionlib.apCtf import ctfdb as apCtf
import shutil
import copy
import sys
import frealignlib
import datetime
import math
import random
import numpy
#from matplotlib import pyplot


def modifyjob(jobin,outpath,jobout,threshold):
	#This could be much more robust by finding the cd line
	f=open(jobin,'r')
	lines=f.readlines()
	f.close()
	
		
	f=open(os.path.join(outpath,jobout),'w')
	for line in lines[:7]:
		f.write(line)
	f.write('cd %s\n' % (outpath))
	
	count=False
	for line in lines[8:]:
		# this is so hacky it makes me ill to write it
		if line[:12] == 'frealign.exe':
			count=True
			counter=0
		if count is True:
			if counter==6:
				words=line.split(',')
				f.write('%s,%s,%s,%.2f,%s,%s,%s,%s' % (words[0],words[1],words[2],threshold,words[4],words[5],words[6],words[7]))
			else:
				f.write(line)
			counter+=1
		else:
			f.write(line)

def logSplit(start,end,divisions):
	end=math.log(end)
	start=math.log(start)
	incr=(end-start)/divisions
	val=start
	stacklist=[]
	for n in range(0, divisions):
		nptcls=int(round(math.exp(val)))
		stacklist.append(nptcls)
		val+=incr
	print "Making stacks of the following sizes",stacklist
	return(stacklist)	

			
def findTargetCenters(particledata):
	"""returns a dictionary of the form: 
	parentholes{holeid:{'targets':[targetid1, targetid2],\
	'cols':[dcol1,dcol2],'rows':[drow1,drow2],'colcen':val,'rowcen':val}"""
	parentholes={}
	print "collecting hole data"
	n=0
	for ptcl in particledata:
		if n%1000 == 0:
			print '\tparticle', n
		parentholeid=ptcl['particle']['image']['target']['image'].dbid
		parentholename=ptcl['particle']['image']['target']['image']['filename']
		targetid=ptcl['particle']['image'].dbid
		targetname=ptcl['particle']['image']['filename']
		parentapix=apDatabase.getPixelSize(ptcl['particle']['image']['target']['image'])
		cols=ptcl['particle']['image']['target']['delta column']*parentapix
		rows=ptcl['particle']['image']['target']['delta row']*parentapix
		if parentholeid not in parentholes.keys():
			parentholes[parentholeid]={}
			parentholes[parentholeid]['targets']=[targetid]
			parentholes[parentholeid]['cols']=[cols]
			parentholes[parentholeid]['rows']=[rows]
		elif targetid not in parentholes[parentholeid]['targets']:
			parentholes[parentholeid]['targets'].append(targetid)
			parentholes[parentholeid]['cols'].append(cols)
			parentholes[parentholeid]['rows'].append(rows)
		n+=1
	print "finding centers"
	for key in parentholes:
		cols=numpy.array(parentholes[key]['cols'])
		rows=numpy.array(parentholes[key]['rows'])
		parentholes[key]['colcen']=cols.mean()
		parentholes[key]['rowcen']=rows.mean()
	return parentholes
		
				

def setupdirectory(set,options, basepath, newjobname='frealign.combine.sh'):
		subdir='set_%d' % (set)
		subdirpath=os.path.join(basepath,subdir)
		os.mkdir(subdirpath)
		modifyjob(options.jobfile,subdirpath,newjobname,options.threshold)
		os.system('ln -s %s %s/.' % (options.stackfile,subdirpath))
		return subdirpath,newjobname
	
def submitjobs(options, data=None, first=None, last=None, div=None):
	newjobname='frealign.combine.sh'
	paramlst=frealignlib.parseFrealignParamFile(options.paramfile)
	nptcls=len(paramlst)
	if len(paramlst)!=len(data):
		print "Error: # particles in frealign param file is different than in queried data"
		sys.exit()
		
	basedir=os.getcwd()
	for set in range(options.sets):
		newparamlst=copy.deepcopy(paramlst)
		### setup directory and files
		subdir, newjobname=setupdirectory(set, options, basedir, newjobname=newjobname)
		total=0		
		paramfilename=options.paramfile.split('/')[-1]
		f=open(os.path.join(subdir,paramfilename),'w')

		f.write ('C MinData\tMaxData\n')
		print 'set', set, 'range',first,last
		if type(first) == datetime.datetime:
			f.write('C %s\t%s\n' % (first.isoformat(),last.isoformat()))
		else:
			f.write ('C %e\t%e\n' % (first,last))
		for ptcl in range(nptcls):
			### eliminate out of range particles
			if data[ptcl] < first or data[ptcl] > last or newparamlst[ptcl]['score'] < options.threshold:
				newparamlst[ptcl]['score']=options.new_score
			else:
				total +=1 
			frealignlib.writeParticleParamLine(newparamlst[ptcl],f)

		print "Total particles", total
		f.write('C Total particles %d\n' % (total))
		f.close()
		first=last
		last=last+div
		print "submitting job: ", os.path.join(subdir,newjobname)
		submitjob(options,jobname=os.path.join(subdir,newjobname))
def submitphaseresidualjobs(options, data=None, first=None, last=None, div=None):
	newjobname='frealign.combine.sh'
	paramlst=frealignlib.parseFrealignParamFile(options.paramfile)
	nptcls=len(paramlst)
	if len(paramlst)!=len(data):
		print "Error: # particles in frealign param file is different than in queried data"
		sys.exit()
		
	basedir=os.getcwd()
	for set in range(options.sets):
		newparamlst=copy.deepcopy(paramlst)
		### setup directory and files
		subdir, newjobname=setupdirectory(set, options, basedir, newjobname=newjobname)
		total=0		
		paramfilename=options.paramfile.split('/')[-1]
		f=open(os.path.join(subdir,paramfilename),'w')

		f.write ('C MinData\tMaxData\n')
		print 'set', set, 'range',first,last
		if type(first) == datetime.datetime:
			f.write('C %s\t%s\n' % (first.isoformat(),last.isoformat()))
		else:
			f.write ('C %e\t%e\n' % (first,last))
		for ptcl in range(nptcls):
			### eliminate out of range particles
			if data[ptcl] < first or data[ptcl] > last:
				newparamlst[ptcl]['presa']=options.new_presa
			else:
				total +=1 
			frealignlib.writeParticleParamLine(newparamlst[ptcl],f)

		print "Total particles", total
		f.write('C Total particles %d\n' % (total))
		f.close()
		first=last
		last=last+div
		print "submitting job: ", os.path.join(subdir,newjobname)
		submitjob(options,jobname=os.path.join(subdir,newjobname))

def submitsquarejobs(options,sets):
	newjobname='frealign.combine.sh'
	paramlst=frealignlib.parseFrealignParamFile(options.paramfile)
	nptcls=len(paramlst)
	basedir=os.getcwd()
	for set in range(len(sets)):
		newparamlst=copy.deepcopy(paramlst)
		subdir, newjobname=setupdirectory(set, options, basedir, newjobname=newjobname)
		total=0		
		paramfilename=options.paramfile.split('/')[-1]
		f=open(os.path.join(subdir,paramfilename),'w')
		
		f.write('C Parent Square\n')
		f.write('C %s\n' % sets[set][2])
		
		for ptcl in range(nptcls):
			### eliminate out of range particles
			if ptcl < sets[set][0] or ptcl > sets[set][1] or newparamlst[ptcl]['presa'] > options.threshold:
				newparamlst[ptcl]['presa']=options.new_presa
			else:
				total +=1 
			frealignlib.writeParticleParamLine(newparamlst[ptcl],f)

		print "Total particles", total
		f.write('C Total particles %d\n' % (total))
		f.close()
		print "submitting job: ", os.path.join(subdir,newjobname)
		submitjob(options,jobname=os.path.join(subdir,newjobname))
		
def submitjob(options, jobname='frealign.combine.sh'):
	if options.setuponly is True:
		pass
	else:
		command = 'sbatch -p %s %s' % (options.queue,jobname)
		print command
		os.system(command)	

def shuffleparams(paramlst, paramfile, keep, threshold, newscore=0):
	nptcls=len(paramlst)
	candidates=[]	
	### find candidate particles with score greater than threshold
	### set all to newscore (less than threshold)
	for n in range(nptcls):
		if paramlst[n]['score'] > threshold:
			candidates.append([n,paramlst[n]['score']])
		paramlst[n]['score']=newscore
	
	
	### randomize the candidate particles and reset the kept ones to original presa
	random.shuffle(candidates)
	for candidate in candidates[:keep]:
		paramlst[candidate[0]]['score']=candidate[1]
	
	### write out the new param file
	for param in paramlst:
		frealignlib.writeParticleParamLine(param,paramfile)
	
# def shuffleparams_nsets(paramlst, paramfilerootname, keep, threshold, nsets, newpresa=90):
#	nptcls=len(paramlst)
#	candidates=[]	
#	### find candidate particles with presa less than threshold
#	### set all to newpresa (greater than threshold)
#	for n in range(nptcls):
#		if paramlst[n]['presa'] < threshold:
#			candidates.append([n,paramlst[n]['presa']])
#		paramlst[n]['presa']=newpresa
#	
#	
#	### randomize the candidate particles and reset the kept ones to original presa
#	random.shuffle(candidates)
#	for set in range(nsets):
#		paramfilename='%s_set%d.par'%(paramfilerootname,set)
#		f=open(paramfilename,'w')
#		for candidate in candidates[(set*keep):(set*keep)+keep]:
#			paramlst[candidate[0]]['presa']=candidate[1]
#	
#		### write out the new param file
#		for param in paramlst:
#			frealignlib.writeParticleParamLine(param,paramfile)
#		f.close()
#

def submitlogsplitjobs(options):
	newjobname='frealign.combine.sh'
	paramlst=frealignlib.parseFrealignParamFile(options.paramfile)
	nptcls=len(paramlst)
	ptclsets=logSplit(100,nptcls,options.sets)
	basedir=os.getcwd()
	for n in range(len(ptclsets)):
		### setup directory and files
		subdir, newjobname=setupdirectory(n, options, basedir, newjobname=newjobname)
		
		paramfilename=options.paramfile.split('/')[-1]
		f=open(os.path.join(subdir,paramfilename),'w')
		newparamlst=copy.deepcopy(paramlst)		
		shuffleparams(newparamlst,f,ptclsets[n], options.threshold)
		print "Total particles", ptclsets[n]
		f.write('C Total particles %d\n' % (ptclsets[n]))
		f.close()		
		print "submitting job: ", os.path.join(subdir,newjobname)
		submitjob(options,jobname=os.path.join(subdir,newjobname))

	
def splitimageshift(stackid, nsets):
	stackdataorig=apStack.getStackParticlesFromId(stackid)
	
	shiftdist=[]
	#f=open('shiftdist.txt','w')
	print "determining image shift for ptcls"
	for ptcl in stackdataorig:
		imageshift=ptcl['particle']['image']['scope']['image shift']
		d=(imageshift['x']**2+imageshift['y']**2)**0.5
		#f.write('%e\n' % (d))
		shiftdist.append(d)

	tmpshiftdist=copy.deepcopy(shiftdist)
	tmpshiftdist.sort()
	minshift=tmpshiftdist[0]
	maxshift=tmpshiftdist[-1]

	div=(maxshift-minshift)/nsets
	first=minshift
	last=first+div
	return shiftdist, first, last, div

def splittime(stackid, nsets):
	stackdataorig=apStack.getStackParticlesFromId(stackid)
	
	times=[]
	#f=open('shiftdist.txt','w')
	print "determining acquisition time for ptcls"
	for ptcl in stackdataorig:
		times.append(ptcl['particle']['image'].timestamp)
	firstpoint=times[0]
	lastpoint=times[-1]
	div=(lastpoint-firstpoint)/nsets
	
	first=firstpoint
	last=first+div
	return times, first, last, div

def splitsquare(stackid):
	#this function only works for particles that haven't been junk sorted
	stackdataorig=apStack.getStackParticlesFromId(stackid)
	
	#f=open('shiftdist.txt','w')
	print "determining square parent for ptcls"
	sets=[]
	square=stackdataorig[0]['particle']['image']['target']['image']['target']['image']
	squareid=square.dbid
	squarename=square['filename']
	print squarename
	first=0
	last=0
	for ptcl in range(len(stackdataorig)):
		parentsquare=stackdataorig[ptcl]['particle']['image']['target']['image']['target']['image']
		parentsquarename=parentsquare['filename']
		parentsquareid=parentsquare.dbid
		if parentsquareid!=squareid:
			### Below is a horrible hack to make sure that the same square with different versions will be counted the same
			if parentsquarename.split('_')[-1][0:1]=='v':
				if parentsquarename[:-4]!=squarename[:-4]:
					last=ptcl-1
					sets.append([first+1,last+1,squarename]) #add one b/c frealign starts at 1 not 0
					first=ptcl
					squarename=parentsquarename
					squareid=parentsquareid
					print sets[-1]
	return sets
		
def splitdefocus(paramfilepath,nsets):

	paramlst=frealignlib.parseFrealignParamFile(paramfilepath)
	nptcls=len(paramlst)
		
	defoclst=[x['df1'] for x in paramlst]
	tmpdefoclst=copy.deepcopy(defoclst)
	tmpdefoclst.sort()
	mindefocus=tmpdefoclst[0]
	maxdefocus=tmpdefoclst[-1]
	defoclst=numpy.array(defoclst)
	mean=defoclst.mean()
	std=defoclst.std()
	median=tmpdefoclst[len(tmpdefoclst)/2]
	print "stats",mean, std, median, mindefocus, maxdefocus
	
	maxrange=mean+2*std
	minrange=mean-2*std
	div=(maxrange-minrange)/nsets
	first=minrange
	last=first+div
	return defoclst, first, last, div
	
def splitholeposition(stackId, nsets):
	#abandon hope all ye who enter here
	#this is some seriously ugly code. good luck ever reading it again.
	stackdata=apStack.getStackParticlesFromId(stackId)
	parentholes=findTargetCenters(stackdata)
#	print parentholes
	ptclapix=apDatabase.getPixelSize(stackdata[0]['particle']['image'])
#	print ptclapix
	ptcldistance=[]
	print "determining ptcl distances"
	for ptcl in range(len(stackdata)):
		if ptcl%1000 == 0:
			print 'particle', ptcl
#	for ptcl in range(100):
		exposureid=stackdata[ptcl]['particle']['image'].dbid
		parentholeid=stackdata[ptcl]['particle']['image']['target']['image'].dbid

		#get particle coords
		ptclx=stackdata[ptcl]['particle']['xcoord']
		ptcly=stackdata[ptcl]['particle']['ycoord']
		imagesize=stackdata[ptcl]['particle']['image']['camera']['dimension']
		orix=imagesize['x']/2
		oriy=imagesize['y']/2
		#print imagesize, ptclx,ptcly, stackdata[ptcl]['particle']['image']['filename']

		#shift to the center
		ptclxcen=ptclx-orix
		ptclycen=oriy-ptcly
		#print ptclxcen, ptclycen

		#scale pixels to angstroms
		ptclxcen=ptclxcen*ptclapix
		ptclycen=ptclycen*ptclapix
		#print ptclxcen, ptclycen
		
		#find distance from target center to hole center
		parentapix=apDatabase.getPixelSize(stackdata[ptcl]['particle']['image']['target']['image'])
		cols=stackdata[ptcl]['particle']['image']['target']['delta column']*parentapix
		rows=stackdata[ptcl]['particle']['image']['target']['delta row']*parentapix

		dcols=cols-parentholes[parentholeid]['colcen']
		drows=rows-parentholes[parentholeid]['rowcen']
		
		dx=dcols+ptclxcen
		dy=drows+ptclycen
		d=math.sqrt(dx**2+dy**2)
#		print "particle", ptcl, "parent image" , stackdata[ptcl]['particle']['image']['filename']
#		print 'target dcols drows', dcols, drows, 'particle coords', ptclx, ptcly, 'particle offset', ptclxcen,ptclycen
#		print "distance", d
#		print
		ptcldistance.append(d)
		

	ptcldistance=numpy.array(ptcldistance)
	mean=ptcldistance.mean()
	std=ptcldistance.std()
	max=ptcldistance.max()
	min=ptcldistance.min()
	print "stats", mean, std, max, min
#	pyplot.hist(ptcldistance)
#	pyplot.show()
#	sys.exit()		
	div = (max-min)/nsets
	last = min + div
	return ptcldistance, min, last, div
	
def splitphaseresidual(paramfilepath,nsets):
	paramlst=frealignlib.parseFrealignParamFile(paramfilepath)
	nptcls=len(paramlst)
		
	presalst=[x['presa'] for x in paramlst]
	presalst=numpy.array(presalst)
	mean=presalst.mean()
	minpresa=presalst.min()
	maxpresa=presalst.max()
	std=presalst.std()
	print "stats",mean, std, minpresa, maxpresa
#	pyplot.hist(presalst)
#	pyplot.show()
	div=(maxpresa-minpresa)/nsets
	first=minpresa
	last=first+div
	return presalst, first, last, div

def splitptcldensity(stackId, nsets):
	#loop through particles and get ptcl density for parent image
	stackdata=apStack.getStackParticlesFromId(stackId)
	parentimages={}
	ptcldensity=[]
	print "collecting density data"
	for ptcl in range(len(stackdata)):
	#for ptcl in range(1010):
		if ptcl%1000 == 0:
			print '\tparticle', ptcl
		parentid=stackdata[ptcl]['particle']['image'].dbid
		if parentid not in parentimages:
			parentimages[parentid]=1
		else:
			parentimages[parentid]+=1
	#loop through particles and associate particle with density
	for ptcl in stackdata:
		parentid=ptcl['particle']['image'].dbid
		ptcldensity.append(parentimages[parentid])
		#print ptcl['particle']['image']['filename'], parentimages[parentid]
	
	ptcldensity=numpy.array(ptcldensity)
	maxdensity=ptcldensity.max()
	mindensity=ptcldensity.min()
	mean=ptcldensity.mean()
	std=ptcldensity.std()
	maxrange=mean+2*std
	minrange=mean-2*std
	div=(maxrange-minrange)/nsets
	first=minrange
	last=first+div
	print first, last, div, mean, std, mindensity, maxdensity
#	pyplot.hist(ptcldensity)
#	pyplot.show()
	return ptcldensity, first, last, div

def aceconfidence(stackId,nsets):
	print "Getting stack data"
	stackdata=apStack.getStackParticlesFromId(stackId)
	confidencelst=[]
	print "finding confidence values"
	n=0
	for ptcl in stackdata:
		if n%1000 == 0:
			print '\tparticle', n
		
		imgdata=ptcl['particle']['image']
		ctfdata=apCtf.getBestCtfValue(imgdata, msg=False)
		confidencelst.append(ctfdata['resolution_50_percent'])
		n+=1
	confidencelst=numpy.array(confidencelst)
	maxconfidence=confidencelst.max()
	minconfidence=confidencelst.min()
	mean=confidencelst.mean()
	std=confidencelst.std()
	div=(maxconfidence-minconfidence)/nsets
	first=minconfidence
	last=first+div
#	pyplot.hist(confidencelst)
#	pyplot.show()
#	sys.exit()
	print first, last, div, mean, std, minconfidence, maxconfidence
	return confidencelst, first, last, div

def titrateconfidence(stackId,nsets):
	print "Getting stack data"
	stackdata=apStack.getStackParticlesFromId(stackId)
	confidencelst=[]
	print "finding confidence values"
	n=0
	for ptcl in stackdata[:5000]:
		if n%1000 == 0:
			print '\tparticle', n
		
		imgdata=ptcl['particle']['image']
		ctfdata=apCtf.getBestCtfValue(imgdata, msg=False)
		confidencelst.append(ctfdata['resolution_80_percent'])
		n+=1
	
	nptcls=len(confidencelst)
	confidencelst.sort()
	percentinclude=0.8
	cutoffindex=int(round(nptcls*percentinclude))
	remaining=nptcls-cutoffindex
	interval=remaining/nsets
	cutofflst=[]
	for nset in range(nsets):
		cutoffindex=cutoffindex+interval*nset
		print cutoffindex, len(confidenclst)
		cutoff=confidencelst[cutoffindex]
		print cutoffindex,cutoff
		cutofflst.append(cutoff)
		
	print first, last, div, mean, std, minconfidence, maxconfidence
	return confidencelst, first, last, div
