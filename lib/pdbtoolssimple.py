#!/usr/bin/env python

"This module provides a set of tools for parsing, measuring, and modifying pdb files"

import math,sys
	
def readpdb(pdbfilename):
	"""Parse pdb and return records as a list of dictionaries"""
	pdbfile=open(pdbfilename,'r')
	lines=pdbfile.readlines()
	pdbfile.close()
	
	#parse the pdb
	records=[]
	for line in lines:
		if line[:4]=="ATOM" or line[:6]=="HETATM":
			d={}
			d['rtype']=line[0:6]
			d['atomnumber']=int(line[6:11])
			d['atomtype']=line[12:16]
			d['altloc']=line[16:17]
			d['residue']=line[17:20]
			d['chain']=line[21:22]
			d['residuenumber']=int(line[22:26])
			d['icode']=line[26:27]
			d['x']=float(line[30:38])
			d['y']=float(line[38:46])
			d['z']=float(line[46:54])
			d['occupancy']=float(line[54:60])
			d['tempfact']=float(line[60:66])
			d['element']=line[76:78].strip()
			d['charge']=line[78:80]
			records.append(d)
	
	return records

def writepdbline(outfile, d):
	"Write a line to outfile in pdb format. d must be a dictionary containing records for an atom"
	outfile.write("%-6s%5d %-4s%1s%-3s %1s%4d%1s   %8.3f%8.3f%8.3f%6.2f%6.2f          %1s%-2s\n" % (d['rtype'],d['atomnumber'],d['atomtype'],d['altloc'],d['residue'],d['chain'],d['residuenumber'],d['icode'],d['x'],d['y'],d['z'],d['occupancy'],d['tempfact'],d['element'],d['charge']))

if __name__ == '__main__':
	atoms=readpdb("2FA9.pdb")	
	print atoms[0]

