#!/usr/bin/env python

import numpy as np
from pyami import fftengine as ffteng
from pyami import imagefun
from pyami import mrc

##Bad Rows for final image
#badrow = [1286]

##Bad Rows for frames
badrow = [2809]

##Bad Rows for Fourier
fftrow = [-1024, 0, 1024]

##The width of the stripes
width = 128


def findDifference(array):
	difference=[]
	for i in array:
		difference.append(array[0]-i)

	return difference

def findMean(img):
	[ysize, xsize] = np.shape(img)
	img_tmp = imagefun.replaceBadRowsAndColumns(img, badrow)

	yintegration = np.mean(img_tmp, axis=0)
	yintegration = yintegration.reshape(xsize/width,-1).mean(axis=1)

	return yintegration

def correctImg(img, difference):
	[ysize, xsize] = np.shape(img)
	img_tmp = imagefun.replaceBadRowsAndColumns(img, badrow)

	correctionmatrix = np.repeat(difference, width, axis=0)
	correctionmatrix = np.tile(correctionmatrix, (ysize,1))

	ic = img_tmp + correctionmatrix

	return ic

def correctFFT(img):
	tmpimg = ffteng.real_fft2d(img)
	for i in fftrow:
		tmpimg[i, :] = 0
	tmpimg = ffteng.inverse_real_fft2d(tmpimg)

	return tmpimg


def correctFrames(filename):
	img = mrc.read(filename)
	for i in range(len(img)):
		img[i] = correctFFT(img[i])
		mean = findMean(img[i])
		dif = findDifference(mean)
		temp = correctImg(img[i], dif)
		img[i] = temp
	return img

