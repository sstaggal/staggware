#!/usr/bin/env python

import numpy as np
from scipy import misc
from scipy import ndimage
from matplotlib import pyplot as plt
import matplotlib.ticker as mticker
import math
import types
import cmath
from optparse import OptionParser

def ctf(s,elambda=2.238e-12,defocus=1e-6,Cs=2.7*1e-3,Ca=0.07,cutoff=0,invert=1.0,Bf=0.0,plot=False):
	if plot:
		#x,y=np.shape(s)
#		flats=flat(s)
#		#print np.shape(flats)			
#		rm=np.zeros((1,100+(x*y)/2))
#		print "Creating the CTF plot, this may take a while...",
#		for l in range(x*y):
#			if l//1000==0:
#				print ".",
#			rand=np.random.uniform()
#			if rand<0.5:
#				np.append(rm,l)
#		#print np.shape(rm)
#		flats=np.delete(flats,rm)
#		flats.sort()
		#freq=np.fft.fftfreq(400,1e-10)
		#freq=s
		x=np.sort(s)
		#x=np.zeros((m/2,))
#		for i in range(m/2):
#			x[i]=freq[i]
		gamma=2*np.pi*((-Cs*(elambda**3)*(x**4))/4 + (defocus*elambda*(x**2))/2)
		ctfunc=(np.sqrt(1-(Ca**2)))*(np.sin(gamma))+Ca*(np.cos(gamma))
		ctfunc*=-1*invert
		B=Bf*1e-20
		envelope=np.exp(-B*(x**2))
		modctfunc=ctfunc*envelope
		#print freq[20000]		
		return x,modctfunc,defocus
	elif s>cutoff:
		gamma=2*np.pi*((-Cs*(elambda**3)*(s**4))/4 + (defocus*elambda*(s**2))/2)
		ctfunc=(np.sqrt(1-(Ca**2)))*(np.sin(gamma))+Ca*(np.cos(gamma))
		ctfunc*=-1*invert
		B=Bf*1e-20
		envelope=np.exp(-B*(s**2))
		modctfunc=ctfunc*envelope
		return modctfunc
	else:
		return 1


#def ftrans(imgarray):
#	ft=np.fft.fft2(imgarray)
#	ftshift=np.fft.fftshift(ft)
#	x,y=np.shape(ftshift)
#	sx=np.zeros((x,y))
#	sy=np.zeros((x,y))
#	for i in range(x):
#		for j in range(y):
#			sx[i][j]=ftshift[i][j].real
#			sy[i][j]=ftshift[i][j].imag
#	return ftshift,sx,sy

def ftrans(imgarray):
	ft=np.fft.fft2(imgarray)
	ftshift=np.fft.fftshift(ft)
	#x,y=np.shape(ft)
#	sx=np.zeros((x,y))
#	sy=np.zeros((x,y))
#	for i in range(x):
#		for j in range(y):
#			sx[i][j]=ft[i][j].real
#			sy[i][j]=ft[i][j].imag
	return ftshift,ft	
	
#def ctfmod(*frq,**yfrq):
#	out=[]	
#	for s in frq:
#		x,y=np.shape(s)
#		modsx=np.zeros((x,y))
#		for i in range(x):
#			for j in range(y):
#				modsx[i][j]=s[i][j]*np.abs(ctf(s[i][j]))
#				#modsx[i][j]=s[i][j]*ctf(s[i][j])
#		out.append(modsx)
#	
#	
#	for k in sorted(yfrq.keys()):
#		
#		s=yfrq[k]
#		
#		x,y=np.shape(s)
#		modsy=np.zeros((x,y))		
#		for i in range(x):
#			for j in range(y):
#				modsy[i][j]=s[i][j]*np.abs(ctf(s[i][j]))
#		out.append(modsy)
#	mods=np.zeros((x,y),dtype=np.complex_)
#	for i in range(x):
#			for j in range(y):
#				mods[i][j]=complex(modsx[i][j],modsy[i][j])
#				
#				
#	
#	out.append(mods)
#	return out

def ctfmod(*ft,**ctfparams):
	out=[]	
#	for s in frq:
#		x,y=np.shape(s)
#		sx=s
#	
#	for k in sorted(yfrq.keys()):		
#		s=yfrq[k]		
#		x,y=np.shape(s)
#		sy=s
#		
#	s=np.zeros((x,y),dtype=np.complex_)
#	for i in range(x):
#			for j in range(y):
#				s[i][j]=complex(sx[i][j],sy[i][j])
	s=ft[0]
	x,y=np.shape(s)
	
	ctfpars=ctfparams[ctfparams.keys()[0]]
	#abberation=ctfpars[0]
#	defoc=ctfpars[1]
#	ampcontrast=ctfpars[2]
	
	freq=np.fft.fftfreq(y,1e-10)
	
	
	
#	modsy=np.zeros((x,y))
#	modsx=np.zeros((x,y))

	smagl=np.ndarray((x*y/4,))
	mods=np.zeros((x,y),dtype=np.complex_)
	for i in range(x):
		k=0
		for j in range(y):
			xp=freq[j]
			yp=freq[i]
			smag=np.sqrt(xp**2 + yp**2)
			#np.append(smagl,smag)
			#if smag<0.005*1e10 and smag>0.003*1e10:
				#mods[i][j]=s[i][j]
			mods[i][j]=s[i][j]*ctf(smag,defocus=ctfpars['defocus']*1e-6,Cs=ctfpars['Cs']*1e-3,Ca=ctfpars['Ca'],invert=ctfpars['invert'],Bf=ctfpars['Af'])
			#elif smag<1*1e10 and smag>0.1*1e10:
				#mods[i][j]=s[i][j]*ctf(smag,defocus=ctfpars['defocus']*1e-6,Cs=ctfpars['Cs']*1e-3,Ca=ctfpars['Ca'],invert=ctfpars['invert'],Bf=ctfpars['Af'])
				#mods[i][j]=s[i][j]
				#mods[i][j]=0
			#else:
				#mods[i][j]=0
			k+=1
	z=0
	for i in range(x/2):
		for j in range(y/2):
			xp=freq[j]
			yp=freq[i]
			smag=np.sqrt(xp**2 + yp**2)
			smagl[z]=smag
			z+=1	
#	out.append(modsx)
#	out.append(modsy)
	out.append(mods)
	out.append(smagl)
	return out

		
def iftrans(*arg,**kwarg):
	
	if arg:
		sx=arg[0]
		sy=arg[1]
		x,y=np.shape(sx)
		frq=np.ndarray((x,y),dtype=np.complex_)	
		for i in range(x):
			for j in range(y):
				frq[i][j]=complex(sx[i][j],sy[i][j])
								
		shift=np.fft.ifftshift(frq)
		inv=np.fft.ifft2(shift)
		absinv=np.abs(inv)		
		
	
	if kwarg:
		k=kwarg.keys()
		f=kwarg[k[0]]
		#shift=np.fft.ifftshift(f)
		inv=np.fft.ifft2(f)
		absinv=np.abs(inv)
		
				
	return inv,absinv
	
	
def display(img,brightness=0.1,contrast=0.0,hist=False):
	x,y=np.shape(img)
	raisepower=np.zeros((x,y))
	xhist=np.zeros((1,x*y))
	k=0
	for i in range(x):
			for j in range(y):
				raisepower[i][j]=math.pow(img[i][j],brightness)
				xhist[0][k]=img[i][j]
				k+=1
	final=raisepower*(255-contrast)/raisepower.max() + contrast
	
	if hist:
		k=0
		i=0
		j=0
		xhist.sort()
		sfinal=np.zeros((1,x*y))		
		for i in range(x):
			for j in range(y):
				sfinal[0][k]=final[i][j]
				k+=1		
		sfinal.sort()		
		rm=np.zeros((1,100+(x*y)/25))
		for l in range(x*y):
			rand=np.random.uniform()
			if rand<0.25:
				np.append(rm,l)
		shortxhist=np.delete(xhist,rm)
		shortsfinal=np.delete(sfinal,rm)		
		plt.plot(shortxhist,shortsfinal)
		ylabel='Brightness = %2.1f' %brightness
		plt.ylabel(ylabel)
				
	return final
	

def histogram(freq,plot=False):
	arange=freq.max()-freq.min()
	abin=arange/(freq.max()/freq.min())
	print abin
	x,y=np.shape(freq)
	logarray=np.zeros((x,y))
	for i in range(x):
		#print i
		for j in range(y):
			#print freq[i][j]
			if freq[i][j]//abin:
				logarray[i][j]=np.log10(freq[i][j]//abin)
	maxlog=logarray.max()
	hist=ndimage.histogram(logarray,0,maxlog,maxlog)
	xhist=np.linspace(0,maxlog,maxlog)
	if plot:
		xlabel="x represents the power of ten in %2.1f*(10**x)" %abin
		ylabel="Number of occurance out of %d" % (x*y)
		#plt.plot(xhist,hist) 
#		plt.xlabel(xlabel)
#		plt.ylabel(ylabel)
#		plt.title('Histogram')
#		print "run the plt.show() command to see the diagram"
	return xhist,hist,xlabel,ylabel
	
	

	
	
def prepare(filename,resize=(400,400),rotate=270,outfile='modified_img.jpg'):
	
	inputimage=misc.imread(filename,flatten=1)
	x,y=np.shape(inputimage)
	#scale=0
	#img_resized=np.ndarray()
	if type(resize)==types.TupleType:
	
		if x/4>resize[0] and y/4>resize[1]:
			scale=25
		elif x/2>resize[0] and y/2>resize[1]:
			scale=50
		elif x*3/4>resize[0] and y*3/4>resize[1]:
			scale=75
		else:
			m=np.min([x,y])
			l=[]
			l.append(m-10)
			l.append(m-10)
			resize=tuple(l)
		try:
		#if scale=0:
			img_resized=misc.imresize(inputimage,scale)
		except:
			pass
		try:
		#if img_resized.any():
			img_rot=ndimage.rotate(img_resized,rotate)
		except:
			#pass
		#else:
			img_rot=ndimage.rotate(inputimage,rotate)

		w,z=np.shape(img_rot)
		resized=img_rot[w/2-resize[0]/2:w/2+resize[0]/2:,z/2-resize[1]/2:z/2+resize[1]/2:]
	else:
		img_resized=misc.imresize(inputimage,resize)
		resized=ndimage.rotate(img_resized,rotate)
	#print np.shape(resized)
	
	
	misc.imsave(outfile,resized)
	return resized


def maketick(x,pos):

	if x==1:
		newx=round(0.5/x,3)
		return 'eq. %2.1f\n 1/pix (Nyquist)' %(newx)
	elif x==0:
		return 'eq. inf/pix' 
	else:
		newx=round(0.5/x,3)
		return 'eq. %2.1f\n 1/pix' %(newx)
	
def flat(a):
	x,y=np.shape(a)
	flata=np.zeros((1,x*y))
	k=0
	for i in range(x):
		for j in range(y):
			flata[0][k]=a[i][j]
			k+=1
	return flata
	
	
def cmlinearguments():
	parser=OptionParser()
	parser.add_option('--input',dest='filename', help="Input image")
	parser.add_option('--output',dest='outname', help="The root name and the desired format for the gray-scale version of the image and the CTF-modulated image")
	parser.add_option('--rotate',dest='rot',type='float',default=0,help="Rotates the input image counter-clockwise")
	parser.add_option('--resize',dest='resize',type='int',help="Percentage(integer) for resizing the input image for faster processing (default=100)") 
	parser.add_option('--crop',dest='crop',help="Two integers separated by a comma (default=400,400 in pixels) with which the input image is cropped around its center.\n The input image is alwayes scaled down before being cropped.\n The main purpose of this option is to make square (equal-dimension) images.") 
	parser.add_option('--ctf_params',dest='ctfparams',type='str',help="Assigns CTF parameters using three values.\n The default is 2.7,1,0.07 for (Spherical aberration(mm),Defocus(micron),Amplitude contrast(<1)). You can change these parameters later interactively.")
	return parser

def parser(string):
	
	values=string.split(',')
	l=[]
	for value in values:
		parsedvalue=float(value)
		l.append(parsedvalue)
	outtuple=tuple(l)
	return outtuple
	
	
	
def makehtml(figure,filename):
	h=open(filename,'w')
	h.write('<!DOCTYPE html>\n')
	h.write('<title> Contrast Transfer Function </title>\n')
	h.write('<div class="top">\n')
	h.write('<head>\n')
	h.write('	<h1> A Quick Lesson on Contrast Transfer Function Theory in Cryo-EM </h1>\n')
	h.write('	<style>\n')
	
	h.write('		.top {background-color:blue}\n')
	h.write('		h1 {text-align: center;color:white;}\n')
	
	
	h.write('	</style>\n')
	h.write('</head>\n')
	h.write('</div>\n')
	h.write('<body>\n')
	
	h.write("	<p> Because of the imperfections of an electron microscope's magnetic lens, the final image is degraded to a certain amount. This degradation is estimated by contrast transfer function(ctf) theory.")
	h.write("		CTF is a function of spherical aberration of the lens, amplitude contrast, and defocus. Of those three parameters, only defocus is variable and has")
	h.write("		varibale effects on the image. This program is intended to demonstrate those effects using not EM images, but images of people and places.</p>\n")
	
	s="	<img src=%s height='1000px' width='1500px' /img>\n" %figure
	h.write(s)		
	
			
	h.write('</body>\n')
	h.write('</html>\n')
	h.close()


def runtimeparser(string):
	params={'ctfparams':(2.7,1,0.07),'af':5,'inv':1.0,'d':1,'s':2.7,'ac':0.07,'ind':0.0,'ins':0.0,'inac':0.0,'inaf':0.0}
	args=string.split('--')
	args.pop(0)
	update=[0,0,0,0,0,0,0,0,0,0]
	for arg in args:
		find=arg.find('ctfparams')
		find1=arg.find('ampf')
		find2=arg.find('inv')
		find3=arg.find('dfcs')
		find4=arg.find('sa')
		find5=arg.find('ampc')
		find6=arg.find('ind')
		find7=arg.find('ins')
		find8=arg.find('inac')
		find9=arg.find('inaf')
		if find!=-1:
			#l1=arg.index('(')+1
#			l2=arg.index(')')
#			values=arg[l1:l2]
			values=arg.split()
			value=values[1].split(',')
			update[0]=1
			m=[]
			#try:
			for n in value:
				#try:
				m.append(float(n))
#				except:
#					print "Please make sure you provide all the three values and only with commas separating them."
#				else:
#					return
			
			params['ctfparams']=tuple(m)
		
		elif find1!=-1:
			values=arg.split()
			params['af']=float(values[1])
			update[1]=1
		elif find2!=-1:
			values=arg.split()
			params['inv']=float(values[1])
			update[2]=1
		elif find3!=-1:
			values=arg.split()
			params['d']=float(values[1])
			update[3]=1
		elif find4!=-1:
			values=arg.split()
			params['s']=float(values[1])
			update[4]=1
		elif find5!=-1:
			values=arg.split()
			params['ac']=float(values[1])
			update[5]=1
		elif find6!=-1:
			values=arg.split()
			params['ind']=float(values[1])
			update[6]=1
		elif find7!=-1:
			values=arg.split()
			params['ins']=float(values[1])
			update[7]=1
		elif find8!=-1:
			values=arg.split()
			params['inac']=float(values[1])
			update[8]=1
		elif find9!=-1:
			values=arg.split()
			params['inaf']=float(values[1])
			update[9]=1
	return params,update
		
			

def parammap(params,update,ctfpars):
	#ctfpars={defocus:1,Cs:2.7,Ca:0.07,Af:5.0,invert:1.0}
	
	if update[0]==1:
		ctfpars['defocus']=params['ctfparams'][1]
		ctfpars['Cs']=params['ctfparams'][0]
		ctfpars['Ca']=params['ctfparams'][2]
	if update[1]==1:
		ctfpars['Af']=params['af']
	if update[2]==1:
		ctfpars['invert']=params['inv']
	if update[3]==1:
		ctfpars['defocus']=params['d']
	if update[4]==1:
		ctfpars['Cs']=params['s']
	if update[5]==1:
		ctfpars['Ca']=params['ac']
	if update[6]==1:
		ctfpars['defocus']+=params['ind']
	if update[7]==1:
		ctfpars['Cs']+=params['ins']
	if update[8]==1:
		ctfpars['Ca']+=params['inac']
	if update[9]==1:
		ctfpars['Af']+=params['inaf']
	
	return ctfpars




#def myplot(string,img,ax1):
#	l=string.split()
#	print string
#	print l
#	n=len(l)
#	#axes=[ax1,ax2,ax3,ax4,ax5,ax6,ax7]
#	plt.ion()
#	
#	
#	fig=plt.figure()
#	
#	fig.set_size_inches(18.5,16.0,forward=True)
#
#	for i in range(n):
#		#m=axes[i]
#		m=fig.add_subplot(2,1,i+1)
#		
#		if l[i]=='i':
#
#			m.imshow(img,cmap='gray')
#			m.get_xaxis().set_visible(False)
#			m.get_yaxis().set_visible(False)	
#		
#		elif l[i]=='ip':
#			m.imshow(ps_ftshift,cmap='gray')
#			m.get_xaxis().set_visible(False)
#			m.get_yaxis().set_visible(False)
#		elif l[i]=='ctf':
#			#m=fig.add_subplot(323)
#			axes[-1]=m.twiny()
#			m.set_ylim([-1.0,1.0])
#			m.xaxis.set_major_locator(mticker.MaxNLocator(7))
#			axes[-1].xaxis.set_major_locator(mticker.LinearLocator(7))
#			axes[-1].xaxis.set_major_formatter(mticker.FuncFormatter(ctflib.maketick))
#			plt.axhline(0, color='black')
#			m.plot(q,ctf)
#			frqrange=[0.0,np.sqrt(2)*0.5e10]
#			m.set_xlim(frqrange)
#			m.set_xlabel('frequency(1/A)')
#			m.set_ylabel('CTF (DEFOCUS=%2.1e)'%defoc)
#		elif l[i]=='op':
#			m.get_xaxis().set_visible(False)
#			m.get_yaxis().set_visible(False)
#			m.imshow(ps_mods,cmap='gray')
#		elif l[i]=='o':
#			m.imshow(absinv,cmap='gray')
#			m.get_xaxis().set_visible(False)
#			m.get_yaxis().set_visible(False)
#		elif l[i]=='eq':
#			equation=misc.imread('ctf2.jpg')
#			#m=fig.add_subplot(326)
#			m.imshow(equation)
#			m.get_yaxis().set_visible(False)
#			for tic in m.xaxis.get_major_ticks():
#    				tic.tick1On = tic.tick2On = False
#    				tic.label1On = tic.label2On = False
#			mlabel='Defocus=%2.1f(micron),   Spherical Ab.=%2.1f(mm),   Amp. Contrast=%2.1f\n\n    B-factor=%2.1f(Angstrom-squared)' %(ctfparams['defocus'],ctfparams['Cs'],ctfparams['Ca'],ctfparams['Af'])
#			m.set_xlabel(mlabel)
#	plt.draw()
#	
#	return

























	
	
	

