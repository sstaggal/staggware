#!/usr/bin/env python

import csv
import sys
import glob
#Two star files. 



#takes in a star file and outputs a list with ((column name, column number -1))
def parse_star(file):
        ssvin = open(file, 'rb')
        reader=csv.reader(ssvin, delimiter=' ')
        number_of_arguments = 0
        argument_list = []
        for row in reader:
                row = filter(None, row)
                if len(row) == 2:
                        number_of_arguments += 1
                        num = row[1]
                        num =int(num.replace("#",""))
                        argument_list.append(row[0])
        ssvin.close()
        return argument_list


def parse_par(file):
	ssvin = open(file, 'rb')
	reader=csv.reader(ssvin, delimiter=' ')
	number_of_arguments = 0
	argument_list = []
	row = next(reader)
	row = filter(None, row)
	ssvin.close()
	return row

parfiles = glob.glob('*.par')
star_args = [' ', 'data_images',' ', 'loop_', '_rlnImageName #1', '_rlnDefocusU #2', '_rlnDefocusV #3', '_rlnDefocusAngle #4', '_rlnVoltage #5', '_rlnSphericalAberration #6', '_rlnAmplitudeContrast #7', '_rlnOriginX #8', '_rlnOriginY #9', '_rlnAngleRot #10', '_rlnAngleTilt #11', '_rlnAnglePsi #12']






for par in parfiles:
	par_split = par.split('_')
	if par_split[0] == 'output':
		if par_split[2] == sys.argv[1]:
			star_out = open('output_star_'+sys.argv[1]+'_'+par_split[3].split('.')[0]+'.star', 'wb')
			writer = csv.writer(star_out, delimiter=' ', lineterminator='\n')
			for arg in star_args:
				star_out.write(arg)
				star_out.write("\n")
			par_1 = open(par, 'rb')
			reader = csv.reader(par_1, delimiter=' ')
			par_arg = parse_par(par)
			for row in reader:
				row = filter(None, row)
				if row[0] != 'C':
					if row[par_arg.index('OCC')] == '100.00':
						writer.writerow([row[par_arg.index('C')]+'@particles.mrcs', row[par_arg.index('DF1')], row[par_arg.index('DF2')], row[par_arg.index('ANGAST')], 300, 2.7, 0.07, float(row[par_arg.index('SHX')])/(-float(sys.argv[2])), float(row[par_arg.index('SHY')])/(-float(sys.argv[2])), row[par_arg.index('PHI')], row[par_arg.index('THETA')], row[par_arg.index('PSI')]])
			par_1.close()	
			star_out.close()



