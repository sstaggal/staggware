#!/usr/bin/env python

import numpy as np
import math
import types
import cmath
from optparse import OptionParser
import sys


class bcolors:

	OKBLUE = '\033[94m'
	ENDC = '\033[0m'
	
	
def freq(x):
	y = (1/x)*(1e10)
	return y

def ctf(s,elambda=1.97e-12,defocus=1e-6,Cs=2.7*1e-3):
	ss=freq(s)
	gamma=(-Cs*(elambda**3)*(ss**4))/4 + (defocus*elambda*(ss**2))/2
	return gamma
		

def deloc(res, defoc):
	
	distance_delocalized=res*ctf(res,defocus=defoc*(1e-6))
	return distance_delocalized

def cmlinearguments():
	parser=OptionParser()
	parser.add_option('--resolution',dest='resolution', type='float',default=3, help="Resolution for which you want to calculate delocalization")
	parser.add_option('--defocus',dest='defocus', type='float',default=3, help="Defocus at which you want to calculate delocalization")
	return parser	
			
		
parser=cmlinearguments()
if len(sys.argv)<2:
	parser.print_help()
	sys.exit()

					

options,args=parser.parse_args()

				
if options:

	distance_delocalized=deloc(options.resolution,options.defocus)
	
print "At "+bcolors.OKBLUE+ str(options.defocus)+ bcolors.ENDC+" microns of defocus, the " +bcolors.OKBLUE+str(options.resolution) +bcolors.ENDC+ " Angstrom information is delocalized by " + bcolors.OKBLUE+str(distance_delocalized) +bcolors.ENDC+ " Angstroms."


