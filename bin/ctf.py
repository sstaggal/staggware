#!/usr/bin/env python

###This program is intended to serve as an edcutional aid for teaching the theory of contrast transfer function in cryo-EM###
###Instead of processing cryo-EM images, this program takes pictures of people and places and modulates them with a CTF###
###All the steps are automated, so the input can be any color image, and the program will initially modify the input for later processing###


import ctflib
from scipy import misc
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.ticker as mticker
import sys

class bcolors:
	RED = '\033[93m'
	OKBLUE = '\033[94m'
	MAG = '\033[95m'  
	ENDC = '\033[0m'
    
    
    
parser=ctflib.cmlinearguments()
if len(sys.argv)<2:
	parser.print_help()
	sys.exit()

					

options,args=parser.parse_args()

				
if options.crop:
	values=ctflib.parser(options.crop)
	img=ctflib.prepare(options.filename,resize=values,rotate=options.rot,outfile=options.outname)
elif options.resize:
	img=ctflib.prepare(options.filename,resize=options.resize,rotate=options.rot,outfile=options.outname)
else:
	img=ctflib.prepare(options.filename,resize=(400,400),rotate=options.rot,outfile=options.outname)

					 

ftshift,s=ctflib.ftrans(img)
ps_ftshift=ctflib.display(np.abs(ftshift)**2)

plt.ion()
fig=plt.figure(1)
fig.set_size_inches(18.5,16.0,forward=True)

ax1=fig.add_subplot(321)
ax1.imshow(img,cmap='gray')
ax1.get_xaxis().set_visible(False)
ax1.get_yaxis().set_visible(False)
ax2=fig.add_subplot(322)
ax2.imshow(ps_ftshift,cmap='gray')
ax2.get_xaxis().set_visible(False)
ax2.get_yaxis().set_visible(False)

ax3=fig.add_subplot(323)
ax4=ax3.twiny()
ax3.set_ylim([-1.0,1.0])
#ax3.set_xlabel('frequency(1/A)')
ax3.xaxis.set_major_locator(mticker.MaxNLocator(7))
ax4.xaxis.set_major_locator(mticker.LinearLocator(7))
ax4.xaxis.set_major_formatter(mticker.FuncFormatter(ctflib.maketick))
plt.axhline(0, color='black')

ax5=fig.add_subplot(324)
ax5.get_xaxis().set_visible(False)
ax5.get_yaxis().set_visible(False)

ax6=fig.add_subplot(325)
ax6.get_xaxis().set_visible(False)
ax6.get_yaxis().set_visible(False)

equation=misc.imread('ctf2.jpg')
ax7=fig.add_subplot(326)
ax7.imshow(equation)
#ax7.get_xaxis().set_visible(False)
ax7.get_yaxis().set_visible(False)
for tic in ax7.xaxis.get_major_ticks():
    tic.tick1On = tic.tick2On = False
    tic.label1On = tic.label2On = False

ctfpars={'defocus':1,'Cs':2.7,'Ca':0.07,'Af':0.0,'invert':1.0}

def process(ctfparams,s=s):
						
	mods,freq=ctflib.ctfmod(s,ctfpars=ctfparams)
	
	
	q,ctf,defoc=ctflib.ctf(freq,defocus=ctfparams['defocus']*1e-6,Cs=ctfparams['Cs']*1e-3,Ca=ctfparams['Ca'],invert=ctfparams['invert'],Bf=ctfparams['Af'],plot=True)
	
	ax3.cla()
	ax3.plot(q,ctf)
	frqrange=[0.0,np.sqrt(2)*0.5e10]
	ax3.set_xlim(frqrange)
	ax3.set_xlabel('frequency(1/A)')
	ax3.set_ylabel('CTF (DEFOCUS=%2.1e)'%defoc)
	plt.draw()

	inv,absinv=ctflib.iftrans(n=mods)
	ps_mods=ctflib.display(np.abs(np.fft.fftshift(mods))**2)
	
	ax5.cla()
	ax5.imshow(ps_mods,cmap='gray')
	plt.draw()
						
	ax6.cla()
	ax6.imshow(absinv,cmap='gray')
	plt.draw()
	
	ax7label='Defocus=%2.1f(micron),   Spherical Ab.=%2.1f(mm),   Amp. Contrast=%2.1f\n\n    B-factor=%2.1f(Angstrom-squared)' %(ctfparams['defocus'],ctfparams['Cs'],ctfparams['Ca'],ctfparams['Af'])
	ax7.set_xlabel(ax7label)
	plt.draw()
	
	return absinv


params=ctflib.parser(options.ctfparams)
ctfpars['Cs']=params[0]
ctfpars['defocus']=params[1]
ctfpars['Ca']=params[2]

	
absinv=process(ctfparams=ctfpars)

ctfhelp0=bcolors.RED+'\n\n\nNow you can change some parameters interactively. There are three ways to do so:\n'+bcolors.ENDC+'A)'+bcolors.OKBLUE+ ' --ctfparams'+bcolors.ENDC+' spherical aberration(mm),defocus(micron),amplitude contrast\n'
ctfhelp1='		Please make sure with this option you provide all the three values with commas separating them only. Example:'+bcolors.OKBLUE+ ' --ctfparams 2.7,5,0\n'+bcolors.ENDC
ctfhelp2='B) You can change the parameters individually:\n	I) To change'+bcolors.OKBLUE+' DEFOCUS'+bcolors.ENDC+' to a value:'+bcolors.OKBLUE+ ' --dfcs value(micron)\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+ ' --dfcs 3.5\n'+bcolors.ENDC
ctfhelp3='	II) To change'+bcolors.OKBLUE+' SPHERICAL ABERRATION'+bcolors.ENDC+' to a value:'+bcolors.OKBLUE+ ' --sa value(mm)\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+ ' --sa 3\n'+bcolors.ENDC
ctfhelp4='	III) To change'+bcolors.OKBLUE+' AMPLITUDE CONTRAST'+bcolors.ENDC+' to a value:'+bcolors.OKBLUE+ ' --ampc value(<1)\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+ ' --ampc 0.1\n'+bcolors.ENDC
ctfhelp5='	IV) To change'+bcolors.OKBLUE+' B factor'+bcolors.ENDC+' (amplitude falloff) to a value:'+bcolors.OKBLUE+' --ampf value(Angstrom-squared, start with 25)\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+' --ampf 25\n'+bcolors.ENDC
ctfhelp6='	V) To invert all the phases:'+bcolors.OKBLUE+ ' --inv -1 (to invert back: --inv 1)\n'+bcolors.ENDC
ctfhelp7='C) You can change the values by increments:\n'
ctfhelp8='	I) To change defocus by an increment:'+bcolors.OKBLUE+ ' --ind value(micron)\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+ ' --ind 0.5'+bcolors.ENDC+' (adds 0.5 micron to the current value)\n'
ctfhelp9='	II) To change spherical aberration by an increment:'+bcolors.OKBLUE+ ' --ins value(mm)\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+ ' --ins -2'+bcolors.ENDC+' (subtracts 2 from the current value)\n'
ctfhelp10='	III) To change amplitude contrast by an increment:'+bcolors.OKBLUE+ ' --inac value\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+ ' --inac 0.4'+bcolors.ENDC+' (adds 0.4 to the current value)\n'
ctfhelp11='	IV) To change B factor by an increment:'+bcolors.OKBLUE+ ' --inaf value(Angstrom-squared)\n'+bcolors.ENDC+'		Example:'+bcolors.OKBLUE+ ' --inaf 40'+bcolors.ENDC+' (adds 40 to the current value)\n'
ctfhelp12=bcolors.RED+'You can use any combination of the options available in B and C\n'+bcolors.ENDC+'	Example:'+bcolors.OKBLUE+' --dfcs 3.5 --sa 2 --inv -1\n'+bcolors.ENDC
ctfhelp13='If you just type'+bcolors.OKBLUE+' zoom'+bcolors.ENDC+' a new figure pops up containing only the input and output images (the latter being in its latest state). Remember that if you apply changes, this figure will not update.\n' 
ctfhelp14='Or if you have had enough fun, type'+bcolors.MAG+ ' q'+bcolors.ENDC+' to quit.\n\n\n'
ctfhelp15=':	'
ctfhelp=ctfhelp0+ctfhelp1+ctfhelp2+ctfhelp3+ctfhelp4+ctfhelp5+ctfhelp6+ctfhelp7+ctfhelp8+ctfhelp9+ctfhelp10+ctfhelp11+ctfhelp12+ctfhelp13+ctfhelp14+ctfhelp15

condition=0
i=0	
while condition==0:
	
	if i==0:		
		string=raw_input(ctfhelp)
	else:
		string=raw_input(ctfhelp15)
	if string=='q':
		condition=1
		plt.close()
	elif string=='zoom':
		fig2=plt.figure()
		Ax1=fig2.add_subplot(121)		
		Ax1.imshow(img,cmap='gray')
		Ax1.get_xaxis().set_visible(False)
		Ax1.get_yaxis().set_visible(False)
		
		Ax2=fig2.add_subplot(122)
		Ax2.get_xaxis().set_visible(False)
		Ax2.get_yaxis().set_visible(False)
		Ax2.imshow(absinv,cmap='gray')
		i+=1
	else:
		plt.figure(1)
		params,update=ctflib.runtimeparser(string)
		ctfpars=ctflib.parammap(params,update,ctfpars)
		absinv=process(ctfparams=ctfpars)
		i+=1
	
