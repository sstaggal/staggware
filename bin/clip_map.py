#!/usr/bin/env python

import numpy as np
import sys
from pyami import mrc

if len(sys.argv) != 5:
	print "To work:  ./clip_map.py [input mrc] [radius of sphere] [initial x center point] [mrc out]" 
	sys.exit()

mrc_in = sys.argv[1]
rad = int(sys.argv[2])
x_i = int(sys.argv[3])
mrc_out = sys.argv[4]

mrci = mrc.read(mrc_in)
box = int(mrci.shape[0])

mrco = mrci[(box/2)-rad:(box/2)+rad, (box/2)-rad:(box/2)+rad, x_i-rad:x_i+rad]

mrc.write(mrco, mrc_out)
print "Done! Model written to "+mrc_out
